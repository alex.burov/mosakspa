<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поверка счетчиков");
?>

 <!-- контент -->
      <div class="services-page services-page services-page--web">
        <div class="container">
          <h2 class="title-color">Поверка счетчиков воды</h2>
        </div>
        <div class="container">
              <div class="row">
                <div class="col-10 col-md-10 col-lg-9">
                  <p>Согласно ФЗ № 102 «Об обеспечении единства измерений» мы передаем данные о результатах поверки в электронном виде в Росстандарт через ФГИС "Аршин". Также для удобства потребителей мы продолжаем дополнительно выдавать документы о прохождении поверки счетчиков в бумажном виде.</p>
                </div>
               </div> 
              </div> 
        <div class="research section right">
          <div class="container">
            <div class="research__wrapper">
              <div class="services-page__line services-page__line--head"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-3 order-lg-2">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/master-4.png"><img class="research__img" src="/local/img/master-4.png" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-9 order-lg-1">
                  <p class="research__description description"></p>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="card-body">
                          <h4 class="card-title">Закон о поверке.</h4>
                          <p class="card-text">
Плановая поверка счетчиков является процедурой, необходимость которой предписана государством. Постановление правительства №354 от 6 мая 2011 обязывает собственников жилья проводить регулярную поверку приборов учета воды согласно их технической документации.<br>

Федеральный закон №102-ФЗ «Об обеспечении единства измерений» не предусматривает наказания за превышение сроков поверки, но нормативы расхода в Москве и Московской области составят 3,5 куб.м. горячей и 5 куб.м. холодной воды в месяц на человека.
</p>
                          
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="card-body">
                          <h4 class="card-title">Поверить или заменить?</h4>
                          <p class="card-text">Если кажется, что счетчик начал выдавать некорректные показания, необязательно его сразу менять. Всё-таки замена счетчиков воды – довольно затратная процедура как по времени, так и в финансовом смысле. Кроме того, может оказаться, что ваше учетное оборудование в порядке.<br>
Таким образом, при сомнениях в правильности работы водомера заказать диагностику на дому без снятия будет более разумным вариантом. Учитывая, что срок эксплуатации составляет 12 лет это вовсе не накладно.</p>

                        </div>
                      </div>

                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="technical-task section left">
          <div class="container">
            <div class="technical-task__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--left"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-4">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/master-3.png"><img class="technical-task__img" src="/local/img/master-3.png" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-8">
                    <div class="row">
                      <div class="col-md-6">
  
                        <div class="card-body">
                          <h4 class="card-title">Сроки поверок.</h4>
                          <br>
                          <p class="card-text">Официально по закону проверка счетчиков воды проводится в сроки, указанные в технической документации устройства. Они зависят от фирмы-производителя и марки устройства.<br>

Как правило, межповерочный интервал составляет 4 года для горячей и 6 лет для холодной воды.</p>
                  
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="card-body">
                          <h4 class="card-title">Что входит в поверку?</h4>
                          <br>
                          <p class="card-text">Законодательство указывает на необходимость государственной поверки счетчиков. Только организация, имеющая аккредитацию, имеет право осуществлять этот сервис.<br> 
                            АКСПА осуществит поверку счетчика воды без снятия на дому официально, проведет консультацию по эксплуатации прибора в дальнейшем.
                            </p>
               
                        </div>
                      </div>

                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="design section right">
          <div class="container">
            <div class="design__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="services-page__line services-page__line--footer"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-3 order-lg-2">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/pov.jpg"><img class="design__img" src="/local/img/pov.jpg" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-9 order-lg-1">
                  <h3 class="design__title title accent">Аппаратура
                  </h3>
                  <div class="row">
                    <div class="col-md-4">
                         <img  src="/local/img/itelma-4.jpg" class="card-img-top transform-img">
                    </div>
                    <div class="col-md-4">
                      <p class="design__description description">
                        По Москве от 600 руб.<br>
                        По Московской области от 700 руб.<br>
                        При вас заносим сведения о поверке счетчика в систему АРШИН<br>
                      </p>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="container">
           <h3 class="technical-task__title title accent">Стоимость поверки счетчиков воды
                  </h3>
             <div class="row">
                      <div class="col-md-4">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-ekonom.png" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Счетчик Эконом</h4>
                          <p class="card-text">Производство: Россия</p>
                          <p class="card-text">Поверка: 600 руб.</p>
                          
                          <p><small> Уточните точную стоимость позвонив оператору</small></p>
                          <p><small>В пределах МКАД, выезд за пределы МКАД 25 р./км</small></p>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-itelma.png" class="card-img-top transform-img">
                          </div>
                          <div class="card-body">
                          <h4 class="card-title">Счетчик Itelma</h4>
                          <p class="card-text">Производство: Германия</p>
                          <p class="card-text">Поверка: 600 руб.</p>
                          
                          <p><small> Уточните точную стоимость позвонив оператору</small></p>
                          <p><small>В пределах МКАД, выезд за пределы МКАД 25 р./км</small></p>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="sch-rew">
                        <img src="img/sch-valtec.png" class="card-img-top">
                          </div>
                             <div class="card-body">
                          <h4 class="card-title">Счетчик Valtec</h4>
                          <p class="card-text">Производство: Италия</p>
                          <p class="card-text">Поверка: 600 руб.</p>
                          
                          <p><small> Уточните точную стоимость позвонив оператору</small></p>
                          <p><small>В пределах МКАД, выезд за пределы МКАД 25 р./км</small></p>
                        </div>
                      </div>
                    </div>

          <div class="experience section">
          <div class="container">
            <div class="experience__wrapper">
              <div class="animate-line"><span class="animate-line__one"></span><span class="animate-line__two"></span><span class="animate-line__three"></span><span class="animate-line__four"></span>
              </div>
              <h4 class="experience__title title-color"><span>Наши сертификаты</span>
              </h4>
              <div class="row justify-content-lg-between">
                <div class="col-12 col-lg-4">
                  <div class="experience__item-wrapper"><img class="experience__img" src="/local/img/akr-1.jpg" width="" alt="" role="presentation"/>

                  </div>
                </div>
                <div class="col-12 col-lg-4">
                  <div class="experience__item-wrapper"><img class="experience__img" src="/local/img/akr-2.jpg" width="" alt="" role="presentation"/>
                  </div>
                </div>
                <div class="col-12 col-lg-4">
                  <div class="experience__item-"><img class="experience__img" src="/local/img/akr-3.jpg" style ="width: 227px;border: 1px solid;" alt="" role="presentation"/>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          
        </div>
 
        
        <div class="container"><a class="services-page__portfolio-link accent" href="#">Посмотреть наши работы<span class="decor"> >></span></a>
        </div>
      </div>
      <!-- end services-page-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>