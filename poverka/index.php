<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поверка счетчиков");
?><div class="container">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12">
			<h2 class="title-color" style="text-align: center;">Поверка</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12">
			<div class="img-text-box">
				<div class="text-box">
					<p>
						Если кажется, что счетчик начал выдавать некорректные показания, необязательно его сразу менять. Всё-таки замена счетчиков воды – довольно затратная процедура как по времени, так и в финансовом смысле. Кроме того, может оказаться, что ваше учетное оборудование в порядке. Таким образом, при сомнениях в правильности работы водомера заказать диагностику на дому без снятия будет более разумным вариантом. Учитывая, что срок эксплуатации составляет 12 лет это вовсе не накладно. <br>
					</p>
					<p>
						 Мастер-метролог сам приезжает к вам на дом в удобное время с переносным портативным стендом Эталон и в течение 10-15 минут делает поверку на работоспособность водосчетчика без демонтажа. Ниже указаны цены на популярные счетчики воды для Москвы
					</p>
					<p>
						Согласно ФЗ № 102 «Об обеспечении единства измерений» мы передаем данные о результатах поверки в электронном виде в Росстандарт через ФГИС "Аршин". Также для удобства потребителей мы продолжаем дополнительно выдавать документы о прохождении поверки счетчиков в бумажном виде.
					</p>
				</div>
 <img alt="1" src="/local/img/pov.jpg" title="" class="img-max-width-md">
			</div>
			<div class="text-box-full">
				<p>
					Если вы решили установить счетчики воды, то это первый и самый важный шаг на пути экономии за коммунальные платежи. Но не стоит торопиться. Вопреки большому количеству видео инструкций и статей далеко не каждая установка считается законной. Перед тем как что-то решать, обязательно проверьте информацию в достоверном источнике.
				</p>
				<p>
					Поставить счетчики на воду в Москве можно двумя способами:
				</p>
				<ul>
					<li><strong>Самостоятельно</strong> – при этом вам придется уведомлять ряд инстанций о своих действиях, ждать ответственных людей и какое-то время жить без воды.</li>
					<li>В <strong>спец. сервисе</strong> – мастер-монтажник выдаст вам полный пакет необходимых документов и объяснит, что именно нужно предоставить в УК, ТСЖ и т.д.</li>
				</ul>
				<p>
				</p>
				<p>
					В любом случае тщательно проверяйте все бумаги. При наличии ошибок обязательно просите их устранить. Ведь любая проблема в первую очередь отразится именно на вас.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12 col-md-12 col-lg-12">
 <section class="section licensies">
			<div class="container">
				<div class="section-content-wrap lic-title">
					<h2 class="title-color">Какие документы мы выдаем для регистрации поверки в УК</h2>
					<div class="licensies__content flex">
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)
							</p>
						</div>
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев
							</p>
						</div>
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Лицензии МЧС<br>
								№ 66-Б/01116
							</p>
						</div>
					</div>
					<div class="licensies__slider owl-carousel" id="licensies-slider">
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" class="owl-fancy-min"></a> <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro-2.7.jpg" data-fancybox="gallery" data-caption="СРО"><img alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro-2.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)
							</p>
						</div>
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" class="owl-fancy-min"></a> <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-2.2.jpg" data-fancybox="gallery" data-caption="ВСК"><img alt="feedback3_s" src="/local/static/truba-delo/images/webarch/license-2-2.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев
							</p>
						</div>
						<div class="license">
							<div class="license__image-wrap flex">
 <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" class="owl-fancy-min"></a>
							</div>
							<p class="license__text">
								Лицензии МЧС<br>
								№ 66-Б/01116
							</p>
						</div>
					</div>
				</div>
			</div>
 </section>
		</div>
	</div>
</div>
<!-- <div class="container">
      <div class="row">
          <div class="col-12 col-md-12 col-lg-12">

          </div>
      </div> 
</div> -->
<!-- форма -->
<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form",
	"calendar",
	Array(
		"COMPONENT_TEMPLATE" => "feebdack_up",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(0=>"2",),
		"IBLOCK_ID" => FORMS_IBLOCK,
		"IBLOCK_TYPE" => "forms",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(),
		"PROPERTY_CODES_REQUIRED" => array(),
		"RESIZE_IMAGES" => "N",
		"SEF_MODE" => "N",
		"SERVICE_TYPE" => 1,
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"TITLE" => "Отправить заявку на поверку счетчика",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N"
	)
);?>   
<!-- форма -->
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>