<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши работы");?>
<div class="content-wrapper">
   <div class="container">
            
      <!-- SERVICES -->
      <section class="section services">
         <div class="container">
               
               <div class="section-content-wrap serv-title">
                     <h2 class="h2 section__title">Портфолио</h2>
                     
                     <!-- desctop версия -->
                     <div class="cards service-cards flex service-cards_hidden">
                        
                              <div class="flipcard h">                                   
                                       <div class="card__image-wrap service-card__image-wrap">
                                          <img src="/local/static/truba-delo/images/webarch/service-3.jpg" alt="" class="card__image service-card__image">
                                       </div>

                                       <div class="service-card__content-wrap">
                                          <p class="service-card__title card__title stc">
                                             <a href="/uslugi/ustanovka-santehpriborov">замена счетчика</a>
                                          </p>

                                          <ul class="service-card__subservice-list">
                                             <li class="service-card__subservice">
                                                <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                                             </li>                           
                                          </ul>
                                       </div>                                    
                              </div>


                              <div class="flipcard h">                                    
                                       <div class="card__image-wrap service-card__image-wrap">
                                          <img src="/local/static/truba-delo/images/webarch/service-3.jpg" alt="" class="card__image service-card__image">
                                       </div>

                                       <div class="service-card__content-wrap">
                                          <p class="service-card__title card__title stc">
                                             <a href="/uslugi/ustanovka-santehpriborov">сантехника</a>
                                          </p>

                                          <ul class="service-card__subservice-list">
                                             <li class="service-card__subservice">
                                                <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                                             </li>                           
                                          </ul>
                                       </div>                                   
                              </div>

                              <div class="flipcard h">                                    
                                       <div class="card__image-wrap service-card__image-wrap">
                                          <img src="/local/static/truba-delo/images/webarch/service-5.jpg" alt="" class="card__image service-card__image">
                                       </div>

                                       <div class="service-card__content-wrap">
                                          <p class="service-card__title card__title stc">
                                             <a href="/uslugi/sistemi-otopleniya">системы отопления</a>
                                          </p>

                                          <ul class="service-card__subservice-list">
                                             <li class="service-card__subservice">
                                                <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                                             </li>                           
                                          </ul>
                                       </div>                                                                   
                              </div>
                     </div> 

                     <!-- desctop версия -->

               </div>

               <!-- mobile версия -->
               <div class="cards service-cards owl-carousel" id="service-cards_slider">  

                  <!-- service-1 -->
                  <div class="flipcard h">                    
                        <div class="card__image-wrap service-card__image-wrap">
                           <img src="/local/static/truba-delo/images/webarch/service-1.jpg" alt="" class="card__image service-card__image">
                        </div>
                        <div class="service-card__content-wrap">
                           <p class="service-card__title card__title stc">
                              <a href="/uslugi/avariinaya_slujba">замена счетчика</a>
                           </p>
                           <ul class="service-card__subservice-list">
                              <li class="service-card__subservice">
                                 <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                              </li>                           
                           </ul>
                        </div>                  
                  </div>

                  <!-- service-2 -->
                  <div class="flipcard h">                    
                        <div class="card__image-wrap service-card__image-wrap">
                           <img src="/local/static/truba-delo/images/webarch/service-2.jpg" alt="" class="card__image service-card__image">
                        </div>
                        <div class="service-card__content-wrap">
                           <p class="service-card__title card__title stc">
                              <a href="/uslugi/ustranenie-zasorov">сантехника</a>
                           </p>
                            <ul class="service-card__subservice-list">
                              <li class="service-card__subservice">
                                 <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                              </li>                           
                           </ul>
                        </div>                   
                  </div>

                  <!-- service-3 -->
                  <div class="flipcard h">                    
                        <div class="card__image-wrap service-card__image-wrap">
                           <img src="/local/static/truba-delo/images/webarch/service-3.jpg" alt="" class="card__image service-card__image">
                        </div>
                        <div class="service-card__content-wrap">
                           <p class="service-card__title card__title stc">
                              <a href="/uslugi/ustanovka-santehpriborov">системы отопления</a>
                           </p>
                           <ul class="service-card__subservice-list">
                              <li class="service-card__subservice">
                                 <a href="/uslugi/ustranenie-zasorov/zasor-unitaza">смотреть подробнее</a>
                              </li>                           
                           </ul>
                        </div>                    
                  </div>

               </div>
               <!-- mobile версия -->


            </div>
         </div>
      </section>


   </div>
</div>

<script type="text/javascript">

   var owl = $('#service-cards_slider');
   owl.owlCarousel({
         items:1,
         center: false,
         loop:true,
         dots:false,
         nav: true,
         autoplay: false,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         navText: ['<span aria-label="Previous">‹</span>','<span aria-label="Next">›</span>']
      });

</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>