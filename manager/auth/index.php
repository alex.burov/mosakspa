<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}

$APPLICATION->SetTitle("Авторизация");
?>
<div class="research section right">
		<div class="container">
		    <div class="research__wrapper">                    
		        <div class="col-12 col-lg-12 order-lg-1 manager">
					<p>Вы зарегистрированы и успешно авторизовались.</p>
					<p><a href="<?=SITE_DIR?>manager/?logout=yes">Выйти</a></p>
				</div>
			</div>
		</div>
</div>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>