<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аккаунт");
?>
<div class="research section right">
	<div class="container">
		<div class="research__wrapper">                    
		    <div class="col-12 col-lg-12 order-lg-1 manager">
		        
		       		<h2>Кабинет сотрудника</h2>

		       		<?if($USER->IsAuthorized()):?>
		       		<?$arUser = getUserData($USER->GetID());?>	
						<div class="row">
							<div class="col-12 account-info">

									<div class="form-group row">
										<label>Логин:</label>
										<div class="acount-field">
											<?=$arUser['LOGIN']?>
										</div>
									</div> 

									<div class="form-group row">
										<label>ФИО:</label>
										<div class="acount-field"><?=$arUser['NAME']?> <?=$arUser['SECOND_NAME']?> <?=$arUser['LAST_LOGIN']?></div>
									</div>

									<div class="form-group row">
										<label>E-Mail:</label>
										<div class="acount-field">
											<?=$arUser['EMAIL']?>
										</div>
									</div>

									<div class="form-group row">
										<label>Телефон:</label>
										<div class="acount-field">
											<?=$arUser['PERSONAL_PHONE']?>
										</div>
									</div>

									<div class="form-group row">
										<label>Должность:</label>
										<div class="acount-field">
											<?=$arUser['TYPE']?>
										</div>
									</div> 
												
							</div>

						</div>						

					<?endif?>

				</div>

			</div>

		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>