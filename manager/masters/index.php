<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мастера");?>
<?if($USER->IsAuthorized()):?> 
<h2 class="manager-header">Мастера</h2> 
<?
$APPLICATION->IncludeComponent(
			"mosakspa:user.list", 
			"employee", 
			array(
				"COMPONENT_TEMPLATE" => $template,
				"COUNT" => count($users),
				"SORT_BY1" => "NAME",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ACTIVE_FROM",
				"SORT_ORDER2" => "ASC",
				"SORT_BY3" => "id",
				"SORT_ORDER3" => "DESC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "ID",
					1 => "LOGIN",
					2 => "NAME",
				),
				"PROPERTY_CODE" => array(
				),
				"DETAIL_URL" => "/manager/masters/profile/",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"RESIZE_PERSONAL_PHOTO" => "500*600",
				"RESIZE_WORK_LOGO" => "500*600",
				"RESIZE_TYPE" => "BX_RESIZE_IMAGE_PROPORTIONAL",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_STATUS_404" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Пользователи",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"GROUPS_ID"=>MASTERS, 
				"APPLICATIONS_URL"=>"/manager/applications/masters/",
				"TITLE"=>"Заявки мастера ".$arMarager['NAME']." ".$arMarager["LAST_NAME"]
			),
			false
);
?>
<?endif?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 
