<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль мастера");?>
<?if($USER->IsAuthorized()):?> 
<?if(isset($_REQUEST["USER_ID"])){
	$user_id = (int)$_REQUEST["USER_ID"];
}?>
<?$APPLICATION->IncludeComponent(
	"mosakspa:main.profile", 
	"profile", 
	array(
		"CHECK_RIGHTS" => "N",
		"SEND_INFO" => "N",
		"SET_TITLE" => "N",
		"USER_ID" => $user_id,
		"USER_PROPERTY_NAME" => "",
		"COMPONENT_TEMPLATE" => "profile",
		"USER_PROPERTY" => array(
			0 => "UF_DOCUMENT",
		)
	),
	false
);?>
<?endif?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 
