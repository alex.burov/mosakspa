<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кабинет менеджера");
use Bitrix\Main\Context;
$request = Context::getCurrent()->getRequest();


if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}
?> 
<?if($USER->IsAuthorized()):?> 
	<?$arUser = getUserData($USER->GetID());?> 
	<div class="services-page services-page services-page--web">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 col-lg-9">

					<p><b>Раздел для сотрудников организации</b></p>
					<?if($arUser['TYPE']!=='master'):?> 
						<p><b><a href="<?=SITE_DIR?>manager/applications/">Список заявок</a></b></p>
					<?else:?>
						<p><b><a href="<?=SITE_DIR?>manager/applications/masters/">Список заявок</a></b></p>
					<?endif?>
					<p><a href="<?=SITE_DIR?>manager/?logout=yes">Выйти</a></p>				
					
				</div>
			</div>
		</div>
	</div>
<?endif?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>