<?
// define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Выход из аккаунта");?>
<div class="research section right">
		<div class="container">
		    <div class="research__wrapper">                    
		        <div class="col-12 col-lg-12 order-lg-1 manager">
		        	<?if($USER->IsAuthorized()):?>
					<?
						$USER->Logout();
						// LocalRedirect("/manager/auth/");
					?>
					<?else:?>
						<p>Вы не авторизованы</p>
						<p><a href="<?=SITE_DIR?>manager/auth/">Страница авторизации</a></p>
					<?endif?>
					
				</div>
			</div>
		</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>