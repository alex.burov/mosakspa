<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки оператора");?>
<?if($USER->IsAuthorized()):?> 
<?
if(isset($_REQUEST["USER_ID"])){	
	$user_id = (int)$_REQUEST["USER_ID"];
	global $arrFilter;
	$arrFilter = array("PROPERTY_OPERATOR"=> $user_id);
}
?>
<?
	$arUser = getUserData($USER->GetID());
	
	if($arUser['TYPE'] == 'admin'){
		$PROPERTY_CODES = array(
				"NAME",
				"25",
				"26",
				"28",
				"31",
				"32", // округа
				"33",
				"34",
				"35",
				"36",
				"37",
				"38", 
				// "39",
				"40",
				"41", //метро
				"IBLOCK_SECTION",
		);
	}
	
	if($arUser['TYPE'] == 'operator'){
		$PROPERTY_CODES = array(
				"NAME",
				"25",
				"26",
				"28",
				"31",
				"32", // округа
				// "33", // отчет
				// "34",
				// "35",
				"36", // "36" мастер
				"40",
				"41" // метро
		);		
	}	

	if($arUser['TYPE'] == 'master'){		
		LocalRedirect('/manager');
	}

	$title = "Заявки операторов";
	if($user_id){
		$arMarager = getUserData($user_id);
		$title = "Заявки оператора ".$arMarager['NAME']." ".$arMarager["LAST_NAME"];
	}

	// фильтр по разделу site
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"applications", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "EMAIL",
			1 => "TIME",
			2 => "DONE",
			3 => "DATE",
			4 => "MASTER",
			5 => "MATERIALS",
			6 => "DISTRICTS",
			7 => "MESSAGE",
			8 => "PHONE",
			9 => "SERVICE_TYPE",
			10 => "NAME",
			11 => "ADDRESS",
			12 => "COMPANY",
			13 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "forms",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "TIME",
			1 => "DATE",
			2 => "PHONE",
			3 => "SERVICE_TYPE",
			4 => "ADDRESS",
			5 => "COMPANY",
			6 => "MASTER",
			7 => "OPERATOR"
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "arrFilter",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "applications",
		"PROPERTY_CODES" => $PROPERTY_CODES,
		"USER_TYPE" => $arUser["TYPE"],
		"GROUPS" => array(
			0 => ADMINS,
			1 => OPERATORS,
			2 => MASTERS,
		),
		"EDIT" => (($request->get("EDIT"))?htmlspecialcharsbx($request->get("EDIT")):"N"),
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SEF_FOLDER" => "/manager/masters/applications/",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "#ELEMENT_ID#/",
		),
		"LINKED_IBLOCKS"=>array(
			'MASTER'=>MASTERS,
			'CAMPAIGN'=>CAMPAIGNS_IBLOCK,
			'METRO'=>METRO_IBLOCK,
			'DISTRICTS'=>DISTRICTS_IBLOCK
		),
		"TITLE"=>$title
	),
	false
);?>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>