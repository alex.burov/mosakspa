<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Операторы");?><?if($USER->IsAuthorized()):?>
<h2 class="manager-header">Операторы</h2>
<?$APPLICATION->IncludeComponent(
	"mosakspa:user.list",
	"employee",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"APPLICATIONS_URL" => "/manager/applications/main/",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => $template,
		"COUNT" => count($users),
		"DETAIL_URL" => "/manager/masters/profile/",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"ID",1=>"LOGIN",2=>"NAME",),
		"FILTER_NAME" => "",
		"GROUPS_ID" => OPERATORS,
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Пользователи",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(),
		"RESIZE_PERSONAL_PHOTO" => "500*600",
		"RESIZE_TYPE" => "BX_RESIZE_IMAGE_PROPORTIONAL",
		"RESIZE_WORK_LOGO" => "500*600",
		"SET_STATUS_404" => "N",
		"SORT_BY1" => "NAME",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_BY3" => "id",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"SORT_ORDER3" => "DESC"
	)
);?>
<?endif?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>