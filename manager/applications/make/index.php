<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки с сайта");
use Bitrix\Main\Context;
$request = Context::getCurrent()->getRequest();

if($USER->IsAuthorized()):?> 
	<?$arUser = getUserData($USER->GetID());
		if($arUser['TYPE'] == 'admin'){
			$PROPERTY_CODES = array(
					"NAME",
					"25",
					"26",
					"28",
					"31",
					"32", // округа
					"33",
					"34",
					"35",
					"36",
					"37",
					"38", 
					"40",
					"41", //метро
					"42", // оператор
					"48",
					"IBLOCK_SECTION",
			);
		}

		if($arUser['TYPE'] == 'operator'){
			$PROPERTY_CODES = array(
					"NAME",
					"25",
					"26",
					"28",
					"31",
					"32", // округа
					"36", // "36" мастер
					"38", 
					"40",
					"41", // метро
					"42", // оператор
					"48"
			);		
		}	

		if($arUser['TYPE'] == 'master'){
			LocalRedirect('/manager');	
		}
?> 
<?$APPLICATION->IncludeComponent(
	"mosakspa:iblock.element.add.form", 
	"application", 
	array(
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(
			0 => ADMINS,
			1 => OPERATORS,
			2 => MASTERS,
		),
		"IBLOCK_ID" => FORMS_IBLOCK,
		"IBLOCK_TYPE" => "forms",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => $PROPERTY_CODES,
		"PROPERTY_CODES_REQUIRED" => array(),
		"RESIZE_IMAGES" => "N",
		"SEF_FOLDER" => "/manager/applications/",
		"SEF_MODE" => "Y",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "application",
		"LINKED_IBLOCKS"=>array(
			'MASTER'=>MASTERS,
			'CAMPAIGN'=>CAMPAIGNS_IBLOCK,
			'METRO'=>METRO_IBLOCK,
			'DISTRICTS'=>DISTRICTS_IBLOCK,
			'OPERATOR'=>OPERATORS
		),
		"USER_TYPE" => $arUser['TYPE']
	),
	false
);?>
<?endif?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>