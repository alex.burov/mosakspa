<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки с сайта");
?>
<?
// $APPLICATION->IncludeComponent(
// 	"bitrix:news.list", 
// 	".default", 
// 	array(
// 		"ACTIVE_DATE_FORMAT" => "d.m.Y",
// 		"ADD_SECTIONS_CHAIN" => "Y",
// 		"AJAX_MODE" => "N",
// 		"AJAX_OPTION_ADDITIONAL" => "",
// 		"AJAX_OPTION_HISTORY" => "N",
// 		"AJAX_OPTION_JUMP" => "N",
// 		"AJAX_OPTION_STYLE" => "Y",
// 		"CACHE_FILTER" => "N",
// 		"CACHE_GROUPS" => "Y",
// 		"CACHE_TIME" => "36000000",
// 		"CACHE_TYPE" => "A",
// 		"CHECK_DATES" => "Y",
// 		"DETAIL_URL" => "",
// 		"DISPLAY_BOTTOM_PAGER" => "Y",
// 		"DISPLAY_DATE" => "Y",
// 		"DISPLAY_NAME" => "Y",
// 		"DISPLAY_PICTURE" => "Y",
// 		"DISPLAY_PREVIEW_TEXT" => "Y",
// 		"DISPLAY_TOP_PAGER" => "N",
// 		"FIELD_CODE" => array(
// 			0 => "",
// 			1 => "",
// 		),
// 		"FILTER_NAME" => "",
// 		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
// 		"IBLOCK_ID" => "13",
// 		"IBLOCK_TYPE" => "forms",
// 		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
// 		"INCLUDE_SUBSECTIONS" => "Y",
// 		"MESSAGE_404" => "",
// 		"NEWS_COUNT" => "20",
// 		"PAGER_BASE_LINK_ENABLE" => "N",
// 		"PAGER_DESC_NUMBERING" => "N",
// 		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
// 		"PAGER_SHOW_ALL" => "N",
// 		"PAGER_SHOW_ALWAYS" => "N",
// 		"PAGER_TEMPLATE" => ".default",
// 		"PAGER_TITLE" => "Новости",
// 		"PARENT_SECTION" => "",
// 		"PARENT_SECTION_CODE" => "",
// 		"PREVIEW_TRUNCATE_LEN" => "",
// 		"PROPERTY_CODE" => array(
// 			0 => "EMAIL",
// 			1 => "ADDRESS",
// 			2 => "TIME",
// 			3 => "TRANSFER_TIME",
// 			4 => "DONE",
// 			5 => "TRANSFER_DATE",
// 			6 => "DATE",
// 			7 => "SECOND_PHONE",
// 			8 => "REMARK",
// 			9 => "MASTER",
// 			10 => "TRANSFER_MASTER",
// 			11 => "MATERIALS",
// 			12 => "OPERATOR",
// 			13 => "SEARCH_ID",
// 			14 => "REJECTION_REASON",
// 			15 => "MESSAGE",
// 			16 => "SUM",
// 			17 => "PHONE",
// 			18 => "SERVICE_TYPE",
// 			19 => "NAME",
// 			20 => "FORM",
// 			21 => "",
// 		),
// 		"SET_BROWSER_TITLE" => "Y",
// 		"SET_LAST_MODIFIED" => "N",
// 		"SET_META_DESCRIPTION" => "Y",
// 		"SET_META_KEYWORDS" => "Y",
// 		"SET_STATUS_404" => "N",
// 		"SET_TITLE" => "Y",
// 		"SHOW_404" => "N",
// 		"SORT_BY1" => "ACTIVE_FROM",
// 		"SORT_BY2" => "SORT",
// 		"SORT_ORDER1" => "DESC",
// 		"SORT_ORDER2" => "ASC",
// 		"STRICT_SECTION_CHECK" => "N",
// 		"COMPONENT_TEMPLATE" => ".default"
// 	),
// 	false
// );
?>

<?
// $APPLICATION->IncludeComponent(
//     "bitrix:news.list", 
//     ".default", 
//     array(
//         "ACTIVE_DATE_FORMAT" => "d.m.Y",
//         "ADD_SECTIONS_CHAIN" => "Y",
//         "AJAX_MODE" => "N",
//         "AJAX_OPTION_ADDITIONAL" => "",
//         "AJAX_OPTION_HISTORY" => "N",
//         "AJAX_OPTION_JUMP" => "N",
//         "AJAX_OPTION_STYLE" => "Y",
//         "CACHE_FILTER" => $arParams["CACHE_FILTER"],,
//         "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
//         "CACHE_TIME" => $arParams["CACHE_TIME"],
//         "CACHE_TYPE" => $arParams["CACHE_TYPE"],
//         "CHECK_DATES" => "Y",
//         "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
//         "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
//         "DISPLAY_DATE" => "Y",
//         "DISPLAY_NAME" => "Y",
//         "DISPLAY_PICTURE" => "Y",
//         "DISPLAY_PREVIEW_TEXT" => "Y",
//         "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
//         "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
//         "FILTER_NAME" => "",
//         "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//         "IBLOCK_ID" => $arParams["IBLOCK_ID"],
//         "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
//         "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
//         "INCLUDE_SUBSECTIONS" => "Y",
//         "MESSAGE_404" => $arParams["MESSAGE_404"],
//         "NEWS_COUNT" => $arParams["NEWS_COUNT"],
//         "PAGER_BASE_LINK_ENABLE" => "N",
//         "PAGER_DESC_NUMBERING" => "N",
//         "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//         "PAGER_SHOW_ALL" => "N",
//         "PAGER_SHOW_ALWAYS" => "N",
//         "PAGER_TEMPLATE" => ".default",
//         "PAGER_TITLE" => $arParams["PAGER_TITLE"],
//         "PARENT_SECTION" => "",
//         "PARENT_SECTION_CODE" => "",
//         "PREVIEW_TRUNCATE_LEN" => "",
//         "PROPERTY_CODE" => array(
//             0 => "EMAIL",
//             1 => "ADDRESS",
//             2 => "TIME",
//             3 => "TRANSFER_TIME",
//             4 => "DONE",
//             5 => "TRANSFER_DATE",
//             6 => "DATE",
//             7 => "SECOND_PHONE",
//             8 => "REMARK",
//             9 => "MASTER",
//             10 => "TRANSFER_MASTER",
//             11 => "MATERIALS",
//             12 => "OPERATOR",
//             13 => "SEARCH_ID",
//             14 => "REJECTION_REASON",
//             15 => "MESSAGE",
//             16 => "SUM",
//             17 => "PHONE",
//             18 => "SERVICE_TYPE",
//             19 => "NAME",
//             20 => "FORM",
//             21 => "",
//         ),
//         "SET_BROWSER_TITLE" => "Y",
//         "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
//         "SET_META_DESCRIPTION" => "Y",
//         "SET_META_KEYWORDS" => "Y",
//         "SET_STATUS_404" => $arParams["SET_STATUS_404"],
//         "SET_TITLE" => $arParams["SET_TITLE"],
//         "SHOW_404" => $arParams["SHOW_404"],
//         "SORT_BY1" => $arParams["SORT_BY1"],
//         "SORT_BY2" => $arParams["SORT_BY2"],
//         "SORT_ORDER1" => $arParams["SORT_ORDER1"],
//         "SORT_ORDER2" => "ASC",
//         "STRICT_SECTION_CHECK" => "N",
//         "COMPONENT_TEMPLATE" => ".default",
//         "USE_FILTER" => "Y", // !!!
//         "FILTER_NAME" => "arrFilter"
//     ),
//     false
// );

// use Bitrix\Main\Loader;
// Bitrix\Main\Loader::IncludeModule('iblock');
// $arPROPS = array(
//     'NAME' => 'TEST 1',
// );
// $arFields = array(
//     'IBLOCK_ID' => FORMS_IBLOCK,
//     'ACTIVE' => 'Y',
//     'NAME' => 'TEST 1',
//     'DATE_ACTIVE_FROM' => new \Bitrix\Main\Type\DateTime(),
//     'PROPERTY_VALUES' => $arPROPS,
//     'IBLOCK_SECTION_ID' => SITE_IBLOCK_SECTION    
// );

// $el = new \CIBlockElement();
// echo $itemId = $el->Add($arFields);



// $property_enums = CIBlockPropertyEnum::GetList(
// 	array("DEF"=>"DESC", "SORT"=>"ASC"), 
// 	array("IBLOCK_ID"=>FORMS_IBLOCK, "CODE"=>"ARRIVAL_DATE_SET")
// );
// while($enum_fields = $property_enums->GetNext())
// {
//   // echo $enum_fields["ID"]." - ".$enum_fields["VALUE"]."<br>";
// 	pr($enum_fields);
// }

// if (CModule::IncludeModule("iblock")){

// 	$property_enums = CIBlockPropertyEnum::GetList(
// 		Array("ID"=>"ASC", "SORT"=>"ASC"), 
// 		Array("IBLOCK_ID"=>FORMS_IBLOCK, "CODE"=>"ARRIVAL_DATE_SET")
// 	);
// 	$arrivalDateSet = [];
// 	while ($enum_fields = $property_enums->GetNext()) {
// 		$arrivalDateSet[] = $enum_fields;
// 	}

// 	pr($arrivalDateSet);
// }

?>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>