<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявки с сайта");
// use Bitrix\Main\Context;
// $request = Context::getCurrent()->getRequest();
// $edit = 'N';
?><?if($USER->IsAuthorized()):?> <?$arUser = getUserData($USER->GetID());?>
<div class="services-page services-page services-page--web">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10 col-lg-9">
				 <?if($arUser['TYPE']!='admin'):?> <?if($arUser['TYPE']=='operator'):?>
				<p>
 <a href="/manager/applications/site/">Заявки с сайта</a>
				</p>
				<p>
 <a href="/manager/applications/operators/">Заявки от организаций</a>
				</p>
				 <?endif?> <?if($arUser['TYPE']=='master'):?>
				<p>
 <a href="/manager/applications/masters/">Назначенные заявки</a>
				</p>
				 <?endif?> <?else:?>
				<p>
 <b><a href="/manager/applications/site/">Заявки с сайта (для операторов)</a></b>
				</p>
				<p>
 <b><a href="/manager/applications/operators/">Заявки от организаций (для операторов)</a></b>
				</p>
				<p>
 <b><a href="/manager/applications/masters/">Назначенные заявки (для мастеров)</a></b>
				</p>
				 <?endif?>
			</div>
		</div>
	</div>
</div>
 <?else:?> <?LocalRedirect('/manager/');?> <?endif?> 
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>