<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("История заявок с сайта");
use Bitrix\Main\Context;
$request = Context::getCurrent()->getRequest();
$edit = 'N';?>

<?if($USER->IsAuthorized()):?>
<?$arUser = getUserData($USER->GetID());
	if($arUser['TYPE']!='admin'){
		LocalRedirect('/manager');
	}
?>
	<div class="services-page services-page services-page--web">
	    <div class="container">
	        <div class="row">
	        	<div class="col-12 col-md-10 col-lg-9">
	        			<p>История заявок - список всех заявок которые были сделаны</p> 
				</div>
			</div>
		</div>
	</div>
<?endif?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>