<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?><div class="services-page services-page services-page--web">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10 col-lg-9">
				<p><b>Поиск заявок по параметрам: номер, телефон, адрес</b></p>

						<?
						$APPLICATION->IncludeComponent(
				"bitrix:search.page", 
				"mosakspa", 
				array(
					"AJAX_MODE" => "N",
					"RESTART" => "N",
					"CHECK_DATES" => "Y",
					"USE_TITLE_RANK" => "N",
					"DEFAULT_SORT" => "rank",
					"arrWHERE" => "",
					"arrFILTER" => array(
					),
					"SHOW_WHERE" => "N",
					"PAGE_RESULT_COUNT" => "50",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"PAGER_TITLE" => "Результаты поиска",
					"PAGER_SHOW_ALWAYS" => "Y",
					"PAGER_TEMPLATE" => "",
					"AJAX_OPTION_SHADOW" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"COMPONENT_TEMPLATE" => "mosakspa",
					"NO_WORD_LOGIC" => "N",
					"FILTER_NAME" => "",
					"SHOW_WHEN" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"USE_LANGUAGE_GUESS" => "Y",
					"SHOW_RATING" => "",
					"RATING_TYPE" => "",
					"PATH_TO_USER_PROFILE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"RESULT_PATH" => "/manager/applications/"
				),
				false
			);
			?>

				<p><b><a href="<?=SITE_DIR?>manager/applications/">Список заявок</a></b></p>
				<p><a href="<?=SITE_DIR?>manager/?logout=yes">Выйти</a></p>

			</div>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>