<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("TEST");
?>

<!-- наше преимущества -->
<?require_once($_SERVER["DOCUMENT_ROOT"].'/local/include/static/advantage.php');?>  
<!-- наше преимущества  -->

<!-- наши услуги -->
<?require_once($_SERVER["DOCUMENT_ROOT"].'/local/include/static/services.php');?>  
<!-- наши услуги  -->

<!-- форма -->
<?
$APPLICATION->IncludeComponent(
  "bitrix:iblock.element.add.form", 
  "calendar", 
  array(
    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
    "CUSTOM_TITLE_DETAIL_TEXT" => "",
    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
    "CUSTOM_TITLE_NAME" => "",
    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
    "CUSTOM_TITLE_TAGS" => "",
    "DEFAULT_INPUT_SIZE" => "30",
    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
    "ELEMENT_ASSOC" => "CREATED_BY",
    "GROUPS" => array(
      0 => "2",
    ),
    "IBLOCK_ID" => FORMS_IBLOCK,
    "IBLOCK_TYPE" => "forms",
    "LEVEL_LAST" => "Y",
    "LIST_URL" => "",
    "MAX_FILE_SIZE" => "0",
    "MAX_LEVELS" => "100000",
    "MAX_USER_ENTRIES" => "100000",
    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
    "PROPERTY_CODES" => array(
    ),
    "PROPERTY_CODES_REQUIRED" => array(
    ),
    "RESIZE_IMAGES" => "N",
    "SEF_MODE" => "N",
    "STATUS" => "ANY",
    "STATUS_NEW" => "N",
    "USER_MESSAGE_ADD" => "",
    "USER_MESSAGE_EDIT" => "",
    "USE_CAPTCHA" => "N",
    "COMPONENT_TEMPLATE" => "feebdack_up",
    "TITLE"=>"Отправить заявку"
  ),
  false
);
?>   
<!-- форма -->

<!-- как мы работаем -->
<?require_once($_SERVER["DOCUMENT_ROOT"].'/local/include/static/process.php');?>  
<!-- как мы работаем  -->

<!-- отзывы -->
<?require_once($_SERVER["DOCUMENT_ROOT"].'/local/include/static/reviews.php');?>  
<!-- отзывы  -->

<!-- клиенты -->
<?require_once($_SERVER["DOCUMENT_ROOT"].'/local/include/static/clients.php');?>  
<!-- клиенты  -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>