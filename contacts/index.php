<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контактная информация");
?>


<div class="container">
	<div class="row">
        <div class="col-xs-12">
			

		<h2 class="title-color">Наши контакты</h2>

		<div class="row contacts-list">
           <div class="col-md-4 ">
               <div class="item row">
                  <div class="col-md-3 col-sm-2 img"> <img class="img-responsive" src="https://user87271.clients-cdnnow.ru/files/images/contacts/phone.png" alt="Телефоны"> </div>
                  <div class="col-md-9 col-sm-10">
                     <h2>Телефоны</h2>
                     <div class="desc">
                        <p>+7 (495) 374-57-21</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="item row">
                  <div class="col-md-3 col-sm-2 img"> <img class="img-responsive" src="https://user87271.clients-cdnnow.ru/files/images/contacts/email.png" alt="Email"> </div>
                  <div class="col-md-9 col-sm-10">
                     <h2>Email</h2>
                     <div class="desc">
                        <p><a href="mailto:info@gcur.ru">poverka@dbu-schetchiki.ru</a></p>

                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="item row">
                  <div class="col-md-3 col-sm-2 img"> <img class="img-responsive" src="https://user87271.clients-cdnnow.ru/files/images/contacts/time.png" alt="Время работы"> </div>
                  <div class="col-md-9 col-sm-10">
                     <h2>Время работы</h2>
                     <div class="desc">
                        <p>с 9.00 до 21.00 ежедневно<br /> Без выходных</p>
                     </div>
                  </div>
               </div>
            </div>
        </div>

		<h2 class="title-color">Наш офис в Москве</h2>


		<p>Обратитесь к нашим специалистам и получите профессиональную консультацию по услугам нашего банка.</p>

		<p>Вы можете обратиться к нам по телефону, по электронной почте или договориться о встрече в нашем офисе. Будем рады помочь вам и ответить на все ваши вопросы. </p>

			<?
				$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"API_KEY" => "",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "MINIMAP",
			2 => "TYPECONTROL",
			3 => "SCALELINE",
		),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:3:{s:10:\"yandex_lat\";s:7:\"55.7383\";s:10:\"yandex_lon\";s:7:\"37.5946\";s:12:\"yandex_scale\";i:14;}",
		"MAP_HEIGHT" => "",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);
			?>

		</div>
	</div>
</div>

<div class="container">
                  <div class="row contacts-list req">
                  	 <h2 class="title-color">Реквизиты</h2>
                     <div class="col-md-12">
                        <div class="row item last">

                           <div class="col-md-12 col-sm-12">
                             
                              <div class="desc">
                                 <table class="table none">
                                    <tbody>
                                       <tr>
                                          <td>Полное наименование юридического лица:</td>
                                          <td>Общество с ограниченной ответственностью «<a title="Городской центр учета и экономии ресурсов в Москве" href="https://gcur.ru/centre">Городской Центр Учета и Экономии Ресурсов</a>»</td>
                                       </tr>
                                       <tr>
                                          <td>Сокращенное наименование юридического лица:</td>
                                          <td>ООО «ГЦУ и ЭР»</td>
                                       </tr>
                                       <tr>
                                          <td>Генеральный директор:</td>
                                          <td>----</td>
                                       </tr>
                                       <tr>
                                          <td>ИНН</td>
                                          <td>---</td>
                                       </tr>
                                       <tr>
                                          <td>КПП</td>
                                          <td>---</td>
                                       </tr>
                                       <tr>
                                          <td>ОГРН</td>
                                          <td>---</td>
                                       </tr>
                                       <tr>
                                          <td>Юридический адрес</td>
                                          <td>---</td>
                                       </tr>
                                       <tr>
                                          <td>Номер аттестата аккредитации</td>
                                          <td>---</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
</div>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>