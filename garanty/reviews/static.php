<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>

   <?$APPLICATION->IncludeComponent(
     "bitrix:iblock.element.add.form", 
     "reviews", 
     array(
       "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
       "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
       "CUSTOM_TITLE_DETAIL_PICTURE" => "",
       "CUSTOM_TITLE_DETAIL_TEXT" => "",
       "CUSTOM_TITLE_IBLOCK_SECTION" => "",
       "CUSTOM_TITLE_NAME" => "",
       "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
       "CUSTOM_TITLE_PREVIEW_TEXT" => "",
       "CUSTOM_TITLE_TAGS" => "",
       "DEFAULT_INPUT_SIZE" => "30",
       "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
       "ELEMENT_ASSOC" => "CREATED_BY",
       "GROUPS" => array(
         0 => "2",
       ),
       "IBLOCK_ID" => FORMS_IBLOCK,
       "IBLOCK_TYPE" => "forms",
       "LEVEL_LAST" => "Y",
       "LIST_URL" => "",
       "MAX_FILE_SIZE" => "0",
       "MAX_LEVELS" => "100000",
       "MAX_USER_ENTRIES" => "100000",
       "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
       "PROPERTY_CODES" => array(
       ),
       "PROPERTY_CODES_REQUIRED" => array(
       ),
       "RESIZE_IMAGES" => "N",
       "SEF_MODE" => "N",
       "STATUS" => "ANY",
       "STATUS_NEW" => "N",
       "USER_MESSAGE_ADD" => "",
       "USER_MESSAGE_EDIT" => "",
       "USE_CAPTCHA" => "N",
       "COMPONENT_TEMPLATE" => "feebdack_up"
     ),
     false
   );?>

   <div class="page-reviews">           

            <div class="content reviews-container">
               <div class="container">
                  <div class="row">

                     <div class="col-sm-12">
                       
                        
                        <div class="ec-message-list">
                           <h3 class="title-color">Отзывы клиентов</h3>

                           <div class="ec-message">
                              <div class="number-order"> Номер заявки: #849455 </div>
                              <div class="name">Ирина</div>
                              <div class="desc">
                                 <p>В новом доме надо было установить счётчики на воду. Подали заявку в фирму Городской центр учёта и в этот же день приехал мастер. Выполнил качественно работу и оформил соответствующие документы. Кстати, они и ещё водомер уже включены в стоимость услуги. Буду теперь знать надёжную фирму, ведь их специалисты выполняют и сантехнические работы, и техобслуживание и другие.</p>
                              </div>

                              <div class="services" >
                                 <div class="ser">
                                    <div class="OtNaUs">Отзыв на услугу:</div>
                                    <div class="NameUs"> 
                                    	<span itemprop="itemReviewed">Установка счетчиков воды</span> 
                                    </div>
                                 </div>
                                 <div class="ratval"> 
                                 	<span itemprop="ratingValue">5</span> 
                                 	<span itemprop="bestRating">5</span> 
                                 </div>
                                 <div class="ec-stars"> <span style="width: calc(20%*5)"></span> </div>
                              </div>
                           </div>

                           <div id="ec-resource-12-message-621" class="ec-message">   
                              <div class="number-order"> Номер заявки: #245876 </div>
                              <div class="name">Элина</div>
                              <div class="desc">
                                 <p>Меняла унитаз, старый потек, а я вообще не знаю как это все делается. Вызвала их, сделали все быстро, если бы знала раньше как это делается, сама бы справилась оказывается) В любом случае работу сделали, приехали в день заявки. Потом еще и вниз его спустили, попросила куда-нибудь деть старый, мне -то он больше не нужен. Без проблем утащили куда-то, не возмущались. Новый нормально работает)</p>
                              </div>
                              <div class="services" >
                                 <div class="ser">
                                    <div class="OtNaUs">Отзыв на услугу:</div>
                                    <div class="NameUs"> 
                                    	<span itemprop="itemReviewed">Ремонт сантехники</span> 
                                    </div>
                                 </div>
                                 <div class="ratval"> 
                                 	<span itemprop="ratingValue">5</span> 
                                 	<span itemprop="bestRating">5</span> 
                                 </div>
                                 <div class="ec-stars"> <span style="width: calc(20%*5)"></span> </div>
                              </div>
                           </div>

                           <div id="ec-resource-12-message-619" class="ec-message" itemscope itemtype="http://schema.org/Review">            
                              <div class="number-order"> Номер заявки: #001418 </div>
                              <div class="name" itemprop="author">Владимир</div>
                              <div class="desc" itemprop="description">
                                 <p>Мастер всю работу сделал аккуратно, использовал средства индивидуальной защиты ,ответил на все вопросы. заполнил пакет документов. Все заняло не так много времени как я предполагал.</p>
                              </div>
                              <div class="services" >
                                 <div class="ser">
                                    <div class="OtNaUs">Отзыв на услугу:</div>
                                    <div class="NameUs"> 
                                       <span itemprop="itemReviewed">Поверка счетчиков воды</span> 
                                    </div>
                                 </div>
                                 <div class="ratval"> <span itemprop="ratingValue">5</span> 
                                 	<span itemprop="bestRating">5</span> 
                                 </div>
                                 <div class="ec-stars"> <span style="width: calc(20%*5)"></span> </div>
                              </div>
                           </div>                          
       
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>