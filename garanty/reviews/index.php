<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>

 <?$APPLICATION->IncludeComponent(
     "bitrix:iblock.element.add.form", 
     "reviews", 
     array(
       "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
       "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
       "CUSTOM_TITLE_DETAIL_PICTURE" => "",
       "CUSTOM_TITLE_DETAIL_TEXT" => "",
       "CUSTOM_TITLE_IBLOCK_SECTION" => "",
       "CUSTOM_TITLE_NAME" => "",
       "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
       "CUSTOM_TITLE_PREVIEW_TEXT" => "",
       "CUSTOM_TITLE_TAGS" => "",
       "DEFAULT_INPUT_SIZE" => "30",
       "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
       "ELEMENT_ASSOC" => "CREATED_BY",
       "GROUPS" => array(
         0 => "2",
       ),
       "IBLOCK_ID" => REVIEWS_IBLOCK,
       "IBLOCK_TYPE" => "forms",
       "LEVEL_LAST" => "Y",
       "LIST_URL" => "",
       "MAX_FILE_SIZE" => "0",
       "MAX_LEVELS" => "100000",
       "MAX_USER_ENTRIES" => "100000",
       "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
       "PROPERTY_CODES" => array(
       ),
       "PROPERTY_CODES_REQUIRED" => array(
       ),
       "RESIZE_IMAGES" => "N",
       "SEF_MODE" => "N",
       "STATUS" => "ANY",
       "STATUS_NEW" => "N",
       "USER_MESSAGE_ADD" => "",
       "USER_MESSAGE_EDIT" => "",
       "USE_CAPTCHA" => "N",
       "COMPONENT_TEMPLATE" => "feebdack_up"
     ),
     false
   );?>
   <div class="page-reviews">           

            <div class="content reviews-container">
               <div class="container">
                  <div class="row">

                     <div class="col-sm-12">
                    <?
                    $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "17",
		"IBLOCK_TYPE" => "forms",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "TYPE",
			1 => "NUMBER",
			2 => "RATING",
			3 => "SEARCH_ID",
			4 => "MESSAGE",
			5 => "CLIENT_NAME",
			6 => "APP_NUMBER",
			7 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"TITLE" => "Отзывы клиентов",
		"COMPONENT_TEMPLATE" => "reviews"
	),
	false
);
                    ?>
                  </div>
               </div>
            </div>
         </div>
   </div>
<div class="bottom"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>