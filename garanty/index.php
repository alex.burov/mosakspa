<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы и гарантии");
?>
<h2 class="title-color">Лицензии</h2>
   <!-- LICENSIES --> 
    <section class="section licensies">
         <div class="container">
            <div class="section-content-wrap lic-title">
                             

               <div class="licensies__content flex">

                   <div class="license">
                     <div class="license__image-wrap flex">
                        <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" alt="" class="license__image"></a>
                        <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro-2.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro-2.jpg" alt="" class="license__image"></a>
                     </div>
                     <p class="license__text">Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)</p>
                  </div>

                  <div class="license">
                     <div class="license__image-wrap flex">
                        <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" alt="" class="license__image"></a>
                        <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-2.2.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback3_s" src="/local/static/truba-delo/images/webarch/license-2-2.jpg" alt="" class="license__image"></a>
                     </div>
                     <p class="license__text">Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев</p>
                  </div>

                  <div class="license">
                     <div class="license__image-wrap flex">
                        <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img class="owl-fancy-min" alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" alt="" class="license__image"></a>
                     </div>
                     <p class="license__text">Лицензии МЧС<br />№ 66-Б/01116</p>
                  </div>

               </div>


               <div class="licensies__slider owl-carousel" id="licensies-slider">
                      <div class="license">
                        <div class="license__image-wrap flex">
                           <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" alt="" class="license__image"></a>
                           <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro-2.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro-2.jpg" alt="" class="license__image"></a>
                        </div>
                        <p class="license__text">Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)</p>
                     </div>

                     <div class="license">
                        <div class="license__image-wrap flex">
                           <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" alt="" class="license__image"></a>
                           <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-2.2.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback3_s" src="/local/static/truba-delo/images/webarch/license-2-2.jpg" alt="" class="license__image"></a>
                        </div>
                        <p class="license__text">Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев</p>
                     </div>
                     
                     <div class="license">
                        <div class="license__image-wrap flex">
                           <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img class="owl-fancy-min" alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" alt="" class="license__image"></a>
                        </div>
                        <p class="license__text">Лицензии МЧС<br />№ 66-Б/01116</p>
                     </div>
               </div>

            </div>
         </div>
    </section>
<div class="bottom"></div>
<script type="text/javascript">
            $(document).ready(function(){
            $("#licensies-slider").owlCarousel({
               items: 2
            });
         });

         var owl = $('#licensies-slider');
         owl.owlCarousel({
            items:1,
            center: true,
            loop:true,
            dots:false,
            nav: true,
            autoplay: false,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            navText: ['<span aria-label="Previous">‹</span>','<span aria-label="Next">›</span>']
         });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>