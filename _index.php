<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости банка");
?> 

<!-- контент первый блок-->
      <div class="services-page services-page services-page--web">
            <div class="container">
              <div class="row">
                <div class="col-12 col-md-10 col-lg-9">
                  <p>«АКСПА» – компания основанная в 2012 году и сегодня является одной из успешных и динамично развивающих компаний в сфере установки, замены и поверки счетчиков воды Москвы и Московской области . Также мы предлагаем своим клиентам широкий спектр сантехнических работ любой сложности.
                  </p>
                  <p>Мы являемся официальной организацией по работе с водосчетчиками – это можно проверить на официальном сайте Росаккредитации, аттестат аккредитации №RA.RU.000000.</p>
                </div>
                <div class="col-12 col-md-2 col-lg-3">
                  <img class="card-img-main" src="/local/img/team/user.png">
                </div>
              </div>
            </div>   
        <div class="experience section">
          <div class="container">
            <div class="experience__wrapper">
              <div class="animate-line"><span class="animate-line__one"></span><span class="animate-line__two"></span><span class="animate-line__three"></span><span class="animate-line__four"></span>
              </div>
              <h2 class="experience__title title-color"><span>Наше преимущество это опыт</span>
              </h2>
              <div class="row justify-content-lg-between">
                <div class="col-12 col-md-3 col-lg-3">
                  <div class="experience__item-wrapper"><img class="experience__img" src="/local/img/services/calendar.svg" width="78" alt="" role="presentation"/>
                    <p class="experience__description description">В нашем штате есть сотрудники<br> с опытом работы от 15 лет<br> в сфере сантехнических услуг<br>
                    </p>
                  </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                  <div class="experience__item-wrapper"><img class="experience__img" src="/local/img/services/team.svg" width="80" alt="" role="presentation"/>
                    <p class="experience__description description">Качественные материалы<br>
                      Внимание к мелочам
                    </p>
                  </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                  <div class="experience__item-"><img class="experience__img" src="/local/img/services/platform.svg" width="83" alt="" role="presentation"/>
                    <p class="experience__description description">Работаем по<br> Москве и МО
                    </p>
                  </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                  <div class="experience__item-wrapper"><img class="experience__img" src="/local/img/services/servises.svg" width="84" alt="" role="presentation"/>
                    <p class="experience__description description">Выполнение заказа в день<br>
                        оформления обращения
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="container">
          <h2 class="title-color">Наши услуги</h2>
        </div>
        <!-- второй блок -->
        <div class="research section right">
          <div class="container">
            <div class="research__wrapper">
              <div class="services-page__line services-page__line--head"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-3 order-lg-2">
                  <picture>
                    <img class="research__img" src="/local/img/pov.jpg">
                  </picture>
                </div>

                <div class="col-12 col-lg-9 order-lg-1">
                  <h3 class="research__title title accent">Поверка счетчиков воды
                  </h3>
                  <p class="research__description description">Мастер-метролог сам приезжает к вам на дом в удобное время с переносным портативным стендом Эталон и в течение 10-15 минут делает поверку на работоспособность водосчетчика без демонтажа. Ниже указаны цены на популярные счетчики воды для Москвы</p>
                  <div class="row">
                      <div class="col-md-3">
                        <br> 
                        <br> 
                        <div class="sch-rew">
                        <a data-fancybox="gallery" href="/local/img/akr-1.jpg"><img src="/local/img/akr-1.jpg"></a>
                        </div>
                        <div class="card-body">
                          <p class="card-text">Аттестат аккредитации ООО МС-Ресурс (новый)</p>                        
                        </div>
                      </div>
                      <div class="col-md-3">
                        <br> 
                        <br> 
                        <div class="sch-rew">
                        <a data-fancybox="gallery" href="/local/img/akr-2.jpg"><img src="/local/img/akr-2.jpg"></a>
                          </div>
                          <div class="card-body">
                          <p class="card-text">Аттестат аккредитации ООО МС-Ресурс (старый)</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <a data-fancybox="gallery" href="/local/img/akr-3.jpg"><img src="/local/img/akr-3.jpg"></a>
                          </div>
                             <div class="card-body">
                          <p class="card-text">Область аккредитации ООО МС-Ресурс</p>                       
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <a data-fancybox="gallery" href="/local/img/prikaz-1.jpg"><img src="/local/img/prikaz-1.jpg"></a>
                          </div>
                             <div class="card-body">
                          <p class="card-text">Приказ об аккредитации ООО МС-Ресурс</p>                       
                        </div>
                      </div>
                    </div>
                </div>


              </div>
            </div>
          </div>
        </div>
        <!-- третий блок установка -->
        <div class="technical-task section left">
          <div class="container">
            <div class="technical-task__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--left"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-4">
               
                    <source media="(min-width: 960px)" srcset="/local/img/mont-2.png"><img class="" src="/local/img/mont-2.png">
                       <source media="(min-width: 960px)" srcset="/local/img/mont-4.jpg"><img class="technical-task__img" src="/local/img/mont-4.jpg">
        
                </div>
                <div class="col-12 col-lg-8">
                  <h3 class="technical-task__title title accent">Установка счетчиков воды
                  </h3>
                  <p class="technical-task__description description">Монтаж приборов учета воды производится в соответствии со строительными нормами. Установка пломб на счетчики выполняется обязательно, поскольку без них управляющие организации не вправе произвести регистрацию оборудования. Заводской гарантийный срок на все счетчики – 6 лет.
                  </p>
                    <div class="row">
                      <div class="col-md-2">
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-ekonom.png" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Счетчик Эконом</h4>
                          <p class="card-text">Производство: Россия</p>
                          <p class="card-text">Установка: 1600 руб.</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-itelma.png" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Счетчик Itelma</h4>
                          <p class="card-text">Производство: Германия</p>
                          <p class="card-text">Установка: 1700 руб.</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                                 <div class="sch-rew">
                        <img src="/local/img/sch-valtec.png" class="card-img-top transform-img">
                          </div>
                             <div class="card-body">
                          <h4 class="card-title">Счетчик Valtec</h4>
                          <p class="card-text">Производство: Италия</p>
                          <p class="card-text">Установка: 1800 руб.</p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- четвертый блок замена -->
        <div class="design section right">
          <div class="container">
            <div class="design__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="services-page__line services-page__line--footer"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-4 order-lg-2">
 
              <!-- мини слайдер замена счетчиков-->
              <div class="slideshow-container-replace">
                <div class="slider-mini">
                  <div><img style="padding-left: 0; transform: translateX(0px);" src="/local/img/master.jpg">

                  </div>
                  <div><img style="padding-left: 0;transform: translateX(0px);" src="/local/img/sla-4.jpg">

                  </div>
                  <div><img style="padding-left: 0;transform: translateX(0px);" src="/local/img/sla-3.jpg">

                  </div>
                </div>
              </div>
               <!-- конец мини слайдер -->
                </div>
                <div class="col-12 col-lg-8 order-lg-1">
                  <h3 class="design__title title accent">Замена счетчиков воды
                  </h3>
                  <p class="design__description description">Замена водосчетчиков может понадобиться вам в случае неисправности
                  </p>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-ekonom.png" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Счетчик Эконом</h4>
                          <p class="card-text">Производство: Россия</p>
                          <p class="card-text">Замена: 600 руб.</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/sch-itelma.png" class="card-img-top transform-img">
                          </div>
                          <div class="card-body">
                          <h4 class="card-title">Счетчик Itelma</h4>
                          <p class="card-text">Производство: Германия</p>
                          <p class="card-text">Замена: 600 руб.</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img src="/local/img/sch-valtec.png" class="card-img-top transform-img">
                          </div>
                             <div class="card-body">
                          <h4 class="card-title">Счетчик Valtec</h4>
                          <p class="card-text">Производство: Италия</p>
                          <p class="card-text">Замена: 600 руб.</p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- блок монтажные работы -->
        <div class="container">
          <h2 class="title-color">Монтажные работы</h2>
        </div>
        <div class="tool section left">
          <div class="container">
            <div class="tool__wrapper">
              <div class="services-page__line services-page__line--head"></div>
              <div class="services-page__line services-page__line--left"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-6">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/mont-1.jpg"><img class="tool__img" src="/local/img/mont-1.jpg" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-6">
                  <h3 class="tool__title title accent">Монтаж труб
                  </h3>
                  <p class="tool__description description">Многолетний стаж нашей компании в этой сфере позволил сформировать профессиональную, современную материальную базу. Это позволяет нам проводить работы со всеми видами труб и сантехнического оборудования. Партнерские отношения с постоянными поставщиками определяют высокое качество применяемых материалов, и помогают держать невысокие цены для наших клиентов на расходные материалы.
                  С первого же знакомства с нашими специалистами, вы убедитесь, что без волнения доверите им свое жильё. Все операции проводятся максимально аккуратно, предусмотрена уборка мусора по завершению.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="suggestions section right">
          <div class="container">
            <div class="suggestions__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-6 order-lg-2">
                <!-- мини слайдер -->
              <div class="slideshow-container">
                <div class="slider-mini">
                          <div><img style="padding-left: 0; transform: translateX(0px);" src="/local/img/sla-1.jpg">

                          </div>
                          <div><img style="padding-left: 0;transform: translateX(0px);" src="/local/img/sla-2.jpg">

                          </div>
                          <div><img style="padding-left: 0;transform: translateX(0px);" src="/local/img/sla-3.jpg">

                          </div>
                        </div>
              </div>
               <!-- конец мини слайдер -->
              </div>
                <div class="col-12 col-lg-6 order-lg-1">
                  <h3 class="suggestions__title title accent">Монтаж сантехники
                  </h3>
                  <p class="suggestions__description description">Вы приобрели новую сантехнику или оборудование? Слаженная и профессиональная команда наших сантехников и слесарей в оговоренные сроки проведут установку и подключение к системе водоснабжения и канализации. Соблюдение норм, большой опыт и добросовестное отношение к своему делу позволит гарантировать функционирование всех систем и техники без сбоя. Монтаж сантехники любой сложности производится с учётом проекта водоснабжения объекта и особенностей оборудования различных производителей. Помимо непосредственно монтажа мы окажем помощь в комплектации и  закупке комплектующих и сделаем полную разводку сантехники.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="reserve section left">
          <div class="container">
            <div class="reserve__wrapper">
              <div class="services-page__line services-page__line--top"></div>
              <div class="services-page__line services-page__line--left"></div>
              <div class="services-page__line services-page__line--footer"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-6">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/services/6.jpg"><img class="reserve__img" src="/local/img/services/6.jpg" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-6">
                  <h3 class="reserve__title title accent">Гарантия на все работы
                  </h3>
                  <p class="reserve__description description">Гарантия на все работы
 составляет 1 год с момента выполнения сантехнических услуг
также вы можете заключить дополнительный договор на техническое обслуживание с нашей компанией
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- блок преимущества -->
        <div class="container">
          <h2 class="title-color">АКСПА недежная компания!</h2>
        </div>
        <div class="debugs section right">
          <div class="container">
            <div class="debugs__wrapper">
              <div class="services-page__line services-page__line--head"></div>
              <div class="services-page__line services-page__line--right"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-lg-6 order-lg-2">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/services/7.jpg"><img class="debugs__img" src="/local/img/services/7.jpg" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-lg-6 order-lg-1">
                  <h3 class="debugs__title title accent">Наши преимущества
                  </h3>
                  <p class="debugs__description description">Режим нашей компании обеспечит возможность поиска удобного времени для вас. Вам не придется перестраивать свои планы, отпрашиваться со службы, или терять часы на бесполезные ожидания. При аварийной ситуации выезд осуществляется незамедлительно. Если заявка не требует срочного вмешательства, но вам необходимо ускорить процесс, есть возможность оплатить услугу срочного вызова.

                  Звоните нам, и наша фирма пополнит ваш доверенный список специалистов.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="support section right">
          <div class="container">
            <div class="support__wrapper">
              <div class="services-page__line services-page__line--right"></div>
              <div class="services-page__line services-page__line--footer"></div>
              <div class="row align-items-lg-center">
                <div class="col-12 col-md-4 col-lg-4 order-lg-2">
                  <picture>
                    <source media="(min-width: 960px)" srcset="/local/img/happy-client.jpg"><img class="support__img" src="/local/img/happy-client.jpg" alt="1" title=""/>
                  </picture>
                </div>
                <div class="col-12 col-md-8 col-lg-8 order-lg-1">
                  <h3 class="support__title title accent">Как мы работаем
                  </h3>
                  <p class="support__description description"></p>
                    <div class="row">
                       <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/call.png" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">Звонок</h5>
                          <p class="card-text">Вы оставляете заявку на сайте или по телефону получайте консультацию от нашего оператора</p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/master1.jpg" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">Приезд мастера</h5>
                          <p class="card-text">В назначенное время к Вам приедет мастер  для замера и составления точной сметы </p>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img  src="/local/img/dogovor.jpg" class="card-img-top transform-img">
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">Договор</h5>
                          <p class="card-text">Вы подписывайте договор в соответствии с составленной сметой</p>                   
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="sch-rew">
                        <img src="/local/img/sch-valtec.png" class="card-img-top transform-img">
                          </div>
                             <div class="card-body">
                          <h5 class="card-title">Выполнение</h5>
                          <p class="card-text">Мастер приступает к работе. Расчет производится после окончательной сдачи работ</p>
                        </div>
                      </div>
                    </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container"><a class="services-page__portfolio-link accent" href="#">Посмотреть наши работы<span class="decor"> >></span></a>
        </div>
      </div>
      <!-- конец контента-->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>