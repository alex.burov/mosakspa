$(document).ready(function() {
	
	function showBeforeUnload()
	{
		$(".showBeforeUnload").mouseleave(function(e){
			if (!localStorage.userCatched && !localStorage.discountViewed && ($("#modal-discount").css("display") == 'none' && $(".overlay").css("display") == 'none'))
			{
				Modal.showModal($("#modal-discount"));
				localStorage.discountViewed = 1;
			}
		});
	}

	showBeforeUnload();	
	$('input[type="tel"]').mask('+7 (999) 999-99-99');

});


