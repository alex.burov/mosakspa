$(document).ready(function() {
	
	// РІСЃРїР»С‹РІР°СЋС‰Р°СЏ С„РѕСЂРјР° РїСЂРё Р·Р°РєСЂС‹С‚РёРё РІРєР»Р°РґРєРё
	function showBeforeUnload()
	{
		$(".showBeforeUnload").mouseleave(function(e){
			if (!localStorage.userCatched && !localStorage.discountViewed && ($("#modal-discount").css("display") == 'none' && $(".overlay").css("display") == 'none'))
			{
				Modal.showModal($("#modal-discount"));
				localStorage.discountViewed = 1;
			}
		});
	}

	showBeforeUnload();
	
	$('input[type="tel"]').mask('+7 (999) 999-99-99');

	// var delay_popup = 1000;
	// setTimeout("document.getElementById('modal-discount').style.display='block'", delay_popup);

////////////////////////////////////////////////
//////////////////  SLIDERS  ///////////////////
////////////////////////////////////////////////

// РЈСЃР»СѓРіРё
	$('#service-slider').slick({
			infinite: true,
			slidesToShow: 1,
			speed: 1000,
			prevArrow: '.service-slider__arrow_prev',
			nextArrow: '.service-slider__arrow_next ',
			dots: false,
			autoplay: false,
			autoplaySpeed: 5000,
		});

// Р’РёРґРµРѕ-РєРѕРјРјРµРЅС‚Р°СЂРёРё
$('#comments-slider').slick({
	infinite: true,
	slidesToShow: 3,
	speed: 600,
	prevArrow: '.comments__arrow_prev',
	nextArrow: '.comments__arrow_next ',
	dots: true,
	dotsClass: 'slider__dots',
	responsive: [
		{
			breakpoint: 960,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 525,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
	]
});

$('#comments-slider-by-one').slick({
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	speed: 600,
	prevArrow: '.comments__arrow_prev',
	nextArrow: '.comments__arrow_next ',
	dots: false,
});

// РќР°С€Р° РєРѕРјР°РЅРґР°
	$(document).ready(function(){
		$("#crew-slider").owlCarousel({
			items: 2
		});
	});

	var owl = $('#crew-slider');
	owl.owlCarousel({
		items:1,
		center: true,
		loop:true,
		dots:false,
		nav: true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});


	// Р“Р°СЂР°РЅС‚РёРё
	$(document).ready(function(){
		$("#cards-slider").owlCarousel({
			items: 2
		});
	});

	var owl = $('#cards-slider');
	owl.owlCarousel({
		items:1,
		center: true,
		loop:true,
		dots:false,
		nav: true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});

	// Р›РёС†РµРЅР·РёРё
	$(document).ready(function(){
		$("#licensies-slider").owlCarousel({
			items: 2
		});
	});

	var owl = $('#licensies-slider');
	owl.owlCarousel({
		items:1,
		center: true,
		loop:true,
		dots:false,
		nav: true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});

	// РЈСЃР»СѓРіРё 1
	$(document).ready(function(){
		$("#service-cards_slider").owlCarousel({
			items: 1
		});
	});

	var owl = $('#service-cards_slider');
	owl.owlCarousel({
		items:1,
		center: false,
		loop:true,
		dots:false,
		nav: true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});


	// РќР°РїСЂР°РІР»РµРЅРёСЏ
	$(document).ready(function(){
		$("#directions_slider").owlCarousel({
			items: 1
		});
	});

	var owl = $('#directions_slider');
	owl.owlCarousel({
		items:1,
		center: true,
		loop:true,
		dots:false,
		nav: true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});



////////////////////////////////////////////////
///////////////////  MODALS ////////////////////
////////////////////////////////////////////////
var Modal = {
	elemShowingModal: [$('.header-contact__button, .service-modal__button')],
	elemHidingModal: [$('.wait__call'), $('.modal__close'), $('.overlay')],
	overlay: $('.overlay'),
	window: $(window),
	windowSize: {
		windowWidth: null,
		windowHeight: null
	},
	modal: null,
	modalSize: {
		modalHeight: null,
		modalWidth: null
	},
	getModalSize: function() {
		this.modalSize.modalHeight = this.modal.outerHeight();
		this.modalSize.modalWidth = this.modal.outerWidth();
		//console.log(this.modal)
		//console.log(this.modal.innerWidth())
		//console.log(this.modal.width())
		this.setPosition();
	},
	setPosition: function(modal) {
		this.windowSize.windowWidth = this.window.width();
		this.windowSize.windowHeight = this.window.height();
		var htmlScroll = $('html').scrollTop(),
				bodyScroll = $('body').scrollTop(),
				scroll,
				left,
				top;
		(htmlScroll) ? scroll = htmlScroll : scroll = bodyScroll;
		if (this.windowSize.windowWidth > this.modalSize.modalWidth) {
			left = (this.windowSize.windowWidth - this.modalSize.modalWidth) * 0.5;
		} else {
			left = 0;
		}
		
		if (this.windowSize.windowHeight > this.modalSize.modalHeight) {
			top = scroll + (this.windowSize.windowHeight - this.modalSize.modalHeight) * 0.5;
		} else {
			top = scroll + 10;
		}
		this.modal.css({'top':top, 'left':left})
	},
	showModal: function(modal) {
		this.modal = modal;
		this.getModalSize();
		this.overlay.fadeIn();
		this.modal.fadeIn().addClass('modal_active');
	},
	closeModal: function() {
		this.overlay.fadeOut();
		this.modal.fadeOut().removeClass('modal_active');
		this.modal.find(".successfull_container_text_accepted").html('');
	}
}

window.Modal = Modal;
	
	// Show Modals
	for (var i = 0; i < Modal.elemShowingModal.length; i++) {
		Modal.elemShowingModal[i].click(function(e) {
			var modal = $('#' + $(e.target).attr('data-link'));
			Modal.showModal(modal);
		})
	}

	// Close Modals
	for (var i = 0; i < Modal.elemHidingModal.length; i++) {
		Modal.elemHidingModal[i].click(function() {
			Modal.closeModal();
		})
	}

	// Reposition Modals
	$(window).resize(function() {
		if (Modal.modal) {
			setTimeout(function() {
				Modal.setPosition()	
			}, 500);	
		}
	});

/////////////////////////////////////////////////////
///////////////////  MODAL-POPUP ////////////////////
/////////////////////////////////////////////////////

	document.getElementById('modal-popup').onclick = function() {
		document.getElementById('modal-garant1').classList.add('modal_none');
		document.getElementsByClassName('overlay').classList.add('modal_none');
	}

	document.getElementById('modal-popup2').onclick = function() {
		document.getElementById('modal-garant2').classList.add('modal_none');
	}

	document.getElementById('modal-popup3').onclick = function() {
		document.getElementById('modal-garant3').classList.add('modal_none');
	}

	document.getElementById('hid').onclick = function() {
		document.getElementById('modal-garant1').classList.remove('modal_none');
	}

	document.getElementById('btn_hid').onclick = function() {
		document.getElementById('modal-garant1').classList.remove('modal_none');
	}

	document.getElementById('hid2').onclick = function() {
		document.getElementById('modal-garant2').classList.remove('modal_none');
	}

	document.getElementById('btn_hid2').onclick = function() {
		document.getElementById('modal-garant2').classList.remove('modal_none');
	}

	document.getElementById('hid3').onclick = function() {
		document.getElementById('modal-garant3').classList.remove('modal_none');
	}

	document.getElementById('btn_hid3').onclick = function() {
		document.getElementById('modal-garant3').classList.remove('modal_none');
	}

////////////////////////////////////////////////
///////////////////  UNFOLD ////////////////////
////////////////////////////////////////////////
	var Description = {
		articles: $('.description__content').eq(0),
		article: $('.description__article'),
		//////
		articlesUnfoldHeight: 0,
		articlesFoldHeight: 0,
		//////
		unfoldBlock: $('.text-unfold'),
		btn: $('.text-unfold__btn'),
		text: $('.text-unfold__text'),

		fold:  function() {
			this.btn.removeClass('active');
			this.text.text('Р§РёС‚Р°С‚СЊ РґР°Р»РµРµ');

			this.articles.animate({
				height: this.articlesFoldHeight
			}, 1500)
		},
		unfold: function() {
			this.btn.addClass('active');
			this.text.text('РЎРІРµСЂРЅСѓС‚СЊ');

			this.articles.animate({
				height: this.articlesUnfoldHeight
			}, 1500);
			
			setTimeout(function(){
				$('.description__content').css({"height":"100%"});
			}, 1600);

		},

	}
	// РЎРѕС…СЂРµРЅРµРЅРёРµ РЅР°С‡Р°Р»СЊРЅРѕР№ РІС‹СЃРѕС‚С‹ РєРѕРЅС‚РµР№РЅРµСЂР° Рё СЃР¶Р°С‚РёРµ РґРѕ РІС‹СЃРѕС‚С‹ 2-С… СЃС‚Р°С‚РµР№
	Description.articlesUnfoldHeight = Description.articles.outerHeight(true);
	for (var i = 0; i < 1; i++) {	
		Description.articlesFoldHeight += $(Description.article[i]).outerHeight(true);
	}
	Description.articles.height(Description.articlesFoldHeight);
	// 

	Description.unfoldBlock.click(function() {
		if (!Description.btn.hasClass('active')) {
			Description.unfold();
		} else {
			Description.fold();
		}
	})

////////////////////////////////////////////////
////////////  SCROLLING MENU  ///////////
////////////////////////////////////////////////
	$(document).scroll (function () {
		if ($(document).scrollTop () >$('nav').height () +1)
			$('.nav-header-mob').addClass ('fixed-mob');
		else
			$('.nav-header-mob').removeClass ('fixed-mob');
	});

	// $(document).scroll (function () {
	// 	if ($(document).scrollTop () >$('header').height () +1)
	// 		$('.header').addClass ('fixed');
	// 	else
	// 		$('.header').removeClass ('fixed');
	// });

	$(document).scroll (function () {
		if ($(document).scrollTop () >$('header').height () +1)
			$('.nav_fix').addClass ('fixed');
		else
			$('.nav_fix').removeClass ('fixed');
	});



////////////////////////////////////////////////
////////////////////  SCROLL ///////////////////
////////////////////////////////////////////////
	// $('').click(function(e){
	// 	e.preventDefault();
	// 	var destId = $(this).children().attr('href').substr(0)
	// 	$('html, body').animate({
	// 		scrollTop: $(destId).offset().top
	// 	}, 700);
	// });

	//
	//
	// var km = $('.km-item_mark'),
	// 	mk = $('.km_hover'),
	// 	gar = $('.km_img');
	//
	// km.hover(
	// 	function() {
	// 		mk.css('display', 'initial');
	// 	},
	// 	function() {
	// 		gar.css('background', '#e01f1f');
	// 	},
	// 	function() {
	// 		mk.removeAttr('style');
	// 	});
  	//
	//

	var ekb = $('.ekb-item_mark'),
		bk = $('.ekb_hov');

	ekb.hover(
		function(){
			bk.css(
				'display', 'initial'
			);
		},
		function(){
			bk.removeAttr('style');
		});

	var eburg = $('.ekb-item_mark'),
		akab = $('.ekb_hover');

	eburg.hover(
		function(){
			akab.css(
				'display', 'initial'
			);
		},
		function(){
			akab.removeAttr('style');
		});

	var mg = $('.mg-item_mark'),
		gm = $('.mg_hover');

	mg.hover(
		function(){
			gm.css(
				'display', 'initial'
			);
		},
		function(){
			gm.removeAttr('style');
		});

	var see = $('.che-item_mark'),
		ees = $('.che_hover');

	see.hover(
		function(){
			ees.css(
				'display', 'initial'
			);
		},
		function(){
			ees.removeAttr('style');
		});

	var first = $('.sr-item_mark'),
		sr = $('.sr_hover');

	first.hover(
		function(){
			sr.css(
				'display', 'initial'
			);
		},
		function(){
			sr.removeAttr('style');
		});

	var ven = $('.nev-item_mark'),
	nev = $('.nev_hover');

	ven.hover(
		function() {
			nev.css('display', 'initial');
		},
		function() {
			nev.removeAttr('style');
		});

	var rep = $('.per-item_mark'),
	per = $('.per_hover');

	rep.hover(
		function() {
			per.css('display', 'initial');
		},
		function() {
			per.removeAttr('style');
		});

	var rev = $('.rev-item_mark'),
	ver = $('.rev_hover');

	rev.hover(
		function() {
			ver.css('display', 'initial');
		},
		function() {
			ver.removeAttr('style');
		});

	var kr = $('.kr-item_mark'),
	rk = $('.kr_hover');

	kr.hover(
		function() {
			rk.css('display', 'initial');
		},
		function() {
			rk.removeAttr('style');
		});

	var nov = $('.nov-item_mark'),
	von = $('.nov_hover');

	nov.hover(
		function() {
			von.css('display', 'initial');
		},
		function() {
			von.removeAttr('style');
		});

	var rezh = $('.rezh-item_mark'),
	zer = $('.rezh_hover');

	rezh.hover(
		function() {
			zer.css('display', 'initial');
		},
		function() {
			zer.removeAttr('style');
		});

	var art = $('.art-item_mark'),
	tar = $('.art_hover');

	art.hover(
		function() {
			tar.css('display', 'initial');
		},
		function() {
			tar.removeAttr('style');
		});

	var km = $('.km-item_mark'),
	mk = $('.km_hover');

	km.hover(
		function() {
			mk.css('display', 'initial');
		},
		function() {
			mk.removeAttr('style');
		});

	var paf = $('.nov_mark'),
	vap = $('.nov-item_mark');

	paf.hover(
		function() {
			vap.css('color', '#e01f1f');
		},
		function() {
			vap.removeAttr('style');
		});

	var kon = $('.che_mark'),
	ron = $('.che-item_mark');

	kon.hover(
		function() {
			ron.css('color', '#e01f1f');
		},
		function() {
			ron.removeAttr('style');
		});

	var miki = $('.km_mark'),
	rurg = $('.km-item_mark');

	miki.hover(
		function() {
			rurg.css('color', '#e01f1f');
		},
		function() {
			rurg.removeAttr('style');
		});

	var vait = $('.mg_mark'),
	tap = $('.mg-item_mark');

	vait.hover(
		function() {
			tap.css('color', '#e01f1f');
		},
		function() {
			tap.removeAttr('style');
		});

	var rad = $('.art_mark'),
	dar = $('.art-item_mark');

	rad.hover(
		function() {
			dar.css('color', '#e01f1f');
		},
		function() {
			dar.removeAttr('style');
		});

	var loot = $('.sr_mark'),
	tool = $('.sr-item_mark');

	loot.hover(
		function() {
			tool.css('color', '#e01f1f');
		},
		function() {
			tool.removeAttr('style');
		});

	var hot = $('.rezh_mark'),
	toh = $('.rezh-item_mark');

	hot.hover(
		function() {
			toh.css('color', '#e01f1f');
		},
		function() {
			toh.removeAttr('style');
		});

	var edg = $('.nov_mark'),
	gde = $('.nov-item_mark');

	edg.hover(
		function() {
			gde.css('color', '#e01f1f');
		},
		function() {
			gde.removeAttr('style');
		});

	var chil = $('.rev_mark'),
	cheel = $('.rev-item_mark');

	chil.hover(
		function() {
			cheel.css('color', '#e01f1f');
		},
		function() {
			cheel.removeAttr('style');
		});

	var joji = $('.per_mark'),
	jay = $('.per-item_mark');

	joji.hover(
		function() {
			jay.css('color', '#e01f1f');
		},
		function() {
			jay.removeAttr('style');
		});

	var fat = $('.nev_mark'),
	stuf = $('.nev-item_mark');

	fat.hover(
		function() {
			stuf.css('color', '#e01f1f');
		},
		function() {
			stuf.removeAttr('style');
		});

	var dan = $('.kr_mark'),
	ned = $('.kr-item_mark');

	dan.hover(
		function() {
			ned.css('color', '#e01f1f');
		},
		function() {
			ned.removeAttr('style');
		});

	var kol = $('.ekb_mark'),
	lod = $('.ekb-item_mark');

	kol.hover(
		function() {
			lod.css('color', '#e01f1f');
		},
		function() {
			lod.removeAttr('style');
		});


////////////////////////////////////////////////
////////////////////  MOBILE MENU ///////////////////
////////////////////////////////////////////////

$(".nav-header-mob > a").click(function () {
	$(this).next('ul').slideToggle();
	return false;
});

$(".nav-header-mob ul li a").click(function () {
	$(this).next('ul').slideToggle();
	$(this).next('li').toggle();
	return false;
});

$(".nav-header-mob ul li ul").click(function () {
	$(this).next('ul#lt');
	return false;
});

$(".nav-header-mob ul li ul i.fa-chevron-left").click(function () {
	$("ul#lt").hide();
	return false;
});

$(".nav-header-mob ul i.fa-times").click(function () {
	$(".nav-header-mob ul").hide();
	return false;
});

$("#title-btn").click(function () {
	$('.modal-call__header-title').removeClass('active-title');
	$('#new-title').addClass('active-title');
});

	//function setHeiHeight() {
	//	$('.banner').css({
	//		height: $(window).height() + 'px'
	//	});
	//}
	//setHeiHeight(); // СѓСЃС‚Р°РЅР°РІР»РёРІР°РµРј РІС‹СЃРѕС‚Сѓ РѕРєРЅР° РїСЂРё РїРµСЂРІРѕР№ Р·Р°РіСЂСѓР·РєРµ СЃС‚СЂР°РЅРёС†С‹
	//$(window).resize( setHeiHeight ); // РѕР±РЅРѕРІР»СЏРµРј РїСЂРё РёР·РјРµРЅРµРЅРёРё СЂР°Р·РјРµСЂРѕРІ РѕРєРЅР°

});

// $(document).ready(function(){
// 	$('.nav-header-mob ul li').hover(
// 		function() {
// 			$(this).find('ul').slideDown();
// 		},
// 		function() {
// 			$(this).find('ul').slideUp('fast');
// 		}
// 	);
// });

////////////////////////////////////////////////
////////////////////  CLICK ///////////////////
////////////////////////////////////////////////
function toggle(el) {
	el.style.display = (el.style.display == 'none') ? '' : 'none'
}

////////////////////////////////////////////////
////////////////////  FORM SEND ///////////////////
////////////////////////////////////////////////

// $(document).ready(function(){
	// $(function(d,w,k){w.morecrm_callback=function(){try{w.MI=new MorecrmIntegration(k);}catch(e){console.log(e)}};var n=d.getElementsByTagName("script")[0],e=d.createElement("script");e.type="text/javascript";e.async=true;e.src="https://morecrm.ru/integration/site/"+k+".js";n.parentNode.insertBefore(e,n);})(document,window,'1ff1de774005f8da13f42943881c655f');
// });

function sendForm(btn)
{
	$el=$(btn).parents("[data-sender]");
	$name=$el.attr("id");
	$.ajax({
		"type":"POST",
		"url":$el.find("form").attr("action"),
		data:$el.find("form").serialize(),
		beforeSend:function(data){
			$el.find("button").attr("disabled","disabled");
			$el.find(".modal-call__header-title").text("РРґРµС‚ РѕС‚РїСЂР°РІРєР°...");
		},
		success:function(data){
			if($name=='prompt2')
			{
				params_form={
					tel:$('#accident #accidentwebarch_phone').val(),
				};
				name_form='form_accidentwebarch';
			}
			else if($name=='prompt3')
			{
				params_form={
					leadname:$('#prompt3 #feedback_name').val(),
					tel:$('#prompt3 #feedback_phone').val(),
					desc:$el.find("#prompt3 #feedback_text").val()
				};
				name_form='form_servicewebarch';
			}
			else if($name=='cloggingform')
			{
				params_form={
					tel:$('#accident #cloggingwebarch_phone').val(),
				};
				name_form='form_cloggingwebarch';
			}
			else if($name=='sendorder')
			{
				params_form={
					leadname:$('#sendorder #orderwebarch_name').val(),
					tel:$('#sendorder #orderwebarch_phone').val(),
				};
				name_form='form_sendorder';
			}
			else if($name=='discountorder')
			{
				params_form={
					leadname:$('#discountorder #discountwebarch_name').val(),
					tel:$('#discountorder #discountwebarch_phone').val(),
					desc:$el.find("#discountorder #discountwebarch_text").val()
				};
				name_form='form_discountorder';
			}
			else if($name=='colsultform')
			{
				params_form={
					tel:$('#consultform #consultwebarch_phone').val(),
				};
				name_form='form_consult';
			}
			else if($name=='colsult2form')
			{
				params_form={
					leadname:$('#consult2form #consult2webarch_name').val(),
					tel:$('#consult2form #consult2webarch_phone').val(),
					desc:$el.find("#consult2form #consult2webarch_text").val()
				};
				name_form='form_discountorder';
			}
			else if($name=='directorletter')
			{
				params_form={
					leadname:$('#directorletter #directorwebarch_name').val(),
					tel:$('#directorletter #directorwebarch_phone').val(),
					email:$('#directorletter #directorwebarch_email').val(),
					desc:$el.find("#directorletter #directorwebarch_text").val()
				};
				name_form='form_discountorder';
			}
			else if($name=='buyfranchise')
			{
				params_form={
					leadname:$('#buyfranchise #franchisewebarch_name').val(),
					tel:$('#buyfranchise #franchisewebarch_phone').val(),
					email:$('#buyfranchise #franchisewebarch_email').val(),
					desc:$el.find("#buyfranchise #franchisewebarch_text").val()
				};
				name_form='form_discountorder';
			}
			$el.html(data);
			$('input[type="tel"]').mask('+7 (999) 999-99-99');
			if($el.find(".error_list").length==false)
			{
				$el.parents(".modal-wrap").fadeOut(200);
				Modal.showModal($("#modal-thanks"));
				//$el.find('.successfull_container_text_accepted').css("display","block");
			}
			//console.log(params_form);
			//MI.handler(params_form);
		}
	});
	return false;
}

$(document).ready(function(){

	//РРєРѕРЅРєРё СѓСЃР»СѓРі
    var h2_icon_1 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 40.52 39.94"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РђРІР°СЂРёР№РЅР°СЏ СЃР»СѓР¶Р±Р°</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M0,16.49a4.66,4.66,0,0,1,.47-.8,1.66,1.66,0,0,1,1.68-.48l4.59.91a1.68,1.68,0,0,1,1.11,2.73,1.59,1.59,0,0,1-1.65.58c-1.56-.29-3.1-.62-4.66-.91A1.94,1.94,0,0,1,0,17.14Z"/><path class="cls-1" d="M30.65,22.48A10.8,10.8,0,0,0,30,18.64a10.15,10.15,0,0,0-3.07-4.32A10,10,0,0,0,22,12.11a10.82,10.82,0,0,0-4.3.15,10.14,10.14,0,0,0-4.41,2.33A10.27,10.27,0,0,0,11,17.5a10.66,10.66,0,0,0-1.18,5.08c0,4,0,7.94,0,11.91v.46H30.65v-.36Q30.65,28.53,30.65,22.48Zm-3.88-.14A8.46,8.46,0,0,0,19.6,13.9a7.7,7.7,0,0,1,1.19-.09,8.45,8.45,0,0,1,8.37,8.52Z"/><path class="cls-1" d="M7.33,39.94l.57-1.77c.2-.61.42-1.22.6-1.84A.4.4,0,0,1,9,36H31.57a.37.37,0,0,1,.43.31c.35,1.14.74,2.27,1.11,3.41,0,.07,0,.14.06.25Zm1.11-.75.2,0c1.29,0,2.58,0,3.88,0a.59.59,0,0,0,.35-.18c.68-.67,1.36-1.35,2-2,.07-.07.13-.15.19-.23l-.06,0H9.41a.25.25,0,0,0-.19.12C8.95,37.63,8.7,38.4,8.44,39.19Z"/><path class="cls-1" d="M32.91,4.71a1.52,1.52,0,0,1,1.2.87A1.53,1.53,0,0,1,34,7.34c-.8,1-1.65,1.91-2.47,2.85l-.88,1A1.68,1.68,0,1,1,28.13,9l3.18-3.61A1.85,1.85,0,0,1,32.91,4.71Z"/><path class="cls-1" d="M18.82,4.12c0-.81,0-1.62,0-2.42a1.59,1.59,0,0,1,1-1.52,1.6,1.6,0,0,1,1.74.18,1.49,1.49,0,0,1,.64,1.22c0,1.71,0,3.43,0,5.14a1.61,1.61,0,0,1-1.71,1.6,1.66,1.66,0,0,1-1.62-1.71c0-.83,0-1.66,0-2.49Z"/><path class="cls-1" d="M38.74,16a1.7,1.7,0,0,1,1.77,1.46,1.63,1.63,0,0,1-1.26,1.81c-1.26.23-2.53.41-3.8.61-.44.07-.87.14-1.31.18a1.61,1.61,0,0,1-1.71-.91,1.63,1.63,0,0,1,.26-1.87,1.77,1.77,0,0,1,1.13-.56l4.6-.7Z"/><path class="cls-1" d="M13.17,9.61a1.67,1.67,0,0,1-1,1.61,1.61,1.61,0,0,1-1.86-.4C9.56,10,8.86,9.07,8.15,8.19A19.26,19.26,0,0,1,7,6.65a1.62,1.62,0,0,1,.47-2.23,1.66,1.66,0,0,1,2.18.25c.59.67,1.13,1.38,1.69,2.08l1.41,1.77A1.66,1.66,0,0,1,13.17,9.61Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-1'))
	{
		document.getElementById('h2-icon-1').innerHTML = h2_icon_1;
	}

    var h2_icon_2 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 26.16 40.33"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РЈСЃС‚СЂР°РЅРµРЅРёРµ Р·Р°СЃРѕСЂРѕРІ</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M13,24.56A11.29,11.29,0,0,0,1.79,36H24.1A11.28,11.28,0,0,0,13,24.56ZM11.8,25.76A7.84,7.84,0,0,1,13,25.67a8.62,8.62,0,0,1,8.53,8.69H19.1A8.62,8.62,0,0,0,11.8,25.76Z"/><path class="cls-1" d="M26.16,38.83a1.49,1.49,0,0,1-1.5,1.5H1.5A1.49,1.49,0,0,1,0,38.83H0a1.49,1.49,0,0,1,1.5-1.5H24.65a1.49,1.49,0,0,1,1.5,1.5Z"/><path class="cls-1" d="M13,0c-1.48,0-2.7.74-2.7,1.66,0,.61.55,13.12,1.35,21.31.44,0,.89-.06,1.35-.06s.88,0,1.33.06c.82-8.19,1.34-20.7,1.34-21.31C15.63.74,14.44,0,13,0ZM14,2.57c0-.68-.88-1.25-2-1.25a3.15,3.15,0,0,0-1,.18,2.22,2.22,0,0,1,1.9-.83c1.1,0,2,.55,2,1.24,0,.34-.22,5.67-.6,10.91C14.35,7.81,14,2.9,14,2.57Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-2'))
	{
		document.getElementById('h2-icon-2').innerHTML = h2_icon_2;
	}

    var h2_icon_3 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 44.03 25.03"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РЎРёСЃС‚РµРјС‹ РѕС‚РѕРїР»РµРЅРёСЏ</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M22,13c-1.21,3.6-3.6,6-3.6,8.38,0,4.81,7.2,4.81,7.2,0C25.64,19,23.23,16.64,22,13Zm0,10.8c3.6-2.42,0-4.81,0-9.62,0,2.42,2.39,4.81,2.39,7.2A2.35,2.35,0,0,1,22,23.84Z"/><path class="cls-1" d="M4.85,0H3C1.34,0,0,3.57,0,8s1.33,8,3,8H4.84c-1.64,0-3-3.57-3-8S3.2,0,4.85,0Z"/><path class="cls-1" d="M6,12.52C5,11.83,4.54,9.76,4.54,8S5,4.11,6,3.44l.82,0H8C7.5,1.34,6.61,0,5.59,0c-1.64,0-3,3.57-3,8s1.34,8,3,8c1,0,1.91-1.34,2.45-3.42Z"/><path class="cls-1" d="M6.49,1.8c0,.54-.31,1-.69,1s-.7-.43-.7-1,.31-1,.7-1S6.49,1.27,6.49,1.8Z"/><path class="cls-1" d="M6.49,14c0,.51-.31,1-.69,1s-.7-.46-.7-1,.31-1,.7-1S6.49,13.47,6.49,14Z"/><path class="cls-1" d="M4.22,7.63c0,.54-.26,1-.57,1s-.56-.43-.56-1,.26-1,.56-1S4.22,7.13,4.22,7.63Z"/><path class="cls-1" d="M39.2,0H41c1.65,0,3,3.57,3,8s-1.33,8-3,8H39.2c1.64,0,3-3.57,3-8S40.84,0,39.2,0Z"/><path class="cls-1" d="M38.09,12.52c1-.7,1.4-2.77,1.4-4.57S39,4.11,38.09,3.44l-.83,0H36C36.54,1.34,37.44,0,38.44,0c1.65,0,3,3.57,3,8s-1.33,8-3,8c-1,0-1.91-1.34-2.44-3.42Z"/><path class="cls-1" d="M37.54,1.8c0,.54.31,1,.7,1s.7-.43.7-1-.32-1-.7-1S37.54,1.27,37.54,1.8Z"/><path class="cls-1" d="M37.54,14c0,.51.31,1,.7,1s.7-.46.7-1-.32-1-.7-1S37.54,13.47,37.54,14Z"/><path class="cls-1" d="M39.83,7.63c0,.54.24,1,.55,1s.56-.43.56-1-.26-1-.56-1S39.83,7.13,39.83,7.63Z"/><path class="cls-1" d="M37.23,4.35H6.83C6.09,4.35,5.48,6,5.48,8s.6,3.63,1.34,3.63h13l-.44-1.45,3.37-1.37L21.07,6.18l4,3.2-2.82,1,1.69,1.19H37.23C38,11.59,38.59,10,38.59,8S38,4.35,37.23,4.35Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-3'))
	{
		document.getElementById('h2-icon-3').innerHTML = h2_icon_3;
	}

    var h2_icon_4 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 20.55 34.27"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>Р’РѕРґРѕСЃРЅР°Р±Р¶РµРЅРёРµ</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M10.29,0C6.86,10.29,0,17.12,0,24,0,37.7,20.55,37.7,20.55,24,20.55,17.12,13.72,10.29,10.29,0Zm0,30.83c10.26-6.86,0-13.72,0-27.41,0,6.86,6.83,13.69,6.83,20.55S10.29,30.83,10.29,30.83Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-4'))
	{
		document.getElementById('h2-icon-4').innerHTML = h2_icon_4;
	}

    var h2_icon_5 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 39.34 24.15"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РљР°РЅР°Р»РёР·Р°С†РёСЏ</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M4.32,7.55H2.65C1.19,7.55,0,11.25,0,15.84s1.19,8.31,2.65,8.31H4.32c-1.48,0-2.67-3.72-2.67-8.31S2.85,7.55,4.32,7.55Z"/><path class="cls-1" d="M5.31,20.57C4.44,19.87,4,17.71,4,15.84s.4-4,1.26-4.73H7.18C6.7,9,5.9,7.55,5,7.55c-1.48,0-2.67,3.7-2.67,8.28S3.52,24.15,5,24.15c.9,0,1.71-1.43,2.19-3.58Z"/><path class="cls-1" d="M35,7.55h1.66c1.48,0,2.67,3.7,2.67,8.28s-1.19,8.31-2.65,8.31H35c1.48,0,2.67-3.72,2.67-8.31S36.49,7.55,35,7.55Z"/><path class="cls-1" d="M34.35,7.55c-.9,0-1.71,1.4-2.19,3.56H34c.86.73,1.25,2.88,1.25,4.73s-.38,4-1.25,4.73H32.16c.48,2.15,1.28,3.58,2.19,3.58,1.48,0,2.67-3.72,2.67-8.31S35.82,7.55,34.35,7.55Z"/><path class="cls-1" d="M26.74,10.07a25.28,25.28,0,0,1-7.07,1,25.3,25.3,0,0,1-7.07-1,7.67,7.67,0,0,0-2.29,5.4c0,4.56,4.19,8.28,9.37,8.28S29,20,29,15.48A7.67,7.67,0,0,0,26.74,10.07Zm-7.36,12a8.13,8.13,0,0,1-7.51-4.51,8.42,8.42,0,0,0,6.66,3.11c4.47,0,8.08-3.19,8.08-7.14a6.26,6.26,0,0,0-.56-2.6,6.5,6.5,0,0,1,1.41,4C27.47,18.89,23.85,22.08,19.38,22.08Z"/><path class="cls-1" d="M10.47,19.62A8.62,8.62,0,0,1,9.4,15.48a8.41,8.41,0,0,1,.71-3.39h-4c-.66,0-1.2,1.68-1.2,3.75s.54,3.78,1.2,3.78c0,0,0,0,0,0v0Z"/><path class="cls-1" d="M33.27,12.09h-4a8.59,8.59,0,0,1,.7,3.39,8.75,8.75,0,0,1-1.06,4.14h4.39c.67,0,1.21-1.71,1.21-3.78S33.94,12.09,33.27,12.09Z"/><path class="cls-1" d="M19.67,0C14.05,0,8,1.65,8,5.26s6,5.23,11.66,5.23S31.33,8.87,31.33,5.26,25.29,0,19.67,0Zm1,3.16a3.07,3.07,0,0,0-1-.17,3.46,3.46,0,0,0-2.16.78L14.57,3a23.85,23.85,0,0,1,5.1-.5c.64,0,1.24,0,1.8.06ZM18.33,6.8a4.94,4.94,0,0,0,1.34.2,4.3,4.3,0,0,0,2.19-.53l3.48,1a22.6,22.6,0,0,1-5.67.64c-1,0-1.9-.06-2.74-.14Zm4.39-1.59A2.12,2.12,0,0,0,22,3.89l1.47-1.18c3.78.59,5.8,1.9,5.8,2.55,0,.31-.52.81-1.5,1.28ZM10.1,5.26c0-.36.65-1,1.9-1.49L16.65,5a.73.73,0,0,0,0,.23A1.2,1.2,0,0,0,17,6.1L15.06,7.64C11.82,7,10.1,5.85,10.1,5.26Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-5'))
	{
		document.getElementById('h2-icon-5').innerHTML = h2_icon_5;
	}

    var h2_icon_6 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 36.47 39.59"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РЈСЃС‚Р°РЅРѕРІРєР° СЃР°РЅС‚РµС…РЅРёРєРё</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M35.38,19.56c-7.07,0-14.52.19-21.28-2.16-5-1.73-3.41-2.11-8.65-2.22V2.93A1.58,1.58,0,0,1,7,1.35H9.66c.93,0,1.58.48,1.58.94V3.42c-1.63.21-2.85,1.16-2.85,2.3h7.06c0-1.14-1.22-2.09-2.85-2.3V2.29C12.59,1,11.3,0,9.66,0H7A2.93,2.93,0,0,0,4.1,2.93V15.16c-1,0-2,0-3,0a1.13,1.13,0,0,0,0,2.26c6.57,0,6.35.08,12.61,2.26,6.86,2.38,14.51,2.17,21.68,2.17A1.13,1.13,0,0,0,35.38,19.56Z"/><path class="cls-1" d="M7.79,7.39h8.1a.64.64,0,0,0,0-1.27H7.79A.64.64,0,0,0,7.79,7.39Z"/><path class="cls-1" d="M33,36.86a1.35,1.35,0,0,0-1.34,1.35,1.48,1.48,0,0,0,.08.48l-.06,0-.06,0,0,0a1,1,0,0,1-.42-1,1.17,1.17,0,0,1,0-.18s0-.13.08-.16a.77.77,0,0,0,.08-.11l.19-.24,0-.06,0,0h0c0-.07-.06.1,0,.08h0l.11-.13.23-.24.45-.48.45-.48.23-.24a.74.74,0,0,0,.15-.16l.14-.23a3.72,3.72,0,0,0,.45-.92,4.38,4.38,0,0,0,.14-1,3.88,3.88,0,0,0-.4-1.85,7,7,0,0,0,.65-2.95V22.75c-26.36-.1-8.65-4-32.19-4.25l.34,8.33a8,8,0,0,0,1.53,4.45,3.67,3.67,0,0,0-.32,1.66,4.54,4.54,0,0,0,.14,1,3.82,3.82,0,0,0,.45.92l.14.23c0,.06.1.14.13.16l.23.24.45.48.47.48.23.24.11.13H6s-.1-.14,0-.08h0l0,0,0,.06.19.24a.29.29,0,0,0,.08.11s0,.11.08.16a.92.92,0,0,1,0,.18.94.94,0,0,1-.15.72,1.47,1.47,0,0,1-.27.29l0,0,0,0-.06,0a1.57,1.57,0,0,0,.08-.48,1.35,1.35,0,1,0-1.35,1.35,5.83,5.83,0,0,0,.93,0c.1,0,.48-.1.58-.11l.13,0h.07l.08,0A2.11,2.11,0,0,0,7,39a2.28,2.28,0,0,0,.53-.58c.07-.11.13-.24.19-.37a1.83,1.83,0,0,0,.11-.42,1.89,1.89,0,0,0,0-.43,1.51,1.51,0,0,0,0-.45c0-.16-.07-.32-.11-.5L7.6,36c-.19-.42-.39-.86-.6-1.27l-.27-.6,0-.1a6.52,6.52,0,0,0,3.51,1.13h17a7,7,0,0,0,3.62-1l-.27.58L30,36l-.11.3c0,.18-.08.34-.11.5a1.41,1.41,0,0,0,0,.45,1.26,1.26,0,0,0,0,.43,1.69,1.69,0,0,0,.11.42l.18.37a2.65,2.65,0,0,0,.55.58,2.21,2.21,0,0,0,.61.32l.1,0h.06l.13,0c.1,0,.48.11.56.11a5.78,5.78,0,0,0,.93,0,1.35,1.35,0,1,0,0-2.71Zm-7.57-3.58H11.17c-2.67,0-5.38-2.4-6.44-5.09,1.34,2.06,3.59,3.67,5.83,3.67H25.28a6.3,6.3,0,0,0,6.37-6.22v1.44A6.21,6.21,0,0,1,25.43,33.28Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-6'))
	{
		document.getElementById('h2-icon-6').innerHTML = h2_icon_6;
	}

    var h2_icon_7 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 44.22 25.5"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>Р–РёСЂРѕСѓР»РѕРІРёС‚РµР»Рё</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><rect class="cls-1" x="8.91" y="4.46" width="26.19" height="2.29"/><path class="cls-1" d="M33,7.78H11.21A1.26,1.26,0,0,0,10,9.1V20.81a1.26,1.26,0,0,0,1.18,1.32H33a1.26,1.26,0,0,0,1.18-1.32V9.1A1.26,1.26,0,0,0,33,7.78Zm-22,13,.11-10.68c-.2-4.55.27,10.17,4,10.73Z"/><rect class="cls-1" x="12.75" width="18.85" height="0.57"/><rect class="cls-1" x="13.5" y="1.08" width="17.35" height="2.87"/><rect class="cls-1" x="15.85" y="22.79" width="11.3" height="0.57"/><rect class="cls-1" x="16.3" y="24.07" width="10.4" height="1.44"/><rect class="cls-1" y="12.4" width="8.67" height="6.12"/><rect class="cls-1" x="35.55" y="12.4" width="8.67" height="6.12"/></g></g></svg>';
    if(document.getElementById('h2-icon-7'))
	{
		document.getElementById('h2-icon-7').innerHTML = h2_icon_7;
	}

    var h2_icon_8 = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 31.73 31.73"><defs><style>.cls-1{fill:#00a0e3;}</style></defs><title>РќР°СЂСѓР¶РЅС‹Рµ СЃРµС‚Рё</title><g id="РЎР»РѕР№_2" data-name="РЎР»РѕР№ 2"><g id="Layer_2_copy" data-name="Layer 2 copy"><path class="cls-1" d="M15.86,0A15.86,15.86,0,1,0,31.73,15.86,15.88,15.88,0,0,0,15.86,0Zm0,2.44A13.42,13.42,0,1,1,2.44,15.86,13.41,13.41,0,0,1,15.86,2.44ZM13.42,3.89a12.23,12.23,0,0,0,0,23.95,2.42,2.42,0,0,1,2.44-2.21,2.51,2.51,0,0,1,2.44,2.21,12.23,12.23,0,0,0,0-23.95,2.45,2.45,0,0,1-4.88,0ZM8.31,9.76H14a.58.58,0,0,1,.61.61v1.22a.58.58,0,0,1-.61.61H6.83A10.07,10.07,0,0,1,8.31,9.76Zm9.38,0h5.72A10.06,10.06,0,0,1,24.9,12.2H17.69a.58.58,0,0,1-.61-.61V10.37A.58.58,0,0,1,17.69,9.76ZM6.22,14.64H14a.58.58,0,0,1,.61.61v1.22a.58.58,0,0,1-.61.61H6.22a4.14,4.14,0,0,1-.11-1.22A4.14,4.14,0,0,1,6.22,14.64Zm11.48,0h7.82c0,.37.11.85.11,1.22a4.14,4.14,0,0,1-.11,1.22H17.69a.58.58,0,0,1-.61-.61V15.25A.58.58,0,0,1,17.69,14.64ZM6.83,19.53H14a.58.58,0,0,1,.61.61v1.22A.58.58,0,0,1,14,22H8.31A10.07,10.07,0,0,1,6.83,19.53Zm10.87,0H24.9A10.06,10.06,0,0,1,23.42,22H17.69a.58.58,0,0,1-.61-.61V20.14A.58.58,0,0,1,17.69,19.53Z"/></g></g></svg>';
    if(document.getElementById('h2-icon-8'))
	{
		document.getElementById('h2-icon-8').innerHTML = h2_icon_8;
	}
	
});

/**
* РЎР»Р°Р№РґРµСЂ РЈСЃС‚СЂР°РЅРµРЅРёРµ Р·Р°СЃРѕСЂРѕРІ
*/

$(document).ready(function(){
	$('.clogging-slider__wrap').slick({
		adaptiveHeight: true
	});
	
	$('.clogging-slider__wrap').on('beforeChange', function(event, slick, currentSlide, nextSlide)
	{
		if (nextSlide == 1)
		{
			$(".clogging-slider__dot").addClass("right-pos");
			changeCloggingSlide(nextSlide, '.clogging-slider__header-right');
		}
		else
		{
			$(".clogging-slider__dot").removeClass("right-pos");
			changeCloggingSlide(nextSlide, '.clogging-slider__header-left');
		}
	});
	
});

function changeCloggingSlide(num, el)
{
	$(".clogging-slider__header").removeClass("clogging-slider__header-active");

	$(el).addClass("clogging-slider__header-active");

	if (num == 1)
	{
		$(".clogging-slider__dot-circle").animate({"left":"47px"}, 700);
	}
	else
	{
		$(".clogging-slider__dot-circle").animate({"left":"4px"}, 700);
	}

	$('.clogging-slider__wrap').slick('slickGoTo', num)
}

function changeCloggingSlideByDot(el)
{
	if ($(el).hasClass('right-pos'))
	{
		$('.clogging-slider__wrap').slick('slickGoTo', 0);
	}
	else
	{
		$('.clogging-slider__wrap').slick('slickGoTo', 1);
	}
}