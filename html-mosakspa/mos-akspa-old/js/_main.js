'use strict'

$(document).ready(function () {

    let projectsBlock = $('.projects');
    let projectsSlider = new Swiper(projectsBlock.find('.swiper-container'), {
        loop: true,
        spaceBetween: 7,
        autoHeight: true,
        autoplay: {
            delay: 3000,
        },
        // watchSlidesVisibility: true,
        // loopAdditionalSlides: 2,
        navigation: {
            nextEl: projectsBlock.find('.projects__arrow--next'),
            prevEl: projectsBlock.find('.projects__arrow--prev'),
        },
        breakpointsInverse: true,
        breakpoints: {
            0: {
                slidesPerView: 'auto',
                spaceBetween: 13,
            },
            576: {
                slidesPerView: 'auto',
            },
            980: {
                slidesPerView: 3
            }
        }
    });

    let CaseSliderBlock = $('.case-slider');
    let CaseSlider = new Swiper(CaseSliderBlock.find('.swiper-container'), {
        loop: true,
        spaceBetween: 2,
        autoHeight: true,
        slidesPerView: 1,
        speed: 800,
        autoplay: {
            delay: 3000,
        },
        navigation: {
            nextEl: CaseSliderBlock.find('.case-slider__arrow--next'),
            prevEl: CaseSliderBlock.find('.case-slider__arrow--prev'),
        },
        pagination: {
            el: '.case-slider__pagination',
            type: 'fraction',
        }
    });



    let caseFtpSlider;
    let caseFtpSliderDestroy = false;
    let caseFtpBlock = $('.case--ftp');

    function caseFtpSliderInit(screenWidth) {
        if(screenWidth <= 960){
            if(caseFtpSlider == undefined){
                caseFtpSlider = new Swiper(caseFtpBlock.find('.swiper-container'), {
                    loop: true,
                    spaceBetween: 7,
                    autoHeight: false,
                    slidesPerView: 'auto',
                    centeredSlides: true
                });
            }else {
                if (caseFtpSlider && !caseFtpSliderDestroy) {
                    caseFtpSliderDestroy = true
                    caseFtpSlider.destroy(true, true)
                }
            }

        }
    }

    caseFtpSliderInit($(window).width())

    let caseklatzSlider;
    let caseklatzSliderDestroy = false;
    let caseklatzBlock = $('.case--klatz');

    function caseKlatzSliderInit(screenWidth) {
        if(screenWidth <= 960){
            if(caseklatzSlider == undefined){
                caseklatzSlider = new Swiper(caseklatzBlock.find('.swiper-container'), {
                    loop: true,
                    spaceBetween: 20,
                    autoHeight: false,
                    slidesPerView: 'auto',
                });
            }else {
                if (caseklatzSlider && !caseklatzSliderDestroy) {
                    caseklatzSliderDestroy = true
                    caseklatzSlider.destroy(true, true)
                }
            }

        }
    }

    // слайдер типов проектов на странцие проектов, на мобилке
    let servicesTitleSlider = new Swiper($('.services-title .swiper-container'), {
        autoHeight: false,
        slidesPerView: 'auto',
        freeMode: true
    });

    caseKlatzSliderInit($(window).width())


    $(window).resize(function (e) {
        let windowsWidth = $(window).width();
        if ($('.case--ftp').is('div')) {
            caseFtpSliderInit(windowsWidth)
        }
        if ($('.case--klatz').is('div')) {
            caseKlatzSliderInit(windowsWidth)
        }
    })


    // анимация плавного скролла
    function animateScroll(positionToScroll, delay) {
        $('html, body').animate({scrollTop: positionToScroll}, delay);
    }

    $(".animate-scroll, [href^='#']:not([href='#'])").click(function (e) {
        e.preventDefault();
        let elementClick = $(this).attr("href");
        if (elementClick == 'top') {
            animateScroll(0, 600);
        } else {
            let destination = $(elementClick).offset().top;
            let mainNav = $(".main-nav");
            animateScroll(destination, 600);
            if (mainNav.hasClass('main-nav--open')){
                mainNav.toggleClass('main-nav--open')
            }
        }
        return false;
    })

    $(".main-nav__toggle").on('click', function (e) {
        e.preventDefault();
        $(this).closest('.main-nav').toggleClass('main-nav--open')
    })

    // маска на поле телефона
    $('.js_phone_mask').inputmask({
        mask: "+7(999)999-99-99",
        "placeholder": "_",
        showMaskOnHover: false
    });

    // анимация при сколле
    function animations() {
        $('.animate, .animate-line').each(function () {
            let animation = $(this).attr('data-animation');
            let top = $(document).scrollTop() + jQuery(window).height();
            let pos_top = $(this).offset().top + 100;
            let checkClassAnimateLine = $(this).hasClass('animate-line')
            if (top > pos_top && checkClassAnimateLine) {
                $(this).addClass('animate-line__animated');
            } else if (top > pos_top) {
                $(this).addClass('animate__animated  animate__' + animation);
            }
        });
    }

    $(window).on('load scroll', function () {
        animations();
    })

    // триггер скрола, для появления анимации  если она вверху страницы, не дожидаясь скролла пользователя
    $(window).scroll()


    //загрузить еще банеров на главной
    $('.portfolio__load-more--toggle').on('click', function (e) {
        let scrollBefore = jQuery(window).scrollTop();
        e.preventDefault();

        let int = setInterval(() => {
          jQuery(window).scrollTop(scrollBefore);
        }, 1)
        // $('.portfolio__load-more-content').toggleClass('portfolio__load-more-content--open')
        $('.portfolio__load-more-content').slideToggle('slow').queue((next) => {
          clearInterval(int)
          next();
        })

        if ($(this).hasClass('portfolio__load-more--show')) {
            $(this).find('.title').text("Загрузить еще");
        } else {
            $(this).find('.title').text("Скрыть");
        }
        $(this).toggleClass('portfolio__load-more--show')
    })


    // проверка на пустоту в инпуте
    $('input:empty, textarea:empty').not('[type="radio"]').not('[type="checkbox"]').not('[type="file"]').siblings('label').addClass('empty');

    $('input, textarea').not('[type="file"]').keyup(function () {
        if ($(this).val().trim() !== '') {
            $(this).siblings('label').removeClass('empty');
        } else {
            $(this).siblings('label').addClass('empty');
        }
    });
    $('input:empty, textarea:empty').not('[type="radio"]').not('[type="checkbox"]').not('[type="file"]').each(function () {

        if ($(this).val().trim() !== '') {
            $(this).siblings('label').removeClass('empty');
        } else {
            $(this).siblings('label').addClass('empty');
        }
        $(this).on('.keyup', function () {
            if ($(this).val().trim() !== '') {
                $(this).siblings('label').removeClass('empty');
            } else {
                $(this).siblings('label').addClass('empty');
            }
        });
    })

    // добавление ссылки на источник при копировании
    // function addLink() {
    //     let body_element = document.getElementsByTagName('body')[0];
    //     let selection = window.getSelection();
    //     let pagelink = " <a href='https://metasharks.ru'>https://metasharks.ru</a>";
    //     let copytext = selection + 'Подробнее на сайте студии METASHARKS:' + pagelink;
    //     let newdiv = document.createElement('div');
    //     newdiv.style.position = 'absolute';
    //     newdiv.style.left = '-99999px';
    //     body_element.appendChild(newdiv);
    //     newdiv.innerHTML = copytext;
    //     selection.selectAllChildren(newdiv);
    //     window.setTimeout(function () {
    //         body_element.removeChild(newdiv);
    //     }, 0);
    // }

    // document.oncopy = addLink


    let scrollTopIndex = 0;
    let lastScroll = 0
    let scrollBottomIndex = 0;
    const header = $('.header');

    function stickyHeader(scrollTop) {
        if (scrollTop <= 100) {
            header.removeClass('header--fixed')
            header.removeClass('header--hide')
        } else if (lastScroll < scrollTop) {
            scrollBottomIndex++;
            scrollTopIndex = 0;
            if (scrollBottomIndex > 5) {
                header.addClass('header--hide')
            }
        } else if (lastScroll >= scrollTop) {
            scrollTopIndex++;
            scrollBottomIndex = 0;
            if (scrollTopIndex > 15) {
                header.removeClass('header--hide')
                header.addClass('header--fixed')
            }
        }
    }

    function initHeader() {
        let windowPosition = $(document).scrollTop()
        if (windowPosition >= 100) {
            header.addClass('header--fixed')
        }
    }

    initHeader()

    let triggerIndexScroll = false;
    let indexHead = $('.index-head').is('section');

    // $(window).on('load', function () {
    //     triggerIndexScroll = $(window).scrollTop() < '50' ? false : true;
    // });

    // function scrollFirstScreen(){
    //     animateScroll($(window).height(), 600);
    // }



    $(window).on('scroll', function () {
        let top = $(document).scrollTop();
        stickyHeader(top);

        if (top == 0) {
            triggerIndexScroll = false;
        }
        if (top < 100 && indexHead && lastScroll <= top && lastScroll > 50 && !triggerIndexScroll) {
            triggerIndexScroll = true
            animateScroll($('.index-head').outerHeight(), 500);
            setTimeout(function () {
                header.addClass('header--fixed');
            }, 500)
        }

        showBtnUp(top)
        lastScroll = top;
    })

    function showBtnUp(scrollTop) {
        if (scrollTop >= ($(window).height() * 3)) {
            $('.scroll-up').addClass('scroll-up--show')
        } else {
            $('.scroll-up').removeClass('scroll-up--show')
        }
    }

    $('.success__cat img').mouseenter(function () {
        $(this).attr("src", $(this).attr('data-gif-src'))
    })
    $('.success__cat img').mouseleave(function () {
        $(this).attr("src", $(this).attr('data-src-img'))
    })


    // прелоадер
    function Ticker(elem) {
        elem.lettering();
        this.done = false;
        this.cycleCount = 5;
        this.cycleCurrent = 0;
        this.chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-_=+{}|[]\\;\':"<>?,./`~'.split('');
        this.charsCount = this.chars.length;
        this.letters = elem.find('span');
        this.letterCount = this.letters.length;
        this.letterCurrent = 0;

        this.letters.each(function () {
            var $this = $(this);
            $this.attr('data-orig', $this.text());
            $this.text('-');
        });
    }

    Ticker.prototype.getChar = function () {
        return this.chars[Math.floor(Math.random() * this.charsCount)];
    };

    Ticker.prototype.reset = function () {
        this.done = false;
        this.cycleCurrent = 0;
        this.letterCurrent = 0;
        this.letters.each(function () {
            var $this = $(this);
            $this.text($this.attr('data-orig'));
            $this.removeClass('done');
        });
        this.loop();
    };

    Ticker.prototype.loop = function () {
        var self = this;

        this.letters.each(function (index, elem) {
            var $elem = $(elem);
            if (index >= self.letterCurrent) {
                if ($elem.text() !== ' ') {
                    $elem.text(self.getChar());
                    $elem.css('opacity', Math.random());
                }
            }
        });

        if (this.cycleCurrent < this.cycleCount) {
            this.cycleCurrent++;
        } else if (this.letterCurrent < this.letterCount) {
            var currLetter = this.letters.eq(this.letterCurrent);
            this.cycleCurrent = 0;
            currLetter.text(currLetter.attr('data-orig')).css('opacity', 1).addClass('done');
            this.letterCurrent++;
        } else {
            this.done = true;
        }

        if (!this.done) {
            requestAnimationFrame(function () {
                self.loop();
            });
        } else {
            setTimeout(function () {
                self.reset();
            }, 750);
        }
    };

    let $words = $('.preloader__word');

    $words.each(function () {
        var $this = $(this),
            ticker = new Ticker($this).reset();
        $this.data('ticker', ticker);
    });

    function deletePreloader() {
        $('body').removeClass('preloader-active')
        $('.preloader').remove()
    }

    jQuery('.contacts__map').each(function () {
        jQuery(this).css('margin-right', -((jQuery(window).width() - jQuery(this).parents('.container').outerWidth()) / 2) - 15)
    });

    // переворачивание карточки на странице о компании
    $('.team__card-face--front,  .team__close').on('click', function (e) {
        $(this).closest('.team__card').toggleClass('is-flipped')

        // проверка на пустое описание
        // var textContent = $(this).closest('.team__card').find('.team__text').text().trim().length
        // if (textContent !== 0 ){
        //    $(this).closest('.team__card').toggleClass('is-flipped')
        // }
    })

    $(".custom-scroll").mCustomScrollbar()


    if($('.case--ftp').is('div')){
        // head
        const path = 'M185.137 6.26067L234.178 30.3243M185.137 6.26067L304.501 11.8138M185.137 6.26067L120.365 128.892M185.137 6.26067L134.707 30.3243M185.137 6.26067L114.813 30.3243M185.137 6.26067L247.595 1.17029L304.501 11.8138M234.178 30.3243L304.501 11.8138M234.178 30.3243L120.365 128.892M234.178 30.3243L413.225 62.7176M234.178 30.3243L319.306 76.1377M234.178 30.3243L261.475 77.9887M234.178 30.3243L134.707 189.977M304.501 11.8138L413.225 62.7176M304.501 11.8138L366.497 33.5636L413.225 62.7176M120.365 128.892L84.7405 115.472M120.365 128.892L134.707 30.3243M120.365 128.892L72.7115 173.318M120.365 128.892L134.707 189.977M120.365 128.892L109.724 305.205M120.365 128.892L63.4584 269.109M84.7405 115.472L134.707 30.3243M84.7405 115.472H74.0994M84.7405 115.472L72.7115 173.318M134.707 30.3243H114.813M134.707 30.3243L74.0994 115.472M114.813 30.3243L74.0994 115.472M114.813 30.3243L64.8463 92.7971M114.813 30.3243L81.9646 60.8665L64.8463 92.7971M74.0994 115.472L63.4584 148.791M74.0994 115.472C74.1718 115.321 74.168 115.037 74.0994 114.64M64.8463 92.7971L57.9065 127.504L63.4584 148.791M64.8463 92.7971L63.4584 148.791M64.8463 92.7971C67.8806 99.0295 73.4965 111.156 74.0994 114.64M63.4584 148.791L72.7115 161.286M63.4584 148.791L68.0849 161.054M72.7115 161.286L74.0994 114.64M72.7115 161.286V173.318M72.7115 173.318L68.0849 161.054M72.7115 173.318H63.4584M72.7115 173.318L69.9356 202.055M413.225 62.7176L319.306 76.1377M413.225 62.7176L390.093 85.3929M413.225 62.7176L474.296 142.313M413.225 62.7176L449.775 95.1109L474.296 142.313M319.306 76.1377L261.475 77.9887M319.306 76.1377L293.86 96.9619M319.306 76.1377L474.296 142.313M319.306 76.1377L390.093 85.3929M319.306 76.1377L427.105 186.275M319.306 76.1377L368.81 240.881M261.475 77.9887L134.707 189.977M261.475 77.9887L238.805 168.69M261.475 77.9887L293.86 96.9619M134.707 189.977L109.724 305.205M134.707 189.977L138.871 352.406M134.707 189.977L187.45 256.615M134.707 189.977L238.805 168.69M134.707 189.977L240.192 229.312M109.724 305.205L63.4584 269.109M109.724 305.205L138.871 352.406M109.724 305.205L124.991 393.592M63.4584 269.109L69.9356 202.055M63.4584 269.109L124.991 393.592M63.4584 269.109L33.8485 283.455M138.871 352.406L187.45 256.615M138.871 352.406L327.172 313.534M138.871 352.406L353.08 363.975M138.871 352.406C145.04 376.007 157.377 423.672 157.377 425.523M138.871 352.406L124.991 393.592M138.871 352.406L231.865 421.358M187.45 256.615L240.192 229.312M187.45 256.615L327.172 313.534M240.192 229.312L327.172 313.534M240.192 229.312L317.918 248.748M240.192 229.312L238.805 168.69M327.172 313.534L353.08 363.975M327.172 313.534C327.172 312.424 321.003 269.88 317.918 248.748M327.172 313.534L368.81 240.881M353.08 363.975L427.105 186.275M353.08 363.975L442.373 252.45M353.08 363.975L368.81 240.881M353.08 363.975L404.435 324.178M353.08 363.975L380.377 350.092M353.08 363.975L340.126 418.118M353.08 363.975L329.022 406.087M353.08 363.975L231.865 421.358M317.918 248.748L368.81 240.881M317.918 248.748L238.805 168.69M238.805 168.69L293.86 96.9619M238.805 168.69L368.81 240.881M293.86 96.9619L368.81 240.881M474.296 142.313L390.093 85.3929M474.296 142.313L427.105 186.275M474.296 142.313L480.773 180.259L476.609 208.025M474.296 142.313L476.609 208.025M474.296 142.313L442.373 252.45M427.105 186.275L442.373 252.45M427.105 186.275L368.81 240.881M476.609 208.025L442.373 252.45M476.609 208.025L463.655 245.508L442.373 279.753M476.609 208.025L442.373 279.753M442.373 252.45V279.753M442.373 252.45L404.435 324.178M442.373 279.753L404.435 324.178M404.435 324.178L380.377 350.092M380.377 350.092L340.126 418.118M340.126 418.118L329.022 448.661M340.126 418.118V477.352M329.022 448.661V406.087M329.022 448.661L305.889 481.054M329.022 448.661L340.126 477.352M329.022 448.661L337.813 495.862M329.022 448.661L311.904 494.474M329.022 406.087L231.865 421.358M329.022 406.087L288.771 443.108M329.022 406.087L305.889 481.054M231.865 421.358L288.771 443.108M231.865 421.358L305.889 481.054M231.865 421.358L157.377 425.523M288.771 443.108L305.889 481.054M305.889 481.054L311.904 494.474M305.889 481.054L187.45 473.187M305.889 481.054L157.377 425.523M340.126 477.352L346.14 495.862H337.813M340.126 477.352L337.813 495.862M337.813 495.862L311.904 494.474M311.904 494.474L187.45 473.187M311.904 494.474C310.423 494.221 229.551 500.847 189.3 504.192M187.45 473.187L189.3 504.192M187.45 473.187L168.018 491.235M187.45 473.187L157.377 425.523M189.3 504.192L168.018 491.235M168.018 491.235L157.377 425.523M168.018 491.235L137.483 417.193M157.377 425.523L137.483 417.193M157.377 425.523L124.991 393.592M137.483 417.193L124.991 393.592M137.483 417.193L124.991 407.475M124.991 393.592L33.8485 283.455M124.991 393.592V407.475M124.991 393.592L106.948 403.773M124.991 393.592L61.6078 403.773M124.991 393.592L94.9189 388.039M124.991 393.592L31.0726 317.236M68.0849 161.054L63.4584 173.318M63.4584 173.318L1 229.312M63.4584 173.318L31.0726 221.908M63.4584 173.318L69.9356 202.055M63.4584 173.318L25.5207 258.928M1 229.312L31.0726 221.908M1 229.312L25.5207 258.928M31.0726 221.908L25.5207 258.928M69.9356 202.055L25.5207 258.928M69.9356 202.055L33.8485 283.455M25.5207 258.928L12.5664 281.604M25.5207 258.928L17.1929 300.577M25.5207 258.928L33.8485 283.455M25.5207 258.928L23.2074 305.205M12.5664 281.604L17.1929 300.577M12.5664 281.604L10.7157 292.247L17.1929 300.577M17.1929 300.577L23.2074 305.205M33.8485 283.455L23.2074 305.205M33.8485 283.455L31.0726 317.236M23.2074 305.205L31.0726 317.236M23.2074 305.205V324.641M23.2074 305.205L7.93982 317.236M124.991 407.475L106.948 403.773M106.948 403.773H61.6078M61.6078 403.773L94.9189 388.039M61.6078 403.773L33.8485 359.81M61.6078 403.773L25.5207 368.14M61.6078 403.773L23.2074 389.89M61.6078 403.773L31.0726 400.534L23.2074 389.89M94.9189 388.039L31.0726 317.236M94.9189 388.039L33.8485 359.81M31.0726 317.236L33.8485 359.81M23.2074 324.641L33.8485 359.81M23.2074 324.641L7.93982 317.236M23.2074 324.641L20.8942 341.763M33.8485 359.81L25.5207 368.14M33.8485 359.81L20.8942 341.763M25.5207 368.14L23.2074 389.89M25.5207 368.14L20.8942 341.763M23.2074 389.89L20.8942 341.763M23.2074 389.89L14.8796 373.231L20.8942 341.763M20.8942 341.763L7.93982 317.236 ';
        // hand
        const pathTwo = 'M24 299L15 248.5M24 299L59.5 280M24 299L63 361M24 299V377.5M15 248.5L59.5 280M15 248.5L41.5 202M15 248.5L6 191M59.5 280H76.5M59.5 280L41.5 202M59.5 280L63 361M76.5 280L41.5 202M76.5 280L69.5 232.5M76.5 280L93 264.5M76.5 280L112.5 299M76.5 280L63 361M41.5 202L6 191M41.5 202L24 151.5M41.5 202L34 126M41.5 202L57.5 195.959M41.5 202L69.5 232.5M6 191L24 151.5M6 191L1 134.5M24 151.5L1 134.5M24 151.5L34 126M24 151.5L15 118M1 134.5L15 118M34 126L15 118M34 126L57.5 195.959M57.5 195.959L69.5 232.5M93 264.5L85 209.5M93 264.5L119 254.5M93 264.5L112.5 299M85 209.5L119 254.5M85 209.5L131.579 219M85 209.5L136 192M85 209.5L83 173M119 254.5L141.5 242.5M119 254.5L131.579 219M119 254.5L184.5 337M119 254.5L112.5 299M141.5 242.5L136 192M141.5 242.5L153 232.5M141.5 242.5L184.5 337M136 192L131.579 219M136 192L133.5 148M136 192L106.5 180.231M83 173L106.5 180.231M83 173L80 127.5M133.5 148L106.5 180.231M133.5 148L123.5 114M133.5 148L128 95.5M106.5 180.231L80 127.5M106.5 180.231L123.5 114M80 127.5L123.5 114M80 127.5V91M123.5 114L128 95.5M123.5 114L119 73M123.5 114L80 91M128 95.5L119 73M128 95.5L125.5 54M119 73L80 91M119 73L125.5 54M119 73L111.5 37.5M80 91L111.5 37.5M80 91V49.5L111.5 37.5M125.5 54L111.5 37.5M153 232.5L149 202M153 232.5L169.5 234.5M153 232.5L184.5 337M149 202L169.5 234.5M149 202L163 184M149 202L146.5 160M169.5 234.5L205.5 242M169.5 234.5L163 184M169.5 234.5L184.5 337M205.5 242L180.5 213.25M205.5 242L207 184M205.5 242L217 245.5M205.5 242L184.5 337M163 184L146.5 160M163 184L166.5 134.5M163 184L201 111.5M163 184L207 151.5M163 184H207M163 184L180.5 213.25M146.5 160L166.5 134.5M146.5 160L149 108M166.5 134.5L149 108M166.5 134.5L201 111.5M149 108L201 111.5M149 108L144.5 75M201 111.5L207 151.5M201 111.5L144.5 75M201 111.5L198 73M201 111.5L175.332 73M207 151.5V184M207 184L180.5 213.25M144.5 75L146.5 37.5M144.5 75L175.332 73M198 73H175.332M198 73L192.5 33.5M146.5 37.5L149 12M146.5 37.5L169.5 35.5M146.5 37.5L175.332 73M175.332 73L192.5 33.5M175.332 73L169.5 35.5M192.5 33.5L169.5 35.5M192.5 33.5L184.5 8.5M149 12L169.5 35.5M149 12L166.5 1M169.5 35.5L184.5 8.5M169.5 35.5L166.5 1M184.5 8.5L166.5 1M217 245.5L221 221M217 245.5L240.5 258.5M217 245.5L184.5 337M221 221L240.5 258.5M221 221L250 238.75M221 221L228 180.231M240.5 258.5L279 256.5M240.5 258.5L250 238.75M240.5 258.5L279 322M240.5 258.5L184.5 337M279 256.5L250 238.75M279 256.5L285 195.959M279 256.5V322M250 238.75L228 180.231M250 238.75L285 195.959M250 238.75L276 175M228 180.231L232 114M228 180.231L276 175M232 114L276 175M232 114L285 148M232 114L285 83M232 114L272.5 49.5M232 114L236.5 62.5M276 175L285 148M276 175L285 195.959M285 148V83M285 148V195.959M285 83L272.5 49.5M272.5 49.5L236.5 62.5M272.5 49.5L254 37.5L236.5 62.5M279 322L297 377.5M279 322L184.5 337M297 377.5L184.5 337M297 377.5L211 383M297 377.5L205.5 484.5M297 377.5L316.5 454.5M297 377.5L323.5 393.5M184.5 337L112.5 299M184.5 337L63 361M184.5 337L141.5 423.5M184.5 337L211 383M112.5 299L63 361M63 361L30.5 454.5M63 361L24 377.5M63 361L141.5 423.5M30.5 454.5L24 377.5M30.5 454.5L141.5 423.5M30.5 454.5L34 536M141.5 423.5L211 383M141.5 423.5L205.5 484.5M141.5 423.5L34 536M141.5 423.5L131.5 506.364M211 383L205.69 481M205.5 484.5L316.5 454.5M205.5 484.5L276 546M205.5 484.5L250 585.5M205.5 484.5L131.5 506.364M205.5 484.5L205.69 481M205.5 484.5L141.5 528.065M316.5 454.5L276 546M316.5 454.5L323.5 393.5M316.5 454.5L364 451M316.5 454.5L336 484.5M276 546L250 585.5M276 546L336 484.5M34 536L59.5 571.5M34 536L131.5 506.364M59.5 571.5L69.5 628M59.5 571.5L131.5 506.364M59.5 571.5L184.5 601.5M59.5 571.5L141.5 528.065M69.5 628L99 682.5M69.5 628L184.5 601.5M99 682.5L178.5 661M99 682.5L184.5 601.5M178.5 661L256.5 682.5M178.5 661L184.5 601.5M256.5 682.5L250 585.5M256.5 682.5L184.5 601.5M250 585.5L184.5 601.5M131.5 506.364L141.5 528.065M184.5 601.5L205.69 481M184.5 601.5L141.5 528.065M323.5 393.5H369.5M323.5 393.5L351 361M364 451L336 484.5M364 451L369.5 393.5M364 451L415 390M369.5 393.5L351 361M369.5 393.5L415 390M369.5 393.5L421 366M351 361L421 366M351 361L399.5 331M351 361V331M415 390L421 366M421 366L399.5 331M399.5 331L392.5 284L369.5 291M399.5 331H351M399.5 331L369.5 291M351 331L369.5 291';
// face
        const face = 'M38 54.5L90.5 100.5M38 54.5L82 11M38 54.5L7 95M38 54.5L31.5 118M90.5 100.5L82 11M90.5 100.5L31.5 118M90.5 100.5L183 67.0643M90.5 100.5L72.5 138.5M90.5 100.5L183 147M82 11L147 1M82 11L183 67.0643M7 95L1 141.5M7 95L31.5 118M1 141.5L7 220M1 141.5M1 141.5L31.5 118M7 220L11.5 252M7 220L24 239.5M7 220L17.5 203M7 220L41 232.5M11.5 252V288M11.5 252L24 239.5M11.5 252L28 290M11.5 288L17.5 325.5M11.5 288L28 290M17.5 325.5L30.5 369.5M17.5 325.5L28 290M17.5 325.5L34.5 330M30.5 369.5L43.5 401.5M30.5 369.5L49 393M30.5 369.5L34.5 330M30.5 369.5L65 338M43.5 401.5L58.5 434.5M43.5 401.5L62 419M43.5 401.5L49 393M58.5 434.5L95.5 472M58.5 434.5L95.5 453.5M58.5 434.5L62 419M95.5 472L138 505.5M95.5 472V453.5M138 505.5L153 493M138 505.5L163.5 514.5M138 505.5L95.5 453.5M153 493L163.5 514.5M153 493L183 498.5M153 493L95.5 453.5M153 493L183 477.5M153 493L138 463M153 493L169 472M163.5 514.5L183 498.5M163.5 514.5L202.5 515M183 498.5V477.5M183 498.5L202.5 515M183 498.5L213 493.479M95.5 453.5L62 419M95.5 453.5L138 463M95.5 453.5L140.5 441.5M95.5 453.5M62 419L49 393M62 419L100.5 425M62 419L65 338M62 419L82.5 379M62 419L111.5 404.5M49 393L65 338M183 477.5L169 472M183 477.5V441.5M183 477.5L213 493.479M183 477.5L197 472.459M169 472L138 463M169 472L183 441.5M138 463L183 441.5M138 463L140.5 441.5M183 441.5H140.5M183 441.5L155.5 431M183 441.5L197 472.459M183 441.5L228 463.45M183 441.5L225.5 441.929M183 441.5L210.5 431.419M140.5 441.5L155.5 431M140.5 441.5L139 423M155.5 431L139 423M139 423L100.5 425M139 423L111.5 404.5M100.5 425L111.5 404.5M111.5 404.5L135 393M111.5 404.5L82.5 379M111.5 404.5L132 369.5M135 393L157.5 389.5M135 393L132 369.5M135 393L157.5 347M157.5 389.5L183 395M157.5 389.5V347M157.5 389.5L177 366.176M183 395L208.5 389.878M183 395V369.5M183 359L157.5 347M183 359V338M183 359L208.5 347.337M183 359L177 366.176M183 359V369.5M183 359L188.926 366.176M157.5 347L144.5 344M157.5 347L183 316.307M157.5 347L159.5 325.5M157.5 347L183 338M157.5 347L132 369.5M157.5 347L177 366.176M144.5 344L136.5 326.5M144.5 344M144.5 344L132 369.5M136.5 326.5L159.5 325.5M136.5 326.5L148 313.5M136.5 326.5L82.5 379M136.5 326.5L99 299M136.5 326.5L95 339.5M136.5 326.5L132 369.5M159.5 300.5L183 252.244M159.5 300.5L183 316.307M159.5 300.5V325.5M159.5 300.5L165 264.25M159.5 300.5L157.5 272.5M159.5 300.5L148 313.5M170.5 228L183 252.244M170.5 228L183 205.699M170.5 228L183 228.221M170.5 228L157.5 208.5M170.5 228L153.5 239.5M170.5 228L165 264.25M170.5 228L157.5 272.5M170.5 228L144.5 224M183 252.244V316.307M183 252.244M183 252.244L195.5 228.221M183 252.244L206.5 300.792M183 252.244V228.221M183 316.307L159.5 325.5M183 316.307V338M183 316.307L206.5 300.792M183 316.307L208.5 347.337M183 316.307L206.5 325.816M183 338L208.5 347.337M183 228.221V205.699M183 228.221H195.5M183 205.699L157.5 186M183 205.699M183 205.699V169.5M183 205.699L195.5 228.221M183 205.699L208.5 186.18M183 205.699M157.5 208.5L139.5 197.5M157.5 208.5V186M157.5 208.5L135 215.5M157.5 208.5L144.5 224M139.5 197.5L117.5 190M139.5 197.5L157.5 186M139.5 197.5L135 177M139.5 197.5L117.5 207M139.5 197.5L135 215.5M117.5 190L94.5 186M117.5 190L135 177M117.5 190L108.5 174.5M117.5 190L92 201.5M117.5 190V207M94.5 186L61 181M94.5 186L108.5 174.5M94.5 186L61 169.5M94.5 186L64.5 207M94.5 186L92 201.5M61 181L42.5 190M61 181V169.5M61 181L38.5 183.5M61 181L64.5 207M42.5 190L38.5 183.5M42.5 190L64.5 207M42.5 190L48 220M42.5 190L31.7977 199.243M31.7977 199.243L17.5 203M31.7977 199.243L38.5 183.5M31.7977 199.243L48 220M17.5 203L38.5 183.5M17.5 203L31.5 118M17.5 203L48 220M17.5 203L41 232.5M38.5 183.5L61 169.5M38.5 183.5L31.5 118M61 169.5L108.5 174.5M61 169.5L31.5 118M61 169.5L72.5 138.5M108.5 174.5L135 177M108.5 174.5L72.5 138.5M108.5 174.5L183 147M135 177L157.5 186M135 177L183 147M135 177L183 169.5M157.5 186L183 169.5M153.5 239.5L121.5 247M153.5 239.5L157.5 272.5M121.5 247L92.5 258M121.5 247L99 299M121.5 247L157.5 272.5M121.5 247V239.5M92.5 258L55 249M92.5 258L28 290M92.5 258L99 299M92.5 258L121.5 239.5M92.5 258L92 242.5M55 249L24 239.5M55 249L28 290M55 249L61 239.5M55 249L92 242.5M24 239.5L28 290M24 239.5L41 232.5M24 239.5H61M28 290L99 299M28 290L65 338M28 290L34.5 330M99 299L157.5 272.5M99 299L148 313.5M99 299L65 338M99 299L95 339.5M157.5 272.5L165 264.25M157.5 272.5L148 313.5M65 338L34.5 330M65 338L82.5 379M65 338L95 339.5M82.5 379L95 339.5M82.5 379L132 369.5M31.5 118L72.5 138.5M183 67.0643V1M183 67.0643L147 1M183 67.0643V147M183 67.0643L275.5 100.597M183 67.0643L219 1M183 67.0643L284 11.0097M183 1H147M183 1H219M183 147L72.5 138.5M183 147V169.5M183 147L293.5 138.634M183 147L275.5 100.597M183 147L257.5 174.669M183 147L231 177.171M183 169.5L208.5 186.18M183 169.5L231 177.171M41 232.5L48 220M41 232.5L61 239.5M48 220L64.5 207M64.5 207L92 201.5M92 201.5L117.5 207M117.5 207L135 215.5M135 215.5L144.5 224M144.5 224L121.5 239.5M121.5 239.5L92 242.5M92 242.5L61 239.5M328 54.5521L275.5 100.597M328 54.5521L284 11.0097M328 54.5521L359 95.0915M328 54.5521L334.5 118.114M275.5 100.597L284 11.0097M275.5 100.597L334.5 118.114M275.5 100.597L293.5 138.634M284 11.0097L219 1M359 95.0915L365 141.637M359 95.0915L334.5 118.114M365 141.637L359 220.213M365 141.637M365 141.637L334.5 118.114M359 220.213L354.5 252.244M359 220.213L342 239.732M359 220.213L348.5 203.197M359 220.213L325 232.725M354.5 252.244V288.279M354.5 252.244L342 239.732M354.5 252.244L338 290.281M354.5 288.279L348.5 325.816M354.5 288.279L338 290.281M348.5 325.816L335.5 369.859M348.5 325.816L338 290.281M348.5 325.816L331.5 330.32M335.5 369.859L322.5 401.89M335.5 369.859L317 393.382M335.5 369.859L331.5 330.32M335.5 369.859L301 338.328M322.5 401.89L307.5 434.922M322.5 401.89L304 419.407M322.5 401.89L317 393.382M307.5 434.922L270.5 472.459M307.5 434.922L270.5 453.941M307.5 434.922L304 419.407M270.5 472.459L228 505.991M270.5 472.459V453.941M228 505.991L213 493.479M228 505.991L202.5 515M228 505.991L270.5 453.941M213 493.479L202.5 515M213 493.479L270.5 453.941M213 493.479L228 463.45M213 493.479L197 472.459M270.5 453.941L304 419.407M270.5 453.941L228 463.45M270.5 453.941L225.5 441.929M270.5 453.941M270.5 453.941L265.5 425.413M304 419.407L317 393.382M304 419.407L265.5 425.413M304 419.407L301 338.328M304 419.407L283.5 379.368M304 419.407L254.5 404.893M317 393.382L301 338.328M197 472.459L228 463.45M228 463.45L225.5 441.929M225.5 441.929L210.5 431.419M225.5 441.929L227 423.411M210.5 431.419L227 423.411M227 423.411L265.5 425.413M227 423.411L254.5 404.893M265.5 425.413L254.5 404.893M254.5 404.893L231 393.382M254.5 404.893L283.5 379.368M254.5 404.893L234 369.859M231 393.382L208.5 389.878M231 393.382L234 369.859M231 393.382L208.5 347.337M208.5 389.878V347.337M208.5 389.878L188.926 366.176M208.5 347.337L221.5 344.334M208.5 347.337L206.5 325.816M208.5 347.337L234 369.859M208.5 347.337L188.926 366.176M221.5 344.334L229.5 326.817M221.5 344.334M221.5 344.334L234 369.859M229.5 326.817L206.5 325.816M229.5 326.817L218 313.804M229.5 326.817L283.5 379.368M229.5 326.817L267 299.29M229.5 326.817L271 339.83M229.5 326.817L234 369.859M195.5 228.221L208.5 208.702M195.5 228.221L212.5 239.732M195.5 228.221L201 264.506M195.5 228.221L208.5 272.764M195.5 228.221L221.5 224.217M206.5 300.792V325.816M206.5 300.792L201 264.506M206.5 300.792L208.5 272.764M206.5 300.792L218 313.804M208.5 208.702L226.5 197.691M208.5 208.702V186.18M208.5 208.702L231 215.709M208.5 208.702L221.5 224.217M226.5 197.691L248.5 190.184M226.5 197.691L208.5 186.18M226.5 197.691L231 177.171M226.5 197.691L248.5 207.201M226.5 197.691L231 215.709M248.5 190.184L271.5 186.18M248.5 190.184L231 177.171M248.5 190.184L257.5 174.669M248.5 190.184L274 201.695M248.5 190.184V207.201M271.5 186.18L305 181.175M271.5 186.18L257.5 174.669M271.5 186.18L305 169.664M271.5 186.18L301.5 207.201M271.5 186.18L274 201.695M305 181.175L323.5 190.184M305 181.175V169.664M305 181.175L327.5 183.678M305 181.175L301.5 207.201M323.5 190.184L327.5 183.678M323.5 190.184L301.5 207.201M323.5 190.184L318 220.213M323.5 190.184L334.202 199.436M334.202 199.436L348.5 203.197M334.202 199.436L327.5 183.678M334.202 199.436L318 220.213M348.5 203.197L327.5 183.678M348.5 203.197L334.5 118.114M348.5 203.197L318 220.213M348.5 203.197L325 232.725M327.5 183.678L305 169.664M327.5 183.678L334.5 118.114M305 169.664L257.5 174.669M305 169.664L334.5 118.114M305 169.664L293.5 138.634M257.5 174.669L231 177.171M257.5 174.669L293.5 138.634M231 177.171L208.5 186.18M212.5 239.732L244.5 247.24M212.5 239.732L208.5 272.764M244.5 247.24L273.5 258.25M244.5 247.24L267 299.29M244.5 247.24L208.5 272.764M244.5 247.24V239.732M273.5 258.25L311 249.241M273.5 258.25L338 290.281M273.5 258.25L267 299.29M273.5 258.25L244.5 239.732M273.5 258.25L274 242.735M311 249.241L342 239.732M311 249.241L338 290.281M311 249.241L305 239.732M311 249.241L274 242.735M342 239.732L338 290.281M342 239.732L325 232.725M342 239.732H305M338 290.281L267 299.29M338 290.281L301 338.328M338 290.281L331.5 330.32M267 299.29L208.5 272.764M267 299.29L218 313.804M267 299.29L301 338.328M267 299.29L271 339.83M208.5 272.764L201 264.506M208.5 272.764L218 313.804M301 338.328L331.5 330.32M301 338.328L283.5 379.368M301 338.328L271 339.83M283.5 379.368L271 339.83M283.5 379.368L234 369.859M334.5 118.114L293.5 138.634M325 232.725L318 220.213M325 232.725L305 239.732M318 220.213L301.5 207.201M301.5 207.201L274 201.695M274 201.695L248.5 207.201M248.5 207.201L231 215.709M231 215.709L221.5 224.217M221.5 224.217L244.5 239.732M244.5 239.732L274 242.735M274 242.735L305 239.732M177 366.176L183 369.5M183 369.5L188.926 366.176';
        window.animateSVGPath({
            path: face,
            selector: '.animation__interactive--face',
            paddingVertical: 50,
            paddingHorizontal: 100,
            pointRadius: 2,
            gravity: .25,
        });


        window.animateSVGPath({
            path: path,
            selector: '.animation__interactive--head',
            paddingVertical: 60,
            paddingHorizontal: 120,
            pointRadius: 2,
            gravity: .25,
        });

        window.animateSVGPath({
            path: pathTwo,
            selector: '.animation__interactive--hand',
            paddingVertical: 70,
            paddingHorizontal: 150,
            pointRadius: 2,
            gravity: .25,
        });
    }


    // ADDITIONAL JS

    // SCROLLING MENU 
    $(document).scroll (function () {
        if ($(document).scrollTop () >$('nav').height () +1)
            $('.nav-header-mob').addClass ('fixed-mob');
        else
            $('.nav-header-mob').removeClass ('fixed-mob');
    });

    // $(document).scroll (function () {
    //  if ($(document).scrollTop () >$('header').height () +1)
    //      $('.header').addClass ('fixed');
    //  else
    //      $('.header').removeClass ('fixed');
    // });

    $(document).scroll (function () {
        if ($(document).scrollTop () >$('header').height () +1)
            $('.nav_fix').addClass ('fixed');
        else
            $('.nav_fix').removeClass ('fixed');
    }); 

var ekb = $('.ekb-item_mark'),
        bk = $('.ekb_hov');

    ekb.hover(
        function(){
            bk.css(
                'display', 'initial'
            );
        },
        function(){
            bk.removeAttr('style');
        });

    var eburg = $('.ekb-item_mark'),
        akab = $('.ekb_hover');

    eburg.hover(
        function(){
            akab.css(
                'display', 'initial'
            );
        },
        function(){
            akab.removeAttr('style');
        });

    var mg = $('.mg-item_mark'),
        gm = $('.mg_hover');

    mg.hover(
        function(){
            gm.css(
                'display', 'initial'
            );
        },
        function(){
            gm.removeAttr('style');
        });

    var see = $('.che-item_mark'),
        ees = $('.che_hover');

    see.hover(
        function(){
            ees.css(
                'display', 'initial'
            );
        },
        function(){
            ees.removeAttr('style');
        });

    var first = $('.sr-item_mark'),
        sr = $('.sr_hover');

    first.hover(
        function(){
            sr.css(
                'display', 'initial'
            );
        },
        function(){
            sr.removeAttr('style');
        });

    var ven = $('.nev-item_mark'),
    nev = $('.nev_hover');

    ven.hover(
        function() {
            nev.css('display', 'initial');
        },
        function() {
            nev.removeAttr('style');
        });

    var rep = $('.per-item_mark'),
    per = $('.per_hover');

    rep.hover(
        function() {
            per.css('display', 'initial');
        },
        function() {
            per.removeAttr('style');
        });

    var rev = $('.rev-item_mark'),
    ver = $('.rev_hover');

    rev.hover(
        function() {
            ver.css('display', 'initial');
        },
        function() {
            ver.removeAttr('style');
        });

    var kr = $('.kr-item_mark'),
    rk = $('.kr_hover');

    kr.hover(
        function() {
            rk.css('display', 'initial');
        },
        function() {
            rk.removeAttr('style');
        });

    var nov = $('.nov-item_mark'),
    von = $('.nov_hover');

    nov.hover(
        function() {
            von.css('display', 'initial');
        },
        function() {
            von.removeAttr('style');
        });

    var rezh = $('.rezh-item_mark'),
    zer = $('.rezh_hover');

    rezh.hover(
        function() {
            zer.css('display', 'initial');
        },
        function() {
            zer.removeAttr('style');
        });

    var art = $('.art-item_mark'),
    tar = $('.art_hover');

    art.hover(
        function() {
            tar.css('display', 'initial');
        },
        function() {
            tar.removeAttr('style');
        });

    var km = $('.km-item_mark'),
    mk = $('.km_hover');

    km.hover(
        function() {
            mk.css('display', 'initial');
        },
        function() {
            mk.removeAttr('style');
        });

    var paf = $('.nov_mark'),
    vap = $('.nov-item_mark');

    paf.hover(
        function() {
            vap.css('color', '#e01f1f');
        },
        function() {
            vap.removeAttr('style');
        });

    var kon = $('.che_mark'),
    ron = $('.che-item_mark');

    kon.hover(
        function() {
            ron.css('color', '#e01f1f');
        },
        function() {
            ron.removeAttr('style');
        });

    var miki = $('.km_mark'),
    rurg = $('.km-item_mark');

    miki.hover(
        function() {
            rurg.css('color', '#e01f1f');
        },
        function() {
            rurg.removeAttr('style');
        });

    var vait = $('.mg_mark'),
    tap = $('.mg-item_mark');

    vait.hover(
        function() {
            tap.css('color', '#e01f1f');
        },
        function() {
            tap.removeAttr('style');
        });

    var rad = $('.art_mark'),
    dar = $('.art-item_mark');

    rad.hover(
        function() {
            dar.css('color', '#e01f1f');
        },
        function() {
            dar.removeAttr('style');
        });

    var loot = $('.sr_mark'),
    tool = $('.sr-item_mark');

    loot.hover(
        function() {
            tool.css('color', '#e01f1f');
        },
        function() {
            tool.removeAttr('style');
        });

    var hot = $('.rezh_mark'),
    toh = $('.rezh-item_mark');

    hot.hover(
        function() {
            toh.css('color', '#e01f1f');
        },
        function() {
            toh.removeAttr('style');
        });

    var edg = $('.nov_mark'),
    gde = $('.nov-item_mark');

    edg.hover(
        function() {
            gde.css('color', '#e01f1f');
        },
        function() {
            gde.removeAttr('style');
        });

    var chil = $('.rev_mark'),
    cheel = $('.rev-item_mark');

    chil.hover(
        function() {
            cheel.css('color', '#e01f1f');
        },
        function() {
            cheel.removeAttr('style');
        });

    var joji = $('.per_mark'),
    jay = $('.per-item_mark');

    joji.hover(
        function() {
            jay.css('color', '#e01f1f');
        },
        function() {
            jay.removeAttr('style');
        });

    var fat = $('.nev_mark'),
    stuf = $('.nev-item_mark');

    fat.hover(
        function() {
            stuf.css('color', '#e01f1f');
        },
        function() {
            stuf.removeAttr('style');
        });

    var dan = $('.kr_mark'),
    ned = $('.kr-item_mark');

    dan.hover(
        function() {
            ned.css('color', '#e01f1f');
        },
        function() {
            ned.removeAttr('style');
        });

    var kol = $('.ekb_mark'),
    lod = $('.ekb-item_mark');

    kol.hover(
        function() {
            lod.css('color', '#e01f1f');
        },
        function() {
            lod.removeAttr('style');
        });

    // MOBILE MENU
    $(".nav-header-mob > a").click(function () {
        $(this).next('ul').slideToggle();
        return false;
    });

    $(".nav-header-mob ul li a").click(function () {
        $(this).next('ul').slideToggle();
        $(this).next('li').toggle();
        return false;
    });

    $(".nav-header-mob ul li ul").click(function () {
        $(this).next('ul#lt');
        return false;
    });

    $(".nav-header-mob ul li ul i.fa-chevron-left").click(function () {
        $("ul#lt").hide();
        return false;
    });

    $(".nav-header-mob ul i.fa-times").click(function () {
        $(".nav-header-mob ul").hide();
        return false;
    });

    $("#title-btn").click(function () {
        $('.modal-call__header-title').removeClass('active-title');
        $('#new-title').addClass('active-title');
    });


    //  MODAL 
    var Modal = {
        elemShowingModal: [$('.header-contact__button, .service-modal__button')],
        elemHidingModal: [$('.wait__call'), $('.modal__close'), $('.overlay')],
        overlay: $('.overlay'),
        window: $(window),
        windowSize: {
            windowWidth: null,
            windowHeight: null
        },
        modal: null,
        modalSize: {
            modalHeight: null,
            modalWidth: null
        },
        getModalSize: function() {
            this.modalSize.modalHeight = this.modal.outerHeight();
            this.modalSize.modalWidth = this.modal.outerWidth();
            //console.log(this.modal)
            //console.log(this.modal.innerWidth())
            //console.log(this.modal.width())
            this.setPosition();
        },
        setPosition: function(modal) {
            this.windowSize.windowWidth = this.window.width();
            this.windowSize.windowHeight = this.window.height();
            var htmlScroll = $('html').scrollTop(),
                    bodyScroll = $('body').scrollTop(),
                    scroll,
                    left,
                    top;
            (htmlScroll) ? scroll = htmlScroll : scroll = bodyScroll;
            if (this.windowSize.windowWidth > this.modalSize.modalWidth) {
                left = (this.windowSize.windowWidth - this.modalSize.modalWidth) * 0.5;
            } else {
                left = 0;
            }
            
            if (this.windowSize.windowHeight > this.modalSize.modalHeight) {
                top = scroll + (this.windowSize.windowHeight - this.modalSize.modalHeight) * 0.5;
            } else {
                top = scroll + 10;
            }
            this.modal.css({'top':top, 'left':left})
        },
        showModal: function(modal) {
            this.modal = modal;
            this.getModalSize();
            this.overlay.fadeIn();
            this.modal.fadeIn().addClass('modal_active');
        },
        closeModal: function() {
            this.overlay.fadeOut();
            this.modal.fadeOut().removeClass('modal_active');
            this.modal.find(".successfull_container_text_accepted").html('');
        }
    }

window.Modal = Modal;
    
    // Show Modals
    for (var i = 0; i < Modal.elemShowingModal.length; i++) {
        Modal.elemShowingModal[i].click(function(e) {
            var modal = $('#' + $(e.target).attr('data-link'));
            Modal.showModal(modal);
        })
    }

    // Close Modals
    for (var i = 0; i < Modal.elemHidingModal.length; i++) {
        Modal.elemHidingModal[i].click(function() {
            Modal.closeModal();
        })
    }

    // Reposition Modals
    $(window).resize(function() {
        if (Modal.modal) {
            setTimeout(function() {
                Modal.setPosition() 
            }, 500);    
        }
    });


});