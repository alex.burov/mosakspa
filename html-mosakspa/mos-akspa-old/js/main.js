'use strict'

$(document).ready(function () {


    let projectsBlock = $('.projects');
    let projectsSlider = new Swiper(projectsBlock.find('.swiper-container'), {
        loop: true,
        spaceBetween: 7,
        autoHeight: true,
        autoplay: {
            delay: 3000,
        },
        // watchSlidesVisibility: true,
        // loopAdditionalSlides: 2,
        navigation: {
            nextEl: projectsBlock.find('.projects__arrow--next'),
            prevEl: projectsBlock.find('.projects__arrow--prev'),
        },
        breakpointsInverse: true,
        breakpoints: {
            0: {
                slidesPerView: 'auto',
                spaceBetween: 13,
            },
            576: {
                slidesPerView: 'auto',
            },
            980: {
                slidesPerView: 3
            }
        }
    });


    // ADDITIONAL JS

    // SCROLLING MENU 
    $(document).scroll (function () {
        if ($(document).scrollTop () >$('nav').height () +1)
            $('.nav-header-mob').addClass ('fixed-mob');
        else
            $('.nav-header-mob').removeClass ('fixed-mob');
    });

    // $(document).scroll (function () {
    //  if ($(document).scrollTop () >$('header').height () +1)
    //      $('.header').addClass ('fixed');
    //  else
    //      $('.header').removeClass ('fixed');
    // });

    $(document).scroll (function () {
        if ($(document).scrollTop () >$('header').height () +1)
            $('.nav_fix').addClass ('fixed');
        else
            $('.nav_fix').removeClass ('fixed');
    }); 

var ekb = $('.ekb-item_mark'),
        bk = $('.ekb_hov');

    ekb.hover(
        function(){
            bk.css(
                'display', 'initial'
            );
        },
        function(){
            bk.removeAttr('style');
        });

    var eburg = $('.ekb-item_mark'),
        akab = $('.ekb_hover');

    eburg.hover(
        function(){
            akab.css(
                'display', 'initial'
            );
        },
        function(){
            akab.removeAttr('style');
        });

    var mg = $('.mg-item_mark'),
        gm = $('.mg_hover');

    mg.hover(
        function(){
            gm.css(
                'display', 'initial'
            );
        },
        function(){
            gm.removeAttr('style');
        });

    var see = $('.che-item_mark'),
        ees = $('.che_hover');

    see.hover(
        function(){
            ees.css(
                'display', 'initial'
            );
        },
        function(){
            ees.removeAttr('style');
        });

    var first = $('.sr-item_mark'),
        sr = $('.sr_hover');

    first.hover(
        function(){
            sr.css(
                'display', 'initial'
            );
        },
        function(){
            sr.removeAttr('style');
        });

    var ven = $('.nev-item_mark'),
    nev = $('.nev_hover');

    ven.hover(
        function() {
            nev.css('display', 'initial');
        },
        function() {
            nev.removeAttr('style');
        });

    var rep = $('.per-item_mark'),
    per = $('.per_hover');

    rep.hover(
        function() {
            per.css('display', 'initial');
        },
        function() {
            per.removeAttr('style');
        });

    var rev = $('.rev-item_mark'),
    ver = $('.rev_hover');

    rev.hover(
        function() {
            ver.css('display', 'initial');
        },
        function() {
            ver.removeAttr('style');
        });

    var kr = $('.kr-item_mark'),
    rk = $('.kr_hover');

    kr.hover(
        function() {
            rk.css('display', 'initial');
        },
        function() {
            rk.removeAttr('style');
        });

    var nov = $('.nov-item_mark'),
    von = $('.nov_hover');

    nov.hover(
        function() {
            von.css('display', 'initial');
        },
        function() {
            von.removeAttr('style');
        });

    var rezh = $('.rezh-item_mark'),
    zer = $('.rezh_hover');

    rezh.hover(
        function() {
            zer.css('display', 'initial');
        },
        function() {
            zer.removeAttr('style');
        });

    var art = $('.art-item_mark'),
    tar = $('.art_hover');

    art.hover(
        function() {
            tar.css('display', 'initial');
        },
        function() {
            tar.removeAttr('style');
        });

    var km = $('.km-item_mark'),
    mk = $('.km_hover');

    km.hover(
        function() {
            mk.css('display', 'initial');
        },
        function() {
            mk.removeAttr('style');
        });

    var paf = $('.nov_mark'),
    vap = $('.nov-item_mark');

    paf.hover(
        function() {
            vap.css('color', '#e01f1f');
        },
        function() {
            vap.removeAttr('style');
        });

    var kon = $('.che_mark'),
    ron = $('.che-item_mark');

    kon.hover(
        function() {
            ron.css('color', '#e01f1f');
        },
        function() {
            ron.removeAttr('style');
        });

    var miki = $('.km_mark'),
    rurg = $('.km-item_mark');

    miki.hover(
        function() {
            rurg.css('color', '#e01f1f');
        },
        function() {
            rurg.removeAttr('style');
        });

    var vait = $('.mg_mark'),
    tap = $('.mg-item_mark');

    vait.hover(
        function() {
            tap.css('color', '#e01f1f');
        },
        function() {
            tap.removeAttr('style');
        });

    var rad = $('.art_mark'),
    dar = $('.art-item_mark');

    rad.hover(
        function() {
            dar.css('color', '#e01f1f');
        },
        function() {
            dar.removeAttr('style');
        });

    var loot = $('.sr_mark'),
    tool = $('.sr-item_mark');

    loot.hover(
        function() {
            tool.css('color', '#e01f1f');
        },
        function() {
            tool.removeAttr('style');
        });

    var hot = $('.rezh_mark'),
    toh = $('.rezh-item_mark');

    hot.hover(
        function() {
            toh.css('color', '#e01f1f');
        },
        function() {
            toh.removeAttr('style');
        });

    var edg = $('.nov_mark'),
    gde = $('.nov-item_mark');

    edg.hover(
        function() {
            gde.css('color', '#e01f1f');
        },
        function() {
            gde.removeAttr('style');
        });

    var chil = $('.rev_mark'),
    cheel = $('.rev-item_mark');

    chil.hover(
        function() {
            cheel.css('color', '#e01f1f');
        },
        function() {
            cheel.removeAttr('style');
        });

    var joji = $('.per_mark'),
    jay = $('.per-item_mark');

    joji.hover(
        function() {
            jay.css('color', '#e01f1f');
        },
        function() {
            jay.removeAttr('style');
        });

    var fat = $('.nev_mark'),
    stuf = $('.nev-item_mark');

    fat.hover(
        function() {
            stuf.css('color', '#e01f1f');
        },
        function() {
            stuf.removeAttr('style');
        });

    var dan = $('.kr_mark'),
    ned = $('.kr-item_mark');

    dan.hover(
        function() {
            ned.css('color', '#e01f1f');
        },
        function() {
            ned.removeAttr('style');
        });

    var kol = $('.ekb_mark'),
    lod = $('.ekb-item_mark');

    kol.hover(
        function() {
            lod.css('color', '#e01f1f');
        },
        function() {
            lod.removeAttr('style');
        });

    // MOBILE MENU
    $(".nav-header-mob > a").click(function () {
        $(this).next('ul').slideToggle();
        return false;
    });

    $(".nav-header-mob ul li a").click(function () {
        $(this).next('ul').slideToggle();
        $(this).next('li').toggle();
        return false;
    });

    $(".nav-header-mob ul li ul").click(function () {
        $(this).next('ul#lt');
        return false;
    });

    $(".nav-header-mob ul li ul i.fa-chevron-left").click(function () {
        $("ul#lt").hide();
        return false;
    });

    $(".nav-header-mob ul i.fa-times").click(function () {
        $(".nav-header-mob ul").hide();
        return false;
    });

    $("#title-btn").click(function () {
        $('.modal-call__header-title').removeClass('active-title');
        $('#new-title').addClass('active-title');
    });


    //  MODAL 
    var Modal = {
        elemShowingModal: [$('.header-contact__button, .service-modal__button')],
        elemHidingModal: [$('.wait__call'), $('.modal__close'), $('.overlay')],
        overlay: $('.overlay'),
        window: $(window),
        windowSize: {
            windowWidth: null,
            windowHeight: null
        },
        modal: null,
        modalSize: {
            modalHeight: null,
            modalWidth: null
        },
        getModalSize: function() {
            this.modalSize.modalHeight = this.modal.outerHeight();
            this.modalSize.modalWidth = this.modal.outerWidth();
            // console.log(this.modal)
            // console.log(this.modal.innerWidth())
            // console.log(this.modal.width())
            this.setPosition();
        },
        setPosition: function(modal) {
            this.windowSize.windowWidth = this.window.width();
            this.windowSize.windowHeight = this.window.height();
            var htmlScroll = $('html').scrollTop(),
                    bodyScroll = $('body').scrollTop(),
                    scroll,
                    left,
                    top;
            (htmlScroll) ? scroll = htmlScroll : scroll = bodyScroll;
            if (this.windowSize.windowWidth > this.modalSize.modalWidth) {
                left = (this.windowSize.windowWidth - this.modalSize.modalWidth) * 0.5;
            } else {
                left = 0;
            }
            
            if (this.windowSize.windowHeight > this.modalSize.modalHeight) {
                top = scroll + (this.windowSize.windowHeight - this.modalSize.modalHeight) * 0.5;
            } else {
                top = scroll + 10;
            }
            this.modal.css({'top':top, 'left':left})
        },
        showModal: function(modal) {
            this.modal = modal;
            this.getModalSize();
            this.overlay.fadeIn();
            this.modal.fadeIn().addClass('modal_active');
        },
        closeModal: function() {
            this.overlay.fadeOut();
            this.modal.fadeOut().removeClass('modal_active');
            this.modal.find(".successfull_container_text_accepted").html('');
        }
    }

window.Modal = Modal;
    
    // Show Modals
    for (var i = 0; i < Modal.elemShowingModal.length; i++) {
        Modal.elemShowingModal[i].click(function(e) {
            var modal = $('#' + $(e.target).attr('data-link'));
            console.log($(e.target).attr('data-link'));
            // console.log(modal);
            Modal.showModal(modal);
        })
    }

    // Close Modals
    for (var i = 0; i < Modal.elemHidingModal.length; i++) {
        Modal.elemHidingModal[i].click(function() {
            Modal.closeModal();
        })
    }

    // Reposition Modals
    $(window).resize(function() {
        if (Modal.modal) {
            setTimeout(function() {
                Modal.setPosition() 
            }, 500);    
        }
    });

    // прячем Header
    // фиксируем меню
    $(document).scroll (function () {
        var width = $(window).width();
        if(width>=1024){
             let shift = $('.header').height () - $('.nav').height ();
            if ($(document).scrollTop () > $('.header').height () +1){
                $('.header').css( "top", "-"+shift+"px");
            }else{
                $('.header').css( "top", "0px" );
            }
        }       
    });

    var scrollingFlag=false;
    // $("#ac-1").attr("checked","checked");
    // $("#tab1").attr("checked","checked");
    $('.reviews-slide').owlCarousel({
        margin: 50,
        loop: true,
        nav: true,
        autoHeight: true,
        navText: '',
        responsive: {
            991: {
                items: 2,
            },
            600: {
                 items: 2,
            },
            320: {
                 items: 1,
            }
         }
    });


});