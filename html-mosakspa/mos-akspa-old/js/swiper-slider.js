'use strict'

$(document).ready(function () {


 let projectsBlock = $('.slider');
    var Swipes = new Swiper('.swiper-container', {
        loop: true,
        navigation: {
            nextEl: projectsBlock.find('.sliders__arrow--next'),
            prevEl: projectsBlock.find('.sliders__arrow--prev'),        
        },
        pagination: {
            el: '.swiper-pagination',
        },
        breakpoints: {
            0: {
                slidesPerView: 'auto',
                spaceBetween: 15,

            },
            576: {
                slidesPerView: 2,
                spaceBetween: 25,
            },
            980: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
        },
        effect: 'slide',
        scrollbar: false,
        pagination: false

    });
});