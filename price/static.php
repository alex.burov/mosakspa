<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Прайслист");?>  
<div class="container">   
  <div class="accordion">
    <section class="accordion-item accordion-item_opened">
        <div class="accordion-item--title">
           Установка счетчиков воды для Москвы                                                                   
        </div>
        <div class="accordion-item--content" style="display: block;">
          <div class="table-responsiver">
              <table>
                <thead>
                    <tr>
                      <th>Вид работ</th>
                      <th>Стоимость</th>
                    </tr>
                </thead>
                <tbody>
                   <tr>
                      <td>
                          <span>Счетчик Россия Эконом 1 шт.</span>
                      </td>
                      <td>
                          <span>от 1500 руб.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <span>Счетчик Россия Эконом 2 шт.</span>
                      </td>
                      <td>
                          <span>от 3000 руб.</span>
                      </td>
                   </tr>
                    <tr>
                      <td>
                          <span>Счетчик Германия Itelma 1 шт.</span>
                      </td>
                      <td>
                          <span>от 1600 руб.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <span>Счетчик Германия Itelma 2 шт.</span>
                      </td>
                      <td>
                          <span>от 3200 руб.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <span>Счетчик Италия Valtec 1 шт.</span>
                      </td>
                      <td>
                          <span>от 1700 руб.</span>
                      </td>
                   </tr>
                    <tr>
                       <td>
                          <span>Счетчик Италия Valtec 2 шт.</span>
                      </td>
                      <td>
                          <span>от 3400 руб.</span>
                      </td>
                    </tr>
                </tbody>
              </table>
          </div>
        </div>
      </section>

     <section class="accordion-item ">
       <div class="accordion-item--title h4">
           Установка счетчиков воды для Московской области                                                                   
        </div>
        <div class="accordion-item--content" style="display: none;">
          <div class="table-responsiver">
              <table>
                <thead>
                    <tr>
                      <th>Вид работ</th>
                      <th>Стоимость</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td>
                          <span>Счетчик Россия Эконом 1 шт.</span>
                      </td>
                      <td>
                          <span>от 2000 руб.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <span>Счетчик Россия Эконом 2 шт.</span>
                      </td>
                      <td>
                          <span>от 4000 руб.</span>
                      </td>
                   </tr>
                   <tr>
                      <td>
                          <span>Счетчик Германия Itelma 1 шт.</span>
                      </td>
                      <td>
                          <span>от 2200 руб.</span>
                      </td>
                    </tr>
                   <tr>
                       <td>
                         <span>Счетчик Германия Itelma 2 шт.</span>
                       </td>
                      <td>
                          <span>от 4400 руб.</span>
                      </td>
                   </tr>
                   <tr>
                       <td>
                         <span>Счетчик Италия Valtec 1 шт.</span>
                       </td>
                      <td>
                          <span>от 2500 руб.</span>
                      </td>
                    </tr>
                   <tr>
                    <td>
                          <span>Счетчик Италия Valtec 2 шт.</span>
                       </td>
                      <td>
                          <span>от 5000 руб.</span>
                       </td>
                    </tr>
                </tbody>
              </table>
          </div>
       </div>
     </section>

    <section class="accordion-item ">
        <div class="accordion-item--title h4">
           Замена счетчиков воды для Москвы                                                                  
        </div>
       <div class="accordion-item--content" style="display: none;">
          <div class="table-responsiver">
              <table>
                <thead>
                   <tr>
                      <th>Вид работ</th>
                      <th>Стоимость</th>
                   </tr>
                </thead>
                 <tbody>
                    <tr>
                      <td>
                          <span>Счетчик Россия Эконом 1 шт.</span>
                      </td>
                      <td>
                          <span>от 1200 руб.</span>
                      </td>
                    </tr>
                   <tr>
                       <td>
                          <span>Счетчик Россия Эконом 2 шт.</span>
                      </td>
                       <td>
                          <span>от 2400 руб.</span>
                      </td>
                    </tr>
                    <tr>
                       <td>
                         <span>Счетчик Германия Itelma 1 шт.</span>
                      </td>
                      <td>
                          <span>от 1350 руб.</span>
                      </td>
                   </tr>
                    <tr>
                      <td>
                          <span>Счетчик Германия Itelma 2 шт.</span>
                      </td>
                      <td>
                          <span>от 2700 руб.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span>Счетчик Италия Valtec 1 шт.</span>
                       </td>
                      <td>
                          <span>от 1400 руб.</span>
                       </td>
                    </tr>
                    <tr>
                      <td>
                          <span>Счетчик Италия Valtec 2 шт.</span>
                      </td>
                      <td>
                         <span>от 2800 руб.</span>
                      </td>
                    </tr>
                 </tbody>
             </table>
          </div>
        </div>
      </section>

      <section class="accordion-item ">
          <div class="accordion-item--title h4">
             Замена счетчиков воды для Московской области                                                                  
         </div>
          <div class="accordion-item--content" style="display: none;">
            <div class="table-responsiver">
                <table>
                  <thead>
                      <tr>
                        <th>Вид работ</th>
                        <th>Стоимость</th>
                      </tr>
                   </thead>
                   <tbody>
                      <tr>
                         <td>
                            <span>Счетчик Россия Эконом 1 шт.</span>
                        </td>
                        <td>
                            <span>от 1800 руб.</span>
                        </td>
                     </tr>
                    <tr>
                         <td>
                           <span>Счетчик Россия Эконом 2 шт.</span>
                        </td>
                        <td>
                            <span>от 3600 руб.</span>
                        </td>
                     </tr>
                      <tr>
                        <td>
                            <span>Счетчик Германия Itelma 1 шт.</span>
                        </td>
                        <td>
                           <span>от 2000 руб.</span>
                        </td>
                      </tr>
                      <tr>
                        <td>
                           <span>Счетчик Германия Itelma 2 шт.</span>
                         </td>
                        <td>
                           <span>от 4000 руб.</span>
                         </td>
                      </tr>
                      <tr>
                        <td>
                            <span>Счетчик Италия Valtec 1 шт.</span>
                         </td>
                        <td>
                            <span>от 2200 руб.</span>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <span>Счетчик Италия Valtec 2 шт.</span>
                         </td>
                        <td>
                            <span>от 4400 руб.</span>
                        </td>
                      </tr>
                  </tbody>
                </table>
             </div>
        </div>
      </section>

      <section class="accordion-item ">
          <div class="accordion-item--title h4">
            Поверка счетчиков воды для Москвы                                                                 
          </div>
          <div class="accordion-item--content" style="display: none;">
            <div class="table-responsiver">
                <table>
                  <thead>
                      <tr>
                        <th>Вид работ</th>
                        <th>Стоимость</th>
                      </tr>
                   </thead>
                  <tbody>
                      <tr>
                        <td>
                            <span>Поверка счетчика</span>
                        </td>
                        <td>
                            <span>990 руб. <strike>1100 руб.</strike></span>
                        </td>
                      </tr>
                  </tbody>
                </table>
            </div>
          </div>
      </section>

      <section class="accordion-item ">
          <div class="accordion-item--title h4">
                              Поверка счетчиков воды для Московской области                                                                 
          </div>
          <div class="accordion-item--content" style="display: none;">
            <div class="table-responsiver">
                <table>
                  <thead>
                      <tr>
                        <th>Вид работ</th>
                        <th>Стоимость</th>
                     </tr>
                  </thead>
                   <tbody>
                     <tr>
                        <td>
                           <span>Поверка счетчика в Московской области</span>
                                          </td>
                        <td>
                           <span>от 1200 руб.</span>
                        </td>
                      </tr>
                  </tbody>
               </table>
            </div>
          </div>
       </section>

        <section class="accordion-item ">
            <div class="accordion-item--title h4">
                              Опломбировка счетчиков воды для Москвы и Московской области                                                                   
          </div>
           <div class="accordion-item--content" style="display: none;">
               <div class="table-responsiver">
                 <table>
                    <thead>
                        <tr>
                           <th>Вид работ</th>
                          <th>Стоимость</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>
                             <span>Опломбировка счётчика</span>
                          </td>
                           <td>
                              <span>500 руб.</span>
                          </td>
                        </tr>
                    </tbody>
                  </table>
              </div>
           </div>
        </section>


        <section class="accordion-item ">
            <div class="accordion-item--title h4">
              Шеф монтаж счетчиков воды (ввод в эксплуатацию) для Москвы и Московской области                                                                   
           </div>
            <div class="accordion-item--content" style="display: none;">
              <div class="table-responsiver">
                 <table>
                  <thead>
                        <tr>
                          <th>Вид работ</th>
                          <th>Стоимость</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>
                              <span>Шеф монтаж счетчика воды (ввод в эксплуатацию)</span>
                          </td>
                           <td>
                             <span>1250 руб.</span>
                          </td>
                        </tr>
                    </tbody>
                  </table>
              </div>
            </div>
        </section>
        
        <section class="accordion-item ">
            <div class="accordion-item--title h4">
              Снятие контрольных показаний счетчиков воды для Москвы и Московской области                                                                   
            </div>
            <div class="accordion-item--content" style="display: none;">
              <div class="table-responsiver">
                 <table>
                    <thead>
                        <tr>
                          <th>Вид работ</th>
                           <th>Стоимость</th>
                        </tr>
                     </thead>
                    <tbody>
                       <tr>
                          <td>
                              <span> Снятие контрольных показаний счетчиков воды</span>
                           </td>
                          <td>
                              <span>от 1000 руб.</span>
                           </td>
                        </tr>
                     </tbody>
                  </table>
              </div>
           </div>
         </section>
      </div>
</div>
<div class="bottom"></div>
  <script type="text/javascript">
    
    /* Аккордеон */
$(document).ready(function () {
    var o, n;
    $(".accordion-item--title").on("click", function () {
        (o = $(this).parents(".accordion-item")),
            (n = o.find(".accordion-item--content")),
            o.hasClass("accordion-item_opened")
                ? (o.removeClass("accordion-item_opened"), n.slideUp())
                : (o.addClass("accordion-item_opened"), n.stop(!0, !0).slideDown(), o.siblings(".accordion-item_opened").removeClass("accordion-item_opened").children(".accordion-item--content").stop(!0, !0).slideUp());
    });
});

  </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>