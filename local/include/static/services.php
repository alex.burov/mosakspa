<div class="container">
     <h2 class="title-color">Наши услуги</h2>
</div>

<div class="container ">
      <div class="row row-flex items services">
          
          <div class="item">
              <div class="body">
                 <div class="img">
                    <img class="img-responsive imgWebp" alt="Поверка счетчика" src="/local/include/static/img/services/mosakspa-verification.png"> 
                </div>
                <div class="text">
                    <div class="title"> 
                        <a class="order" href="poverka-schetchikov-vody" data-id="8" data-title="Вызов мастера на поверку счетчик(а/ов)">Поверка счетчика</a> 
                    </div>
                    <div class="subtitle">Снижена цена на поверку прибора на дому:</div>
                    <div class="price">1500 р.</div>  
                 </div>
              </div>
              <button class="order btn btn_white header-contact__button" data-link="modal-call" data-title="Вызов мастера на поверку счетчик(а/ов)">Заказать</button>
          </div>

          <div class="item">
            <div class="body">
              <div class="img">
                  <img class="img-responsive imgWebp" src="/local/include/static/img/services/mosakspa-quick-verification.png"> 
              </div>
              <div class="text">
                  <div class="title"> 
                      <a class="order" href="poverka-schetchikov-vody" data-id="8" data-title="Вызов мастера на поверку счетчик(а/ов)">Экспресс поверка</a>
                  </div>
                  <div class="subtitle">Премиум тариф. Подробности уточняйте у операторов</div>
                  <div class="price">2000 р.</div>
              </div>                       
            </div>
            <button class="order btn btn_white header-contact__button" data-link="modal-call" data-title="Вызов мастера на поверку счетчик(а/ов)">Заказать</button>
          </div>

          <div class="item">
            <div class="body">
                <div class="img">
                    <img class="img-responsive imgWebp" src="/local/include/static/img/services/replacement.png"> 
                </div>
                <div class="text">
                    <div class="title"> 
                      <a class="order" href="zamena-schetchikov-vody" data-id="9" data-title="Вызов мастера на замену счетчик(а/ов)">Замена счетчика</a> 
                    </div>
                    <div class="subtitle">Снижена цена работ по замене водяных измерителей:</div>
                    <div class="price">2500 р.</div>
                </div>
                        
                        
            </div>
                    <button class="order btn btn_white header-contact__button" data-link="modal-call" data-title="Вызов мастера на замену счетчик(а/ов)">Заказать</button>
          </div>

          <div class="item">
            <div class="body">
                <div class="img">
                    <img class="img-responsive imgWebp" src="/local/include/static/img/services/installation.png"> 
                </div>
                <div class="text">
                    <div class="title"> <a class="order" href="ustanovka-schetchikov-vody" data-id="7" data-title="Вызов мастера на установку счетчик(а/ов)">Установка счетчика</a> </div>
                    <div class="subtitle">В стоимость включены водомер и документы</div>
                    <div class="price">2500 р.</div>
                </div>                       
              </div>  
              <button class="order btn btn_white header-contact__button" data-link="modal-call" data-title="Вызов мастера на установку счетчик(а/ов)">Заказать</button>
           </div>
    </div>
</div>