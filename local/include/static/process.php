<div class="research section right">
  <div class="container">
    <div class="research__wrapper">                    
        <div class="col-12 col-lg-12 order-lg-1">
                <h3 class="title-color">Как мы работаем</h3>
                <div class="col-xs-12 justify-content-lg-between process">

                  <div class="item">
                      <div class="img">
                        <img class="img-responsive imgWebp" alt="Свяжитесь с оператором или закажите звонок" src="/local/include/static/img/process/hwaw-call.png">
                      </div>
                      <div class="title">Свяжитесь с оператором или закажите звонок</div>
                  </div>

                  <div class="item">
                    <div class="img">
                      <img class="img-responsive imgWebp" alt="В назначенное время к вам приедет мастер" src="/local/include/static/img/process//hwaw-tome.png">
                     </div>
                     <div class="title">В назначенное время к вам приедет мастер</div>
                  </div>
                            
                  <div class="item">
                    <div class="img">
                        <img class="img-responsive imgWebp" alt="Произведёт заказанные вами работы и услуги" src="/local/include/static/img/process/hwaw-services.png">
                     </div> 
                     <div class="title">Произведёт заказанные вами работы и услуги</div>
                  </div>
                             
                  <div class="item">
                    <div class="img">
                      <img class="img-responsive imgWebp" alt="Выписка документов для ЕРЦ и УК" src="/local/include/static/img/process/hwaw-docs.png">
                    </div>
                    <div class="title">Выписка документов для ЕРЦ и УК</div>
                  </div>
                  
              </div>
        </div>
    </div>
  </div>
</div>
