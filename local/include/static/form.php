<div class="container">
    <div class="col-12 col-lg-12 order-lg-1">
        <div class="research__wrapper" style="padding: 1em 0 1em 0;">

            <h3 class="title-color">Отправить заявку</h3>
               
                <div class="container">
                    <div class="order-form">                                  
                        <div class="order-form-block">
                            <form class="mosakspa-request-form">
                    	            <div class="form-group dp-block calendar">
                    	               <div class="dp"> 
                    	                   <input type="hidden" name="date" class="form-control"> 
                    	               </div>
                    	           </div>

                    	            <div class="form-group text-center date-time-block">
                    	               <div class="date-time"></div>
                    	            </div>

                                <div class="form-group time-group">
                                    <label>Время в которое придет мастер:</label> 
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default" data-time="1">9:00 - 11:00</button> 
                                        </div>
                                         <div class="btn-group" role="group"> 
                                             <button type="button" class="btn btn-default" data-time="2">11:00 - 13:00</button> 
                                        </div>
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default" data-time="3">13:00 - 15:00</button> 
                                        </div>
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default" data-time="4">15:00 - 17:00</button> 
                                        </div>
                                                                                
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default" data-time="5">17:00 - 19:00</button> 
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group mb32">
                                        <label>Вид услуги:</label> 
                    	                <select name="services" class="form-control">
                    	                   <option value="">Выберите услугу</option>
                    	                   <option value="Поверка счетчиков воды" data-id="8">Поверка счетчиков</option>
                    	                   <option value="Замена счетчиков воды" data-id="9">Замена счетчиков</option>
                    	                   <option value="Установка теплосчетчиков" data-id="289">Установка счетчиков</option>
                    	                   <option value="Сантехнические работы" data-id="250">Сантехнические работы</option>
                    	               </select>
                                </div>

                                <div class="form-group mb32"> 
                                    <label>Ваше имя *:</label> 
                                    <input type="text" name="name" class="form-control" placeholder="Ваше имя" required> 
                                </div>

                                                
                                <div class="selectCityBlk selectCityBlkPopUp mb32">                                
                                    <div class="inputCont regionInsideContainer inputContSelectArrow form-group" style="display:none;">
                                        <input type="text" name="regionInside" class="regionInside form-control" placeholder="Выберите район Москвы" value="" autocomplete="off"> 
                                        <div class="regionInsideSelect"></div>
                                        <input type="hidden" class="regionInsideRealVal" name="regionInsideRealVal" value=""> 
                                    </div>
                                </div>

                                <div class="form-group mb32"> 
                                    <label>Ваш телефон *:</label> 
                                    <input type="tel" name="phone" class="form-control" placeholder="Ваш телефон" required> 
                                </div>

                                <div class="form-group mb32">
                                    <p>* поля, обязательные для заполнения</p>
                                </div>

                                <div class="form-group text-center btn-box">
                                        <button type="submit" class="btn">Отправить заявку</button> 
                                        <div class="btn-for-modal"> <button type="reset" class="btn btn-default cancel mt16">Отмена</button> </div>
                                </div>
                                <input type="hidden" name="af_action" value="89e6ae6985628f28095e4f3023f8a8f6" /> 

                            </form>

                        </div>
                    </div>
                </div>
                   
            </div>

    </div>
</div>