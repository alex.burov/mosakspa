<div class="container">
    <h3 class="title-color">Наши клиенты</h3>                
    <div class="slider" style=" margin: 50px 100px; position: relative;"> 

        <div class="sliders__arrow sliders__arrow--prev"><</div>
        <div class="sliders__arrow sliders__arrow--next">></div>
        <div class="swiper-container">
            <div class="swiper-wrapper">

                <div class="swiper-slide">
                    <div class="slides__slide-wrapper">
                    <div class="slides__logo-wrapper">
                        <a href="#">
                            <img class="slides__slide-logo" src="/local/include/static/img/clients/lukoil.png" alt="" role="presentation"/>
                        </a>                                      
                    </div>

                     </div>
                </div>

                <div class="swiper-slide">
                    <div class="slides__slide-wrapper">
                     <div class="slides__logo-wrapper">
                        <a href="#">
                            <img class="slides__slide-logo" src="/local/include/static/img/clients/moscow.png" alt="" role="presentation"/>
                        </a>  
                    </div>
                                    
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="slides__slide-wrapper">

                    <div class="slides__logo-wrapper">
                        <a href="#">
                            <img class="slides__slide-logo" src="/local/include/static/img/clients/msk.png" alt="" role="presentation"/>
                         </a>  
                    </div>
                                   
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="slides__slide-wrapper">
                    <div class="slides__logo-wrapper">
                        <a href="#">
                            <img class="slides__slide-logo" src="/local/include/static/img/clients/pik.png" alt="" role="presentation"/>
                        </a>  
                    </div>
                                  
                    </div>
                </div>
                               
            </div>
        </div>                  
    </div>         
</div>

<script type="text/javascript">  /*  
    let projectsBlock = $('.projects');
    let projectsSlider = new Swiper(projectsBlock.find('.swiper-container'), {
        loop: true,
        spaceBetween: 7,
        autoHeight: true,
        autoplay: {
            delay: 3000,
        },
        // watchSlidesVisibility: true,
        // loopAdditionalSlides: 2,
        navigation: {
            nextEl: projectsBlock.find('.projects__arrow--next'),
            prevEl: projectsBlock.find('.projects__arrow--prev'),
        },
        breakpointsInverse: true,
        breakpoints: {
            0: {
                slidesPerView: 'auto',
                spaceBetween: 13,
            },
            576: {
                slidesPerView: 'auto',
            },
            980: {
                slidesPerView: 3
            }
        }
    });*/
</script>