<div class="experience section">
    <div class="container">
        <div class="experience__wrapper">
              
              <div class="animate-line animate-line__animated">
                <span class="-animate-line__one"></span>
                <span class="-animate-line__two"></span>
                <span class="-animate-line__three"></span>
                <span class="-animate-line__four"></span>
              </div>


              <h2 class="experience__title title-color"><span>Наше преимущества</span></h2>

              <div class="justify-content-lg-between advantage-items">
                     <div class="item">
                     <div class="img"> <img class="img-responsive imgWebp"alt="Выезд в день заявки" src="/local/include/static/img/advantage/specialist-at-home-light.png"> </div>
                     <div class="body">
                        <div class="h3 text-center">Выезд в день заявки</div>
                        <div class="desc text-left">
                           <p>Вы сами выбираете время. Мы пришлём мастера к назначенному сроку.</p>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="img"> <img class="img-responsive imgWebp" alt="Низкие цены" src="/local/include/static/img/advantage/mosakspa-low-price.png"> </div>
                     <div class="body">
                        <div class="h3 text-center">Низкие цены</div>
                        <div class="desc text-left">
                           <p>Специальные условия на все виды работ позволят вам сэкономить ваши деньги.</p>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="img"> <img class="img-responsive imgWebp" alt="Квалифицированных инженеров" src="/local/include/static/img/advantage/qualified-engineers.png"> </div>
                     <div class="body">
                        <div class="h3 text-center">Квалифицированных инженеров</div>
                        <div class="desc text-left">
                           <p>Сертифицированные специалисты Городского Центра Учёта и Экономии Ресурсов быстро и качественно выполнят весь необходимый спектр работ.</p>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="img"> <img class="img-responsive imgWebp" alt="Выполнение работ «Под ключ»" src="/local/include/static/img/advantage/mosakspa-portfolio.png"> </div>
                     <div class="body">
                        <div class="h3 text-center">Выполнение работ «Под ключ»</div>
                        <div class="desc text-left">
                           <p>Установленные приборы учёта уже готовы к службе и не требуют проведения каких-либо дополнительных работ.</p>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="img"> <img class="img-responsive imgWebp" src="/local/include/static/img/advantage/mosakspa-quality-control.png"> </div>
                     <div class="body">
                        <div class="h3 text-center">Контроль качества на всех этапах</div>
                        <div class="desc text-left">
                           <p>Городской Центр Учёта и Экономии Ресурсов гарантирует высокий уровень исполнения. Качество для нас - не пустой звук.</p>
                        </div>
                     </div>
                  </div>
              </div>

          </div>
    </div>
</div>