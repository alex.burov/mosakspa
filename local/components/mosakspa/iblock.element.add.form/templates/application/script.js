$(document).ready(function(){

    // выполнен
    $(document).on('change', '#done select', function (event) {
        let done_id = $(this).find(":selected").val();
        
        if(done_id !==''){
            let DONE_XML_ID = LINKED_FIELDS['DONE']['ENUM'][done_id]['XML_ID'];        
            switch (DONE_XML_ID) { 
        		case "yes":
        		    // console.log("Поверка счетчиков");
                    $('#service_type').show();  
                    $('#rejection_reason').hide();    
                    $('#trasfer_master').hide();
                    $('#transfer_date').hide();
                    $('#transfer_time').hide(); 
                    $('#claim').hide();                
        		break;
        
        		case "no":
        		    // console.log("Замена счетчиков");
                    $('#rejection_reason').show();
                    $('#service_type').hide(); 
                    $('#trasfer_master').hide();
                    $('#transfer_date').hide();
                    $('#transfer_time').hide(); 
                    $('#claim').hide();                                       
        		break;
        
        		case "transfer":
        		    // console.log("Установка счетчиков");
                    $('#trasfer_master').show();
                    $('#transfer_date').show();
                    $('#transfer_time').show();                    
                    $('#service_type').hide();
                    $('#rejection_reason').hide();
                    $('#claim').hide();
        		break;

                case "claim":
                    $('#claim').show();
                    $('#transfer_date').hide();
                    $('#transfer_time').hide();                    
                    $('#service_type').hide();
                    $('#rejection_reason').hide();
                break;
        
        		default:
        		    // console.log("(не установлено)");
                    $('#service_type').hide();
        	}
        }
    });


    // вид услуги
    $(document).on('change', '#service_type select', function (event) {
        // let err = obj.checkField($(this));
        // console.log($(this));
        let type_id = $(this).find(":selected").val();
        // console.log(type_id);
        // console.log(LINKED_FIELDS['SERVICE_TYPE']['ENUM'][type_id]);

        if(type_id !==''){
                let SERVICE_TYPE_XML_ID = LINKED_FIELDS['SERVICE_TYPE']['ENUM'][type_id]['XML_ID'];
        
                switch (SERVICE_TYPE_XML_ID) {
        		  	case "1":
        		    	console.log("Поверка счетчиков");
        		    break;
        
        		    case "2":
        		    	console.log("Замена счетчиков");
        		    break;
        
        		    case "3":
        		    	console.log("Установка счетчиков");
        		    break;
        
        		    case "4":
        		    	console.log("Сантехнические работы");
        		    break;
        			
        			default:
        		    	console.log("(не установлено)");
        		}
        }
    });

});