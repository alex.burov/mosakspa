<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):?>
	<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif;
if ($arResult["MESSAGE"] <> ''):?>
	<?ShowNote($arResult["MESSAGE"])?>
<?endif?>
<?if($arParams["CODE"]):?>
	
	<h2>Фома редактирования заявки</h2>
<?else:?>	
<h2>Фома создания заявки</h2>
<?endif?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<?=bitrix_sessid_post()?>
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	<div class="data-table">
		<div><div colspan="2">&nbsp;</div></div>
		<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
		<div class="box">
			<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
				<?
					// pr($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']);
					// if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] == 'SERVICE_TYPE'){
					// 	pr($arResult["PROPERTY_LIST_FULL"][$propertyID]);
					// }
				?>
				<div class="field-box">
					<div class="field-header">
						<?if(intval($propertyID) > 0):?>
							<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
						<?else:?>
							<?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)?>
						<?endif?>
						<?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
							<span class="starrequired">*</span>
						<?endif?>
					</div>
					<div class="field">
						<?
						if (intval($propertyID) > 0)
						{
							if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
							elseif (
								(
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
									||
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
						}
						elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
						{
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						}
						else
						{
							$inputNum = 1;
						}

						if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
							$INPUT_TYPE = "USER_TYPE";
						else
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

						switch ($INPUT_TYPE):
							case "USER_TYPE":
								for ($i = 0; $i<$inputNum; $i++)
								{
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
										$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										$description = "";
									}
									else
									{
										$value = "";
										$description = "";
									}
									echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
										array(
											$arResult["PROPERTY_LIST_FULL"][$propertyID],
											array(
												"VALUE" => $value,
												"DESCRIPTION" => $description,
											),
											array(
												"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
												"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
												"FORM_NAME"=>"iblock_add",
											),
										));
								?><br /><?
								}
							break;
							case "TAGS":
								$APPLICATION->IncludeComponent(
									"bitrix:search.tags.input",
									"",
									array(
										"VALUE" => $arResult["ELEMENT"][$propertyID],
										"NAME" => "PROPERTY[".$propertyID."][0]",
										"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
									), null, array("HIDE_ICONS"=>"Y")
								);
								break;
							case "HTML":
								$LHE = new CHTMLEditor;
								$LHE->Show(array(
									'name' => "PROPERTY[".$propertyID."][0]",
									'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
									'inputName' => "PROPERTY[".$propertyID."][0]",
									'content' => $arResult["ELEMENT"][$propertyID],
									'width' => '100%',
									'minBodyWidth' => 350,
									'normalBodyWidth' => 555,
									'height' => '200',
									'bAllowPhp' => false,
									'limitPhpAccess' => false,
									'autoResize' => true,
									'autoResizeOffset' => 40,
									'useFileDialogs' => false,
									'saveOnBlur' => true,
									'showTaskbars' => false,
									'showNodeNavi' => false,
									'askBeforeUnloadPage' => true,
									'bbCode' => false,
									'siteId' => SITE_ID,
									'controlsMap' => array(
										array('id' => 'Bold', 'compact' => true, 'sort' => 80),
										array('id' => 'Italic', 'compact' => true, 'sort' => 90),
										array('id' => 'Underline', 'compact' => true, 'sort' => 100),
										array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
										array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
										array('id' => 'Color', 'compact' => true, 'sort' => 130),
										array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
										array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
										array('separator' => true, 'compact' => false, 'sort' => 145),
										array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
										array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
										array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
										array('separator' => true, 'compact' => false, 'sort' => 200),
										array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
										array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
										array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
										array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
										array('separator' => true, 'compact' => false, 'sort' => 290),
										array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
										array('id' => 'More', 'compact' => true, 'sort' => 400)
									),
								));
								break;
							case "T":
								for ($i = 0; $i<$inputNum; $i++)
								{

									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									}
									elseif ($i == 0)
									{
										$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									}
									else
									{
										$value = "";
									}
								?>
									<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
								<?
								}
							break;

							case "S":
							case "N":
								if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='OPERATOR' && $arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='SEARCH_ID'){
									for ($i = 0; $i<$inputNum; $i++)
									{
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];

										}
										elseif ($i == 0) 
										{
											$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

										}
										else
										{
											$value = "";
										}
									?>
									<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>" value="<?=$value?>" /><br /><?
									if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
										$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => 'iblock_add',
												'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
												'INPUT_VALUE' => $value,
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);
										?><br /><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
									endif
									?><br /><?
									}
								}else{

									for ($i = 0; $i<$inputNum; $i++)
									{
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];

										}
										elseif ($i == 0)
										{
											$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

										}
										else
										{
											$value = "";
										}
									?>
										<p style="margin-top: 10px; color:#000;">текущий пользователь</p>
										<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>" value="<?=$USER->GetID()?>" />
									<?
									}
								}
							break;

							case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?>
										<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
										<br />
										<input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" />
										<br />
									<?

									if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
									{
										?>
											<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" />
											<label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label>
											<br />
										<?

										if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
										{
											?>
											<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" class="img"/>
											<?
										}
										else
										{
											?>
					<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
					<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
					[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
											<?
										}
									}
								}



							break;
							case "L":

								if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
								else
									$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

								switch ($type):
									case "checkbox":
									case "radio":
										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
												{
													foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
													{
														if ($arElEnum["VALUE"] == $key)
														{
															$checked = true;
															break;
														}
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}

											?>
							<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
											<?
										}
									break;

									case "dropdown":
									case "multiselect":
									?>
							<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='MASTER' && 
								$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='CAMPAIGN' &&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='METRO' &&
							 	$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='DISTRICTS' &&
							    $arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='OPERATOR' &&  
							    $arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] !=='TRANSFER_MASTER'):?>
							<select name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
								<option value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
									<?
										if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
										else $sKey = "ELEMENT";

										foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
										{
											$checked = false;
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
												{
													if ($key == $arElEnum["VALUE"])
													{
														$checked = true;
														break;
													}
												}
											}
											else
											{
												if ($arEnum["DEF"] == "Y") $checked = true;
											}
											?>
								<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
											<?
										}
									?>
							</select>
							<?else:?>  

								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='CAMPAIGN'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CAMPAIGNS"]):?>

									<?
										// pr($arResult["ELEMENT_PROPERTIES"][$propertyID][0]);
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">										
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CAMPAIGNS"] as $key => $campaign):?>
															<option value="<?=$campaign['ID']?>" <?if($campaign['ID']==$selected):?> selected <?endif?> >
																<?=$campaign["NAME"]?>									
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных записей нет</span>
									<?endif?>
								<?endif?>

								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='MASTER'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MASTERS"]):?>
									<?
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">										
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MASTERS"] as $key => $master):?>
															<option value="<?=$master['ID']?>" <?if($master['ID']==$selected):?> selected <?endif?> >
																<?=$master["LOGIN"]?>																
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных записей нет</span>
									<?endif?>
								<?endif?>

								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='TRANSFER_MASTER'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["MASTERS"]):?>
									<?
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">										
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MASTERS"] as $key => $master):?>
															<option value="<?=$master['ID']?>" <?if($master['ID']==$selected):?> selected <?endif?> >
																<?=$master["LOGIN"]?>																
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных записей нет</span>
									<?endif?>
								<?endif?>

								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='OPERATOR'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["OPERATORS"]):?>
									<?
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">										
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["OPERATORS"] as $key => $operator):?>
															<option value="<?=$operator['ID']?>" <?if($operator['ID']==$selected):?> selected <?endif?> >
																<?=$operator["LOGIN"]?>																
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных записей нет</span>
									<?endif?>
								<?endif?>


								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='DISTRICTS'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["DISTRICTS"]):?>

									<?
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">					
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["DISTRICTS"] as $key => $district):?>
															<option value="<?=$district['ID']?>" <?if($district['ID']==$selected):?> selected <?endif?> >
																<?=$district["NAME"]?>
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных записей нет</span>
									<?endif?>
								<?endif?>

								<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'] =='METRO'):?> 
									<?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["METRO"]):?>

									<?
										$selected = false;
										if($arResult["ELEMENT_PROPERTIES"][$propertyID]){
											$selected = $arResult["ELEMENT_PROPERTIES"][$propertyID][0]['VALUE'];
										}
									?>				
										<span class="fields boolean field-wrap">
											<span class="fields boolean field-item">					
													<select name ="PROPERTY[<?=$propertyID?>][]"> 
														<?foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["METRO"] as $key => $metro):?>
															<option value="<?=$metro['ID']?>" <?if($metro['ID']==$selected):?> selected <?endif?> >
																<?=$metro["NAME"]?>
															</option>
														<?endforeach?>
													</select> 
											</span>
										</span>								
									<?else:?>
									<span class="fields boolean field-wrap">Доступных мастеров нет</span>
									<?endif?>
								<?endif?>

								<div class="space" style=""></div>
							<?endif?>	
									<?
									break;

								endswitch;
							break;
						endswitch;?>
					</div>
				</div>
			<?endforeach;?>


			<!-- <div class="field-box">
				<div class="field-header">Выполнен</div>
				<div class="field">
					<select name="PROPERTY[31]">
						<option value="">(не установлено)</option>
						<option value="6">выполнен</option>
						<option value="7">не выполнен</option>
						<option value="30">перенос</option>
					</select>								
				</div>
			</div>

			<div class="field-box">
				<div class="field-header">Тип услуги</div>
				<div class="field">
					<select name="PROPERTY[30]">
						<option value="">(не установлено)</option>
						<option value="8">Поверка счетчиков</option>
						<option value="9">Замена счетчиков</option>
						<option value="10">Установка счетчиков</option>
						<option value="11">Сантехнические работы</option>
					</select>								
				</div>
			</div>

			<div class="field-box">
				<div class="field-header">Дата прихода мастера</div>
				<div class="field">
					<input type="text" name="PROPERTY[38][0][VALUE]" size="25" value="10.05.2021">
					<img src="/bitrix/js/main/core/images/calendar-icon.gif" alt="Выбрать дату в календаре" class="calendar-icon" onclick="BX.calendar({node:this, field:'PROPERTY[38][0][VALUE]', form: 'iblock_add', bTime: false, currentTime: '1620736663', bHideTime: false});" onmouseover="BX.addClass(this, 'calendar-icon-hover');" onmouseout="BX.removeClass(this, 'calendar-icon-hover');" border="0"><br>					
				</div>
			</div>

			<div class="field-box">
				<div class="field-header">Время прихода мастера (для переноса)</div>
				<div class="field">
					<select name="PROPERTY[54]">
						<option value="">(не установлено)</option>
						<option value="31">9:00 - 11:00</option>
						<option value="32">11:00 - 13:00</option>
						<option value="33">13:00 - 15:00</option>
						<option value="34">15:00 - 17:00</option>
						<option value="35">17:00 - 19:00</option>
					</select>
				</div>
			</div>

			<div class="field-box">
				<div class="field-header">Мастер переноса</div>
				<div class="field">	
					<span class="fields boolean field-wrap">
						<span class="fields boolean field-item">										
							<select name="PROPERTY[55][]"> 
								<option value="18">alex_vasin_master</option>
								<option value="17">Jela</option>
								<option value="16">Ivan_Jela</option>
								<option value="15">Sergei_DP</option>
								<option value="14">Ilya_Ramenki</option>
								<option value="13">Victor</option>
								<option value="12">Ibragim</option>
								<option value="11">Alexander_Zapad</option>
								<option value="10" selected="">Vitaly_Sobolev</option>
								<option value="9">Brigada_Maxim</option>
								<option value="8">Sergei_Aksenov</option>
								<option value="7">Oleg_Korostelev</option>
								<option value="6">Mihail_Sever</option>
								<option value="5">Brigada_U</option>
								<option value="4">Ruslan_Batirov</option>
								<option value="3">Alexei_Smirnov</option>
								<option value="2">Alexander_Popov</option>
							</select> 
						</span>
					</span>								
				<div class="space" style=""></div>
				</div>
			</div>

			<div class="field-box">
				<div class="field-header">Причина отказа</div>
				<div class="field">
					<input type="text" name="PROPERTY[56][0]" size="30" value=""><br><br>					
				</div>
			</div> -->

			<?if($arResult["LINKED_FIELDS"]):?>
				<?foreach ($arResult["LINKED_FIELDS"] as $field):?>
						<?//pr($field)?>
						<div class="field-box">
							<div class="field-header"><?=$field['NAME']?></div>
							<div class="field">
								<?if($field['PROPERTY_TYPE'] == 'L'):?>
									<select name="PROPERTY[<?=$field['ID']?>]">
										<?if($field["ENUM"]):?>									
											<option value="">(не установлено)</option>
											<?foreach ($field["ENUM"] as $key => $arEnum):?>
												<option value="<?=$key?>"><?=$arEnum["VALUE"]?></option>
											<?endforeach?>									
										<?endif?>
										<?if($field["CODE"]=='TRANSFER_MASTER'):?>												
											<?if($field["MASTERS"]):?>
												<?
													$selected = false;
													if($arResult["ELEMENT_PROPERTIES"][$field['ID']]){
														$selected = $arResult["ELEMENT_PROPERTIES"][$field['ID']][0]['VALUE'];
													}
												?>		
												<?foreach ($field["MASTERS"] as $key => $master):?>
														<option value="<?=$master['ID']?>" <?if($master['ID']==$selected):?> selected <?endif?> ><?=$master["LOGIN"]?></option>
												<?endforeach?>
											<?endif?>
										<?endif?>
									</select>
								<?endif?>

								<?if($field['PROPERTY_TYPE'] == 'S'):?>
									<?//pr($field)?>
									<?if($field["CODE"]=='REJECTION_REASON'):?>
										<?	$value = "";
											if($arResult["ELEMENT_PROPERTIES"][$field['ID']]){
												$value = $arResult["ELEMENT_PROPERTIES"][$field['ID']][0]['VALUE'];
											}
										?>
										<input type="text" name="PROPERTY[<?=$field['ID']?>][0]" size="30" value="<?=$value?>"><br><br>	
									<?endif?>

									<?if($field["CODE"]=='TRANSFER_DATE'):?>
										<?	$value = "";
											if($arResult["ELEMENT_PROPERTIES"][$field['ID']]){
												$value = $arResult["ELEMENT_PROPERTIES"][$field['ID']][0]['VALUE'];
											}
										?>
										<input type="text" name="PROPERTY[<?=$field['ID']?>][0][VALUE]" size="30" value="<?=$value?>"><br><br>	
									<?endif?>
								<?endif?>
							</div>
						</div> 
				<?endforeach?>
			<?endif?>

			<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
				<div>
					<div><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></div>
					<div>
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
					</div>
				</div>
				<div>
					<div><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</div>
					<div><input type="text" name="captcha_word" maxlength="50" value=""></div>
				</div>
			<?endif?>
		</div>
		<?endif?>
		
		<div>
			<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
			<?if ($arParams["LIST_URL"] <> ''):?>
				<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
				<input type="button" name="iblock_cancel" value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
					onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';">
			<?endif?>
		</div> 

		<?if($arResult["KEYS"]):?>
			<?foreach ($arResult["KEYS"] as $key => $item):?>
				<input type="hidden" name="KEYS[<?=$key?>]" value="<?=$item?>">
			<?endforeach;?>	
		<?endif?>

	</div>
</form>