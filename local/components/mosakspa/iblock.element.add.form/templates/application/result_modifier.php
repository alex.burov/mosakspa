<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function getUsers($groupId) {
    $result = \Bitrix\Main\UserGroupTable::getList(array(
        'filter' => array('GROUP_ID'=>$groupId, 'USER.ACTIVE'=>'Y'),
        'select' => array(
            'ID'=>'USER.ID',
            'NAME'=>'USER.NAME',
            'LOGIN'=>'USER.LOGIN',            
            'LAST_NAME'=>'USER.LAST_NAME'            
        ), // выбираем идентификатор п-ля, имя и фамилию
        'order' => array('USER.ID'=>'DESC'), // сортируем по идентификатору пользователя
    ));
    $arUsers = array();
    while ($arItem = $result->fetch()){
        $arUsers[] = $arItem;
    }
    return $arUsers; 
}


function getElements($iblockId) {
    $arSelect = array("ID", "NAME");
    $arFilter = array("IBLOCK_ID"=>$iblockId);
    $arItems = [];
    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>500), $arSelect);
    while($arItem = $res->fetch()){
        $arItems[] = $arItem; 
    }
    return $arItems;
}

function searchKey($ID, $PROPERTY_LIST){
	if(in_array($ID, $PROPERTY_LIST)){
		return $key = array_search($ID,$PROPERTY_LIST);
	}

	return false;
}


// pr($arResult["PROPERTY_LIST"]);

// КЛЮЧИ СВОЙСТВ 
$arResult["KEYS"] = []; 
$arResult["LINKED_FIELDS"] = [];
$transfer_date = '';
foreach ($arResult["PROPERTY_LIST_FULL"] as $key => $value) {


	if(is_string($value['CODE'])){		
		$arResult["KEYS"][$value['CODE']] = $key; // записываем ключи свойств в массив
	}

	if($value['CODE']=='MASTER'){
		// pr($value);
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
			$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; // select multiple
			$arResult["PROPERTY_LIST_FULL"][$key]["MASTERS"] = getUsers($arParams['LINKED_IBLOCKS'][$value['CODE']]); // список мастеров  
		}		
	}

	if($value['CODE']=='CAMPAIGN'){	
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
			$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; 
			$arResult["PROPERTY_LIST_FULL"][$key]["CAMPAIGNS"] = getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); // список кампаний
		}
	}

	if($value['CODE']=='DISTRICTS'){	
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
			$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; 
			$arResult["PROPERTY_LIST_FULL"][$key]["DISTRICTS"] = getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); // список районов
		}
	}


	if($value['CODE']=='METRO'){
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){	
			$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; 
			$arResult["PROPERTY_LIST_FULL"][$key]["METRO"] = getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); // список метро
			// pr($arResult["PROPERTY_LIST_FULL"][$key]["METRO"]);
		}
	}

	if($value['CODE']=='OPERATOR'){
		if($arParams["USER_TYPE"] == 'admin'){
			if($arParams['LINKED_IBLOCKS'][$value['CODE']]){	
				$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; 
				$arResult["PROPERTY_LIST_FULL"][$key]["OPERATORS"] = getUsers($arParams['LINKED_IBLOCKS'][$value['CODE']]); // список районов
			}
			// pr($arParams['LINKED_IBLOCKS'][$value['CODE']]);
		}
	} 

	if($value['CODE']=='DONE'){ // выполнен / не выполнен / перенесен

		foreach ($value['ENUM'] as $keyEnum => $doneValue) {
			if($doneValue['XML_ID']=='yes'){
				$arResult['KEYS']['DONE_YES'] = $keyEnum;
			}
		}

		$arResult["LINKED_FIELDS"][0] = $value;
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}	
	}

	if($value['CODE']=='SERVICE_TYPE'){ // вид работ
		$arResult["LINKED_FIELDS"][1] = $value;
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}

	if($value['CODE']=='TRANSFER_MASTER'){
		if($arParams['USER_TYPE'] !== 'master'){
			$arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; // select multiple
		}

		$arResult["PROPERTY_LIST_FULL"][$key]["MASTERS"] = getUsers($arParams['LINKED_IBLOCKS']['MASTER']); // список мастеров  
		$arResult["LINKED_FIELDS"][2] = $arResult["PROPERTY_LIST_FULL"][$key];
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}

	if($value['CODE']=='DATE'){ // дата переноса
		// $arResult["LINKED_FIELDS"][3] = $value;
		// $key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		// if($key){
		// 	unset($arResult["PROPERTY_LIST"][$key]);
		// }
		// $arResult["LINKED_FIELDS"][3] = $value;
		// $arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE'] =  'L'; // select multiple
		// pr($arResult["PROPERTY_LIST_FULL"][$key]['PROPERTY_TYPE']);
	}

	if($value['CODE']=='TRANSFER_DATE'){ // дата переноса	
		$transfer_date = $arResult["ELEMENT_PROPERTIES"][$key][0]['VALUE'];		
		// pr($arResult["ELEMENT_PROPERTIES"][$key]);
		// pr($transfer_date);
		$arResult["LINKED_FIELDS"][3] = $value;
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}			
	}

	if($value['CODE']=='TRANSFER_TIME'){ // время переноса
		$arResult["LINKED_FIELDS"][4] = $value;
		// pr($value);
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}

	if($value['CODE']=='REJECTION_REASON'){ // причина отказа от переноса
		$arResult["LINKED_FIELDS"][5] = $value;
		// pr($value);
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}


	if($value['CODE']=='CLAIM'){ // причина отказа от переноса
		$arResult["LINKED_FIELDS"][6] = $value;
		// pr($value);
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}


	if($value['CODE']=='ARRIVAL_DATE_SET'){ 
		$value['PROPERTY_TYPE'] =  'S'; 
		$arResult["LINKED_FIELDS"][7] = $value;
		$key = searchKey($value['ID'], $arResult["PROPERTY_LIST"]);
		if($key){
			unset($arResult["PROPERTY_LIST"][$key]);
		}
	}
}
ksort($arResult["LINKED_FIELDS"]);
if(!empty($transfer_date)){
	unset($arResult["PROPERTY_LIST"][$arResult["KEYS"]['DATE']]);
}
// pr($arResult["LINKED_FIELDS"]);
// pr($arResult["KEYS"]);
// pr($arResult["PROPERTY_LIST"]);
?>