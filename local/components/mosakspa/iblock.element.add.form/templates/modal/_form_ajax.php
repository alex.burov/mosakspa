<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;

global $APPLICATION;
global $USER; 
Bitrix\Main\Loader::IncludeModule('iblock');

$request = Context::getCurrent()->getRequest();
$iblockId = intval($request->getPost('iblock_id'));

$name = htmlspecialcharsbx($request->getPost('name'));
$phone = htmlspecialcharsbx($request->getPost('phone'));

$second_phone = htmlspecialcharsbx($request->getPost('second_phone'));
$address = htmlspecialcharsbx($request->getPost('address'));
$date = htmlspecialcharsbx($request->getPost('date'));

$time = intval($request->getPost('time'));
$master = intval($request->getPost('master'));
$operator = intval($request->getPost('operator'));
$metro = intval($request->getPost('metro'));

$campaign = htmlspecialcharsbx($request->getPost('campaign'));
$district = intval($request->getPost('district'));
$sirvice = intval($request->getPost('sirvice'));

$message = htmlspecialcharsbx($request->getPost('message'));

$arResult = array(
    'hasError' => true,
    'msg' => "ERROR"
);

$arResult['iblock_id'] = $iblockId;
$arResult['name'] = $name;
$arResult['phone'] = $phone;
$arResult['address'] = $address;


if (!$request->isPost()
   || !($name && $phone && $address && $date && $time && $master && $metro && $campaign && $district && $sirvice && $iblockId)
   || !Loader::includeModule('iblock')
) {
    if(!($name && $phone && $address && $date && $time && $master && $metro && $campaign && $district && $sirvice && $iblockId)) {
        // ОШИБКА: Заполните все обязательные поля.;
        $arResult['msg'] = "FIELD_ERROR";
    }
    echo json_encode($arResult);
    die();
}

if(empty($operator)){
    $operatorID = $USER->GetID();
}else{
    $operatorID = $operator;
}


$arPROPS = array(
    'NAME' => $name,
    'PHONE' => $phone,
    'TIME' => array('VALUE' => $time), // время прихода мастера    
    'ADDRESS' => $address,
    'MASTER' => array('VALUE' => $master), // мастер
    'METRO' => array('VALUE' => $metro), // метро
    'CAMPAIGN' => array('VALUE' => $campaign), 
    'DISTRICTS' => array('VALUE' => $district), // район
    'SERVICE_TYPE' => array('VALUE' => $sirvice), // вид услуги
    'MESSAGE' => $message,
    'HTTP_REFERER' => $_SERVER['HTTP_REFERER'],    
    'FORM' => array('VALUE' => $arResult ["FORM"]),
    'OPERATOR'=>array('VALUE' => $operatorID)
);

if(!empty($second_phone)){
    $arPROPS['SECOND_PHONE'] = $second_phone;
}

// дата когда мастер придет выполнять заявку
$format = date_create_from_format('Y-m-d', $date);
if($format){
    $arPROPS['DATE'] = new \Bitrix\Main\Type\DateTime($date.' 00:00:00', "Y-m-d H:i:s");
}

if($campaign == 'site'){
    $section = SITE_IBLOCK_SECTION;
}else{
    $section = MAIN_IBLOCK_SECTION;
}

$arFields = array(
    'IBLOCK_ID' => $iblockId,
    'ACTIVE' => 'Y',
    'NAME' => $name,  
    'DATE_ACTIVE_FROM' => new \Bitrix\Main\Type\DateTime(),
    'PROPERTY_VALUES' => $arPROPS, 
    'IBLOCK_SECTION_ID' => $section    
);

$arResult['arFields'] = $arFields;
// echo json_encode($arResult);
// die();

$el = new \CIBlockElement();
$itemId = $el->Add($arFields);


if(!$itemId) {
    echo json_encode($arResult);
    die();
}
$arResult = array(
    'hasError' => false,
    'msg' => "SUCCESS",//  "Спасибо! Ваше обращение отправлено.",
    'ID'=>$itemId,
    'date'=>$date
);
echo json_encode($arResult);
die();
?>