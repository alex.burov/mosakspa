<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function getFieldData($iblockId, $code){
    $property_enums = CIBlockPropertyEnum::GetList(
        array("DEF"=>"DESC", "SORT"=>"ASC"), 
        array("IBLOCK_ID"=>$iblockId, "CODE"=>$code)
    );
    $field_props = [];
    while($arr_enum = $property_enums->GetNext()){
        $field_props[] = $arr_enum;
    }

    return $field_props;
}

function _getUsers($groupId) {
    $result = \Bitrix\Main\UserGroupTable::getList(array(
        'filter' => array('GROUP_ID'=>$groupId, 'USER.ACTIVE'=>'Y'),
        'select' => array(
            'ID'=>'USER.ID',
            'NAME'=>'USER.NAME',
            'LOGIN'=>'USER.LOGIN',            
            'LAST_NAME'=>'USER.LAST_NAME'            
        ), // выбираем идентификатор п-ля, имя и фамилию
        'order' => array('USER.ID'=>'DESC'), // сортируем по идентификатору пользователя
    ));
    $arUsers = array();
    while ($arItem = $result->fetch()){
        $arUsers[] = $arItem;
    }
    return $arUsers;
}


function _getElements($iblockId) {
    $arSelect = array("ID", "NAME", "CODE");
    $arFilter = array("IBLOCK_ID"=>$iblockId);
    $arItems = [];
    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>500), $arSelect);
    while($arItem = $res->fetch()){
        $arItems[] = $arItem; 
    }
    return $arItems;
}


function getSections($iblockId){
	$arSelect = array("ID", 'CODE', "NAME");
	$arFilter = Array('IBLOCK_ID'=>$iblockId);

	$arSections = [];
    $getiblock = CIBlockSection::GetList(
    	array("SORT"=>"ASC"), array("IBLOCK_ID"=>$iblockId));
    while($sectionwhile = $getiblock->GetNext()){
		$arSections[] = $sectionwhile;
	}
    return $arSections;
}

// мастера
$arResult['MASTERS'] = false;

// КЛЮЧИ СВОЙСТВ 
$arResult["KEYS"] = [];
foreach ($arResult["PROPERTY_LIST_FULL"] as $key => $value) {

	if($value['CODE']=='METRO'){
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){	
			// список мето
			$arResult["PROPERTY_LIST_FULL"][$key]["METRO"] = _getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); 
		}
	}

	if($value['CODE']=='DISTRICTS'){	
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
			// список районов
			$arResult["PROPERTY_LIST_FULL"][$key]["DISTRICTS"] = _getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); 
		}
	}

	if($value['CODE']=='MASTER'){	
		if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
			$arResult["PROPERTY_LIST_FULL"][$key]['MASTERS'] = _getUsers($arParams['LINKED_IBLOCKS'][$value['CODE']]);
		}			
	}

    if($value['CODE']=='OPERATOR'){   
        if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
            $arResult["PROPERTY_LIST_FULL"][$key]['OPERATORS'] = _getUsers($arParams['LINKED_IBLOCKS'][$value['CODE']]);
        }        
    }

    if($value['CODE']=='CAMPAIGN'){ 
        if($arParams['LINKED_IBLOCKS'][$value['CODE']]){
            // список кампаний
            $arResult["PROPERTY_LIST_FULL"][$key]["CAMPAIGN"] = _getElements($arParams['LINKED_IBLOCKS'][$value['CODE']]); 
        }        
    }

	if(is_string($value['CODE'])){		
		$arResult["KEYS"][$value['CODE']] = $key; // записываем ключи свойств в массив
	}
}


// разделы
$arResult ["SECTIONS"] = false;
$arResult ["SECTIONS"] = getSections($arParams["IBLOCK_ID"]); // разделы
$arResult ["FORM"] = false;

// форма с которой пришла заявка 
$forms = getFieldData($arParams["IBLOCK_ID"], 'FORM');
foreach ($forms as $key => $form) {
    if($form['XML_ID']=='calendar'){
            $arResult ["FORM"] =  $form['ID'];
    }
}
// pr($arResult["PROPERTY_LIST_FULL"]);
// pr($arResult["PROPERTY_LIST_FULL"]);
?>