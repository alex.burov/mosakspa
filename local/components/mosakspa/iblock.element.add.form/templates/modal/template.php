<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<form method="post" class="form discount__form form-send-mosakspa" action="#" id="modal_application_form">
     <input type="hidden" name="discountwebarch[_csrf_token]" value=""> 
    
    <div class="inputs discount-form__inputs flex">                                   
        <div class="input_half">
           <input type="text" class="input" placeholder="Имя *"name="name" required id="name_modal">         
        </div>

        <div class="input_half">
          <input type="tel" class="input" placeholder="Телефон *" name="phone" required id="phone_modal"> 
        </div>

        <div class="input_half">
          <input type="tel" class="input" placeholder="Дополнительный телефон" name="second_phone"  id="second_phone"> 
        </div>

        <div class="input_half">
          <input type="text" class="input" placeholder="Адрес *" name="address" required id="address_modal">  
        </div>
              
    </div>

    <br/>

    <div class="inputs discount-form__inputs flex"> 
        <div class="input_half">
            <div class="field-header">Дата прихода мастера</div>

          <input type="date" class="input" placeholder="Дата *" name="date" required id="date_modal">
 
        </div>
    </div>

    <br/>


    <textarea rows="5" cols="27" placeholder="Комментарий" class="textarea textarea_full textarea_grey discount-form__textarea" id="message_modal" name="message_modal" ></textarea> 

    <?//if($arResult["SECTIONS"]):?>
    <!--  <div class="inputs discount-form__inputs flex">
              <div class="input_half">
                  <label>Привязка к разделу*</label>
                  <select class="select-css select-form-control" name="section" id="section_modal">
                      <?//foreach ($arResult["SECTIONS"] as $key => $section):?>
                          <option value="<?//=$section['ID']?>" <?//if($section['CODE']=='main'):?>selected<?//endif?>>
                            <?//=$section['NAME']?></option>
                      <?//endforeach?>
                  </select> 
              </div>
          </div> -->
    <?//endif?>

    <div class="inputs discount-form__inputs flex">
          <?foreach ($arResult["KEYS"] as $key => $value):?>
              <?if($key=='TIME'):?>                
                    <div class="input_half">
                        <label>Время когда приходит мастер *</label> 
                        <select class="select-css select-form-control" name="time" id="time_modal">
                              <option value="" selected>выберите время</option>
                              <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["ENUM"]  as $item):?>
                                  <option value="<?=$item['ID']?>"><?=$item["VALUE"]?></option>
                              <?endforeach?>
                        </select> 
                    </div>

               <?elseif($key=='MASTER'):?>
                    <div class="input_half">
                          <label>Мастер *</label> 
                          <select class="select-css select-form-control" name="master" id="master_modal">
                                <option value="" selected>выберите мастера</option>
                                <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["MASTERS"] as $key => $master):?>
                                    <option value="<?=$master['ID']?>"><?=$master["LOGIN"]?></option>
                                <?endforeach?>
                          </select>
                    </div>


                <?elseif($key=='OPERATOR'):?>
                    <?if($arParams['USER_TYPE']=='admin'):?>
                    <div class="input_half">
                          <label>Оператор </label> 
                          <select class="select-css select-form-control" name="master" id="operator_modal">
                                <option value="" selected>выберите оператора</option>
                                <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["OPERATORS"] as $key => $operator):?>
                                    <option value="<?=$operator['ID']?>"><?=$operator["LOGIN"]?></option>
                                <?endforeach?>
                          </select>
                    </div>
                    <?endif?>
               <?elseif($key=='METRO'):?>
              
                   <div class="input_half">
                      <label>Метро *</label> 
                      <select class="select-css select-form-control" name="metro" id="metro_modal">
                            <option value="" selected>выберите метро</option>
                            <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["METRO"]  as $item):?>
                                <option value="<?=$item['ID']?>"><?=$item["NAME"]?></option>
                            <?endforeach?>
                      </select>
                  </div>

              <?elseif($key=='CAMPAIGN'):?>
              
                   <div class="input_half">
                      <label>Кампания (источник заявки) *</label> 
                      <select class="select-css select-form-control" name="campaign" id="campaign_modal">
                            <option value="" selected>выберите кампанию</option>
                            <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["CAMPAIGN"]  as $item):?>
                                <option value="<?=$item['CODE']?>"><?=$item["NAME"]?></option>
                            <?endforeach?>
                      </select>
                  </div>
               
               <?elseif($key=='DISTRICTS'):?>
                
                   <div class="input_half">
                      <label>Район *</label> 
                      <select class="select-css select-form-control" name="districs" id="district_modal">
                            <option value="" selected>выберите район</option>
                            <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["DISTRICTS"]  as $item):?>
                                <option value="<?=$item['ID']?>"><?=$item["NAME"]?></option>
                            <?endforeach?>
                      </select>
                  </div>
              <?elseif($key=='SERVICE_TYPE'):?>
                  <div class="input_half">
                      <label>Тип услуги *</label> 
                      <select class="select-css select-form-control" name="sirvice" id="sirvice_modal">
                            <option value="" selected>выберите тип услуги</option>
                            <?foreach ($arResult["PROPERTY_LIST_FULL"][$value]["ENUM"]  as $item):?>
                                  <option value="<?=$item['ID']?>"><?=$item["VALUE"]?></option>
                            <?endforeach?>
                      </select>
                  </div>
              <?endif?>

          <?endforeach?>
    </div>

     <textarea rows="2" cols="15" placeholder="Фронт работ" class="textarea textarea_full textarea_grey discount-form__textarea" id="workfront_modal" name="message_modal" ></textarea> 

    <div class="form-group mb32">
        <p>* поля, обязательные для заполнения</p>
        <div class="form__message field_error hide_field"><?=GetMessage("FIELD_ERROR")?></div>
    </div>

      <div class="discount-form__btn-wrap">
        <button type="button" class="btn btn_red discount-form__btn" id="modal_form_send">отправить заявку</button>
      </div>
</form>
<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<script type="text/javascript">
    var arParamsModalForm = <?=CUtil::PhpToJSObject($arParams)?>; 
    BX.ready(function(){            
            BX.message({
                // DOCUMENT_DELETE: '<?//=GetMessage("DOCUMENT_DELETE")?>',
            });
     });
</script>
<?//require(realpath(dirname(__FILE__)) . '/modal.php');?>
