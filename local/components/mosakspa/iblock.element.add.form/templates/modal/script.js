$(document).ready(function(){

    // $('#date input').val('21.05.2021');
    // 21.05.2021
   
    var objModalForm = {
        init: function(){
            this.checkForm();    
        },
        checkPhone: function($field){
            const phonePattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/; 
            let  err = 0;
            if ($field.val() == ''){
                err++;
            } 
            else {
                if(!phonePattern.test($field.val())){
                   err++;
                }               
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },
        checkDate: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },
        checkField: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },
        toggelFiledError: function(err){
            if(err == 0){
                $("#modal_application_form .field_error").addClass('hide_field');
              }else{
                $("#modal_application_form .field_error").removeClass('hide_field');
            }
        },


        formAjax: function(fields) {

            let formData = new FormData();
            let iblock_id = arParamsModalForm['IBLOCK_ID'];
                formData.append('iblock_id', iblock_id);

            for (var field in fields) {
                if (fields.hasOwnProperty(field)){
                    formData.append(field, fields[field].val());
                }                
            }                
            let obj = this;

            let formFields = {
                name: "Max", 
                phone: "+7 (222) 222-22-22",
                address: "ул. Пелевина 13",
                campaign: "akspa",
                date: "2021-05-20",
                district: "95",
                master: "12",
                message: "",
                metro: "37",
                name: "Max",
                phone: "+7 (222) 222-22-22",
                sirvice: "6",
                time: "16"
            }          

            // console.log(formFields);
            // // console.log(fields['date'].val());

            // for (var field in formFields) {
            //     formData.append(field, formFields[field]);
            // }

            // отправляем через ajax 
            $.ajax({
                url: arParamsModalForm['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    console.log(data);                  
                    $(document).trigger('filter.application.list', {'date':fields['date'].val()});
                    $('.modal__close').click();   
                    $('#modal_application_form')[0].reset();                             
                },                
                error: function (jqXHR, exception) {
                    console.log(jqXHR);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });
        },
        checkForm: function() {  

            let obj = this; 

            // проверка формы обратной связи перед отправкой
            $(document).on('click', '#modal_form_send', function (event) {                
                event.preventDefault();
                console.log('modal_form_send');

                let nameField = $("#name_modal");
                let addressField = $("#address_modal");  
                let phoneField = $("#phone_modal");                              
                let secondPhoneField = $("#second_phone");

                let dateField = $("#date_modal");
                let timeField = $("#time_modal");
                let masterField = $("#master_modal");
                let operatorField = $("#operator_modal");
                let metroField = $("#metro_modal");
                let campaignField = $("#campaign_modal");
                let districtField = $("#district_modal");
                
                let sirviceField = $("#sirvice_modal");
                let messageField = $("#message_modal");
                let workfrontField = $("#workfront_modal");

                let fields = {
                    name:nameField,
                    phone:phoneField,
                    address:addressField,
                    date:dateField,
                    time:timeField,
                    master:masterField,
                    operator:operatorField,
                    metro:metroField,
                    campaign:campaignField,
                    district:districtField,
                    sirvice:sirviceField,                   
                    message:messageField,
                    workfront:workfrontField,
                    second_phone:secondPhoneField
                }

                let err = 0;    
                    err = err + obj.checkField(nameField); // name
                    err = err + obj.checkPhone(phoneField); // phone
                    err = err + obj.checkField(addressField); // address
                    err = err + obj.checkDate(dateField); // date               
                    err = err + obj.checkField(timeField); // time               
                    // err = err + obj.checkField(masterField); // master               
                    err = err + obj.checkField(metroField); // metro               
                    err = err + obj.checkField(campaignField); // campaign               
                    err = err + obj.checkField(districtField); // district               
                    err = err + obj.checkField(sirviceField); // sirvice   
                    
                console.log(err);

                if(err == 0){               
                    obj.formAjax(fields); 
                }
                else{
                    // console.log(err);
                }
                
                // obj.formAjax(fields);
            });

            // имя
            $(document).on('change', '#name_modal', function (event) {
                let err = obj.checkField($(this));
            });

            // проверка телефона при вводе
            $(document).on('change', '#phone_modal', function (event) {
                let err = obj.checkPhone($(this));
            });

            // адрес
            $(document).on('change', '#address_modal', function (event) {
                let err = obj.checkField($(this));
            });

            // дата
            $(document).on('change', '#date_modal', function (event) {
                let err = obj.checkDate($(this));
            });

            // время
            $(document).on('change', '#time_modal', function (event) {
                let err = obj.checkField($(this));
            });

            // мастер
            // $(document).on('change', '#master_modal', function (event) {
            //     let err = obj.checkField($(this));
            // });

            // метро
            $(document).on('change', '#metro_modal', function (event) {
                let err = obj.checkField($(this));
            });


            // кампания
            $(document).on('change', '#campaign_modal', function (event) {
                let err = obj.checkField($(this));
            });

            // раздел
            $(document).on('change', '#district_modal', function (event) {
                let err = obj.checkField($(this));
            });            

            // тип
            $(document).on('change', '#sirvice_modal', function (event) {
                let err = obj.checkField($(this));
            });
        }       
    }
    objModalForm.init();
});