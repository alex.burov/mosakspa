var truck = $(".truck"),
    rails = $(".rails"),
    css_top_onmove = $(rails[0]).css('top'),
    css_top_onhold = $(rails).height() - $(truck).height(),
    css_left = $(rails[0]).css('left'),
    css_right = $(rails[1]).css('right');

$(window).scroll(function () {
    var pos_top = $(window).scrollTop();

    if (pos_top > css_top_onhold) {
        $(truck).css({
            'position': 'absolute',
            'top': css_top_onhold + 'px'
        });

        $(truck[0]).css('left', css_left);
        $(truck[1]).css('right', css_right);
    } else {
        $(truck).css({
            'position': 'fixed',
            'top': css_top_onmove,
            'left': '',
            'right': ''
        });
    }
});