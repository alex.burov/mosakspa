<?php
use Bitrix\Main;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Event;
use Bitrix\Sale\Order;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;
use Bitrix\Main\Mail\Event as BitrixMail;
use Bitrix\Main\UserGroupTable as UserGroupTable;

use Bitrix\Main\UserTable;
global $USER;

// обработчик событий
$eventManager = EventManager::getInstance();
$eventManager->addEventHandler(
    "iblock", 
    "OnBeforeIBlockElementAdd", 
    "onBeforeIBlockElementAdd"
);

// function onBeforeIBlockElementAdd(&$arFields){
//     // if($arFields['IBLOCK_ID'] == FEEDBACK){ 
//     //     // добавляем email в название элемента инфоблока
//     //     $arFields['NAME'] = $arFields['PROPERTY_VALUES'][47];
//     //     return;
//     // }
// }

// после добавления записи по балансу обновляем баланс организации
$eventManager->addEventHandler(
    "iblock", 
    "OnAfterIBlockElementAdd", 
    "onAfterIBlockElementAdd" 
);  

// сохраняем ID заявки в свойство SEARCH_ID для поиска по ID
function onAfterIBlockElementAdd(&$arFields){ 
	if($arFields['IBLOCK_ID'] == FORMS_IBLOCK){
		CModule::IncludeModule('iblock');
		CIBlockElement::SetPropertyValueCode($arFields['ID'], "SEARCH_ID", $arFields['ID']);

	}

	if($arFields['IBLOCK_ID'] == REVIEWS_IBLOCK){
		CModule::IncludeModule('iblock');
		CIBlockElement::SetPropertyValueCode($arFields['ID'], "SEARCH_ID", $arFields['ID']);
	}
}



$eventManager->addEventHandler(
    "iblock", 
    "OnAfterIBlockElementUpdate", 
    "onAfterIBlockElementUpdate"
);

// function updateName($elementID){
// 	// setLog($elementID);
// 	$res = CIBlockElement::GetByID($elementID);
// 	$name = false;	
// 	if($ar_res = $res->GetNext()){
// 		$dbProperty = CIBlockElement::getProperty(
// 		$ar_res['IBLOCK_ID'], 
// 		$ar_res['ID'], 
// 		array("sort", "asc"),
// 		array());
// 		while ($arProperty = $dbProperty->GetNext()) {
// 			if($arProperty['CODE'] == 'NAME'){
// 				$name = $arProperty['VALUE'];				
// 				break;
// 			}
// 		}
// 	}
// 	// setLog($name);
// 	// if($name){
// 	// 	$code = Cutil::translit(
// 	// 		$name,"ru",
// 	// 		array("replace_space"=>"_", "replace_other"=>"_",'change_case' => 'L', 'max_len' => 100)
// 	// 	);
// 	// 	setLog($name);
// 	// 	$arLoadData = array('CODE'=>$code,'NAME'=>$name);
// 	// 	$el = new CIBlockElement; 
// 	// 	$res = $el->Update($elementID, $arLoadData);
// 	// }
// }

// function onAfterIBlockElementUpdate(&$arFields){
// 	if($arFields['IBLOCK_ID'] == FORMS_IBLOCK){		
// 		// setLog($arFields['NAME']); 
// 		if($_POST['KEYS']['NAME']){
// 			setLog($arFields['PROPERTY_VALUES'][$_POST['KEYS']['TRANSFER_DATE']]['VALUE']);
// 		}
// 	}
// }

$eventManager->addEventHandler(
    "iblock", 
    "OnBeforeIBlockElementUpdate", 
    "onBeforeIBlockElementUpdate"
);

function updateUserDocument($userId){
    $result = UserTable::getList(array(
        // выберем идентификатор и генерируемое (expression) поле SHORT_NAME
        'select' => array('UF_DOCUMENT'), 
        'filter' => array('ID' => $userId)
    ));
    $arUser = $result->fetch();
    if($arUser['UF_DOCUMENT']){
        $num = $arUser['UF_DOCUMENT']-1;
        $user = new CUser;
        $user->Update($userId, array('UF_DOCUMENT'=>$num));

        return true;
    }

    return false;
}

function onBeforeIBlockElementUpdate(&$arFields){
    if($arFields['IBLOCK_ID'] == FORMS_IBLOCK){ 
        
        if($_POST['KEYS']['TRANSFER_DATE']){        	
            $transferDate = $arFields['PROPERTY_VALUES'][$_POST['KEYS']['TRANSFER_DATE']]['VALUE'];
        	if($transferDate){     		
                $date = $arFields['PROPERTY_VALUES'][$_POST['KEYS']['DATE']]['VALUE'];    	        
                if($transferDate !== $date){
    	        	$arFields['PROPERTY_VALUES'][$_POST['KEYS']['DATE']]['VALUE'] = $transferDate;
    	        }
        	}  
        }

        if($_POST['KEYS']['TRANSFER_MASTER']){            
            $transferMaster = intval($_POST['PROPERTY'][$_POST['KEYS']['TRANSFER_MASTER']]);
            if(!empty($transferMaster)){
                $arFields['PROPERTY_VALUES'][$_POST['KEYS']['TRANSFER_MASTER']] = $transferMaster;
                $arFields['PROPERTY_VALUES'][$_POST['KEYS']['MASTER']] = $transferMaster;
            }
        }

        if($_POST['KEYS']['DONE_YES']) {            
            $done = intval($arFields['PROPERTY_VALUES'][$_POST['KEYS']['DONE']]);
            $done_yes = $_POST['KEYS']['DONE_YES'];            
            // pr($done);
            // pr($done_yes);
            if($done == $done_yes){                
                $userId = intval($_POST['user']);
                updateUserDocument($userId);
            }
            // pr($_POST);
            // die();
        }

        if($_POST['KEYS']['TRANSFER_MASTER']){ 

        }
    }
}

