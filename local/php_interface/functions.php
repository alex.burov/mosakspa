<?php
use Bitrix\Main, Bitrix\Sale;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Sale\PaySystem;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity; 


// вывод данных 
function pr($var, $type = false) {
    echo '<pre style="font-size:10px; border:1px solid #000; background:#FFF; text-align:left; color:#000;">';
    if ($type)
        var_dump($var);
    else
        print_r($var);
    echo '</pre>';
}

// пользователь
function getUserData($userId){ 
      $result = \Bitrix\Main\UserTable::getList(array(
            'select' => array(
              'ID',
              'LOGIN', 
              'EMAIL', 
              'NAME',
              'LAST_NAME',
              'SECOND_NAME',
              'PERSONAL_PHONE'
            ), 
            'filter' => array('ID' => $userId)
        ));

      if($arUser = $result->fetch()){            
          $arUser;
      }

      if(isset($arUser)){
        // группы у пользователя
        $arUser['GROUPS']  = \Bitrix\Main\UserTable::getUserGroupIds($arUser['ID']);

        // тип пользователя
        if(in_array(ADMINS, $arUser['GROUPS'])){
             $arUser['TYPE'] = 'admin';             
        }else{
            
            if(in_array(OPERATORS, $arUser['GROUPS'])){
              $arUser['TYPE'] = 'operator';
            }

            if(in_array(MASTERS, $arUser['GROUPS'])){
              $arUser['TYPE'] = 'master';
            }
        }
      }
    return $arUser;
}


// запись в файл логов
function setLog($arFields){
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log_iblock_setlog.txt', serialize($arFields)."\r\n", FILE_APPEND);
}


// function getMasters($groupId) {
//     $result = \Bitrix\Main\UserGroupTable::getList(array(
//         'filter' => array('GROUP_ID'=>$groupId, 'USER.ACTIVE'=>'Y'),
//         'select' => array(
//             'ID'=>'USER.ID',
//             'NAME'=>'USER.NAME',
//             'LOGIN'=>'USER.LOGIN',            
//             'LAST_NAME'=>'USER.LAST_NAME'            
//         ), // выбираем идентификатор п-ля, имя и фамилию
//         'order' => array('USER.ID'=>'DESC'), // сортируем по идентификатору пользователя
//     ));
//     $arUsers = array();
//     while ($arItem = $result->fetch()){
//         $arUsers[] = $arItem;
//     }

//     return $arUsers;
// }


// function getElements($iblockId) {
//     $arSelect = array("ID", "NAME");
//     $arFilter = array("IBLOCK_ID"=>$iblockId);
//     $arItems = [];
//     $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>500), $arSelect);
//     while($arItem = $res->fetch()){
//         $arItems[] = $arItem; 
//     }
//     return $arItems;
// } 

function getArrivalDateSet($iblockId){
  $property_enums = CIBlockPropertyEnum::GetList(
      array("ID"=>"ASC", "SORT"=>"ASC"), 
      array("IBLOCK_ID"=>$iblockId, "CODE"=>"ARRIVAL_DATE_SET")
  );
  $yesID = false;
  $noID = false;
  while ($enum_fields = $property_enums->GetNext()) {
      $arrivalDateSet[] = $enum_fields;
      if($enum_fields['XML_ID']== 'yes'){
         $yesID = $enum_fields['ID'];
      }

      if($enum_fields['XML_ID']== 'no'){
          $noID = $enum_fields['ID'];
      }
  }

  return $result = array('YES' => $yesID, 'NO'=>$noID);
}