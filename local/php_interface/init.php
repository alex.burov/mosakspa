<?php
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Sale\Registry;
use Bitrix\Main\Context;
use Bitrix\Sale\Fuser;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
Bitrix\Main\Loader::IncludeModule('iblock');
Bitrix\Main\Loader::IncludeModule('highloadblock');

define('WEBSERVICES','/webservices/');
define('CLASS_LIB', '/local/php_interface/lib');

// группы пользователей
define('ADMINS', 1);
define('OPERATORS', 6);
define('MASTERS', 7);

// инфоблок заявки
define('FORMS_IBLOCK',13);
define('MAIN_IBLOCK_SECTION', 11);
define('SITE_IBLOCK_SECTION', 12);

define('MAIN_IBLOCK_SECTION_CODE', 'main');
define('SITE_IBLOCK_SECTION_CODE', 'site');

// инфоблоки
define('DISTRICTS_IBLOCK', 14); // инфоблок округа
define('CAMPAIGNS_IBLOCK', 15); // инфоблок кампании
define('METRO_IBLOCK', 16); // инфоблок метро
define('REVIEWS_IBLOCK', 17); // инфоблок метро

// автозагрузка классов при объявлении
Bitrix\Main\Loader::registerAutoLoadClasses(null, [
    'Mobile_Detect' => '/local/php_interface/lib/Mobile_Detect.php'
]);

// устновка значения google recaptcha
// use Bitrix\Main\Config\Configuration;
// $google_recaptcha =array(
//     'value'=> array(
//         'public' => '6LeS8PAZAAAAABVYiuUlnp2C-IIMgTY7WuPGXFRa',  
//         'secret' => '6LeS8PAZAAAAAB9yEkEtNB0bK6Gs-ikzRf97hPeE'
//     ),
//     'readonly' => 1
// );
// $configuration = \Bitrix\Main\Config\Configuration::getInstance();
// $result = $configuration->add('google_recaptcha', $google_recaptcha);
// $configuration->saveConfiguration();


if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/iblock_event_handler.php")){
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/iblock_event_handler.php");
}

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/functions.php")){
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/functions.php");
}
