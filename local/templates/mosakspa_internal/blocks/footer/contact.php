 <div class="footer__contacts-wrap flex">
  <div class="footer__contacts flex">
       <div class="footer-contacts__content footer-contacts__left-side flex" style="/*background: #ce8888;*/">
            <div class="footer__contact flex" style="/*background: #88ce9d;*/">
                  <div class="footer-contact__icon-wrap flex">
                    <svg class="footer-contact__icon">
                       <use xlink:href="#phone__icon"></use>
                    </svg>
                </div>
                <span class="footer-contact__text footer-contact__text_phone">
                    <a href="tel:+74953745721" class="roistat-phone-tel mgo-number">+7 (495) 374-57-21</a>
                </span>
            </div>

            <div class="footer__contact flex" style="/*background: #88ce9d;*/">
              <div class="footer-contact__icon-wrap flex">
                <svg class="footer-contact__icon">
                  <use xlink:href="#address__icon"></use>
                </svg>
              </div>
              <span class="footer-contact__text">Москва ...</span>
            </div>
      </div>           
      <div class="footer-contacts__content footer-contacts__right-side flex" style="/*background: #dbdbe8;*/">
          <div class="footer__contact flex" style="">
            <div class="footer-contact__icon-wrap flex">
                <svg class="footer-contact__icon">
                  <use xlink:href="#whatsapp__icon"></use>
                </svg>
            </div>
            <span class="footer-contact__text" >
            <a href="tel:+7 343 238 55 55">+7 495 555-05-55</a>
            </span>
          </div>
          <div class="footer__contact flex" style="">
            <div class="footer-contact__icon-wrap flex" >
                <svg class="footer-contact__icon">
                  <use xlink:href="#mail__icon"></use>
                </svg>
            </div>
             <span class="footer-contact__text">
            <a href="mailto:poverka@dbu-schetchiki.ru">poverka@</a>
            </span>
          </div>
      </div>
    </div>
</div>