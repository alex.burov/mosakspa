<div class="footer-col footer-col_right footer-col-content flex_col">
    <div class="footer-col-content_right">
        <button class="btn btn_white header-contact__button" data-link="modal-call">
          Оставить заявку
        </button>
                                
        <div class="footer__social-wrap flex" style="">
              <a href="#" class="footer-social-wrap__links">
                 <svg class="footer-social-wrap__icon fb-icon">
                    <use xlink:href="#fb__icon"></use>
                </svg>
              </a>
             <a href="#" class="footer-social-wrap__links">
                <svg class="footer-social-wrap__icon vk-icon">
                    <use xlink:href="#vk__icon"></use>
                </svg>
             </a>
             <a href="#" class="footer-social-wrap__links">
                <svg class="footer-social-wrap__icon ok-icon">
                   <use xlink:href="#ok__icon"></use>
                  </svg>
             </a>
              <a href="#" class="footer-social-wrap__links">
                <svg class="footer-social-wrap__icon insta-icon">
                    <use xlink:href="#insta__icon"></use>
                  </svg>
              </a>
             <a href="#" class="footer-social-wrap__links">
                <svg class="footer-social-wrap__icon youtube-icon">
                    <use xlink:href="#youtube__icon"></use>
                </svg>
              </a>
        </div>
    </div>
</div>