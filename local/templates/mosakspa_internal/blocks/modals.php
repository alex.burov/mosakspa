<!----------- MODAL ----------->
<div class="overlay"></div>
<div id="accident">
            <div class="modal-wrap modal-call-wrap" id="modal-call" style="top: 10px; left: 469px; display: none;">
                          
            <div class="modal modal-call">
                            
                <div class="modal-call__header">
                    <p class="modal-call__header-title">Оставить заявку</p>
                </div>
                            
                <div class="modal-call__content" id="prompt2" data-sender="1">
                    <?
                    $APPLICATION->IncludeComponent(
                      "bitrix:iblock.element.add.form", 
                      "modal", 
                      array(
                        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                        "CUSTOM_TITLE_DETAIL_TEXT" => "",
                        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                        "CUSTOM_TITLE_NAME" => "",
                        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                        "CUSTOM_TITLE_TAGS" => "",
                        "DEFAULT_INPUT_SIZE" => "30",
                        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                        "ELEMENT_ASSOC" => "CREATED_BY",
                        "GROUPS" => array(
                          0 => "2",
                        ),
                        "IBLOCK_ID" => FORMS_IBLOCK,
                        "IBLOCK_TYPE" => "forms",
                        "LEVEL_LAST" => "Y",
                        "LIST_URL" => "",
                        "MAX_FILE_SIZE" => "0",
                        "MAX_LEVELS" => "100000",
                        "MAX_USER_ENTRIES" => "100000",
                        "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                        "PROPERTY_CODES" => array(
                        ),
                        "PROPERTY_CODES_REQUIRED" => array(
                        ),
                        "RESIZE_IMAGES" => "N",
                        "SEF_MODE" => "N",
                        "STATUS" => "ANY",
                        "STATUS_NEW" => "N",
                        "USER_MESSAGE_ADD" => "",
                        "USER_MESSAGE_EDIT" => "",
                        "USE_CAPTCHA" => "N",
                        "COMPONENT_TEMPLATE" => "modal"
                      ),
                      false
                    );
                    ?>
                  <div class="successfull_container_text_accepted" style="display: none;"></div>
                </div>

                <div class="modal__close flex">
                    <svg class="modal__close-icon">
                        <use xlink:href="#close__icon"></use>
                    </svg>
                </div>

            </div>
        </div>


        <div class="modal-wrap modal-call-wrap" id="modal-call-back" style="">
                          
            <div class="modal modal-call">
                            
                <div class="modal-call__header">
                    <p class="modal-call__header-title">Вызвать мастера</p>
                </div>
                            
                <div class="modal-call__content" id="_prompt2" data-sender="1">

                     <?$APPLICATION->IncludeComponent(
                      "bitrix:iblock.element.add.form", 
                      "callback", 
                      array(
                        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                        "CUSTOM_TITLE_DETAIL_TEXT" => "",
                        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                        "CUSTOM_TITLE_NAME" => "",
                        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                        "CUSTOM_TITLE_TAGS" => "",
                        "DEFAULT_INPUT_SIZE" => "30",
                        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                        "ELEMENT_ASSOC" => "CREATED_BY",
                        "GROUPS" => array(
                          0 => "2",
                        ),
                        "IBLOCK_ID" => FORMS_IBLOCK,
                        "IBLOCK_TYPE" => "forms",
                        "LEVEL_LAST" => "Y",
                        "LIST_URL" => "",
                        "MAX_FILE_SIZE" => "0",
                        "MAX_LEVELS" => "100000",
                        "MAX_USER_ENTRIES" => "100000",
                        "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                        "PROPERTY_CODES" => array(
                        ),
                        "PROPERTY_CODES_REQUIRED" => array(
                        ),
                        "RESIZE_IMAGES" => "N",
                        "SEF_MODE" => "N",
                        "STATUS" => "ANY",
                        "STATUS_NEW" => "N",
                        "USER_MESSAGE_ADD" => "",
                        "USER_MESSAGE_EDIT" => "",
                        "USE_CAPTCHA" => "N",
                        "COMPONENT_TEMPLATE" => "callback"
                      ),
                      false
                    );?>
                   
                  <div class="successfull_container_text_accepted" style="display: none;"></div>
                </div>

                <div class="modal__close flex">
                    <svg class="modal__close-icon">
                        <use xlink:href="#close__icon"></use>
                    </svg>
                </div>

            </div>
        </div>
</div>

<div class="modal-wrap modal-call-wrap" id="form-send" style="">       
        <div class="modal modal-call">                            
            <div class="modal-call__content"  data-sender="1" style="text-align: center; padding: 10px;">
                <p>Ваша заявка отправлена, мы перезвоним Вам в ближайшее время!</p>    
            </div>
            <div class="modal__close flex">
                <svg class="modal__close-icon">
                    <use xlink:href="#close__icon"></use>
                </svg>
            </div>
        </div>
</div>
<button data-link="form-send" id="init-form-send" class="btn btn_white header-contact__button" style="display: none;"></button>