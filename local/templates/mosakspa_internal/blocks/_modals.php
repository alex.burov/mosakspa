    <!----------- MODAL ----------->
    <div class="overlay"></div>
    <div id="accident">
            <div class="modal-wrap modal-call-wrap" id="modal-call" style="top: 10px; left: 469px; display: none;">
                          
            <div class="modal modal-call">
                            
                <div class="modal-call__header">
                    <p class="modal-call__header-title">Оставить заявку</p>
                </div>
                            
                <div class="modal-call__content" id="prompt2" data-sender="1">
                    <form method="post" class="form discount__form form-send-mosakspa" action="#">
                                <input type="hidden" name="discountwebarch[_csrf_token]" value=""> 
                                <div class="inputs discount-form__inputs flex">
                                   
                                   <div class="input_half">
                                      <input class="input" placeholder="Ваше имя" type="text" name="discountwebarch[name]" id="discountwebarch_name">         
                                   </div>

                                   <div class="input_half">
                                      <input type="tel" class="input" placeholder="Ваш телефон" name="discountwebarch[phone]" id="discountwebarch_phone">         
                                   </div>

                                    <div class="input_half">
                                        <label>Вид услуги:</label> 

                                        <select name="services" class="select-css select-form-control">
                                            <option value="">Выберите услугу</option>
                                            <option value="Поверка счетчиков воды" selected>Поверка счетчиков</option>
                                            <option value="Замена счетчиков воды">Замена счетчиков</option>
                                            <option value="Установка теплосчетчиков">Установка счетчиков</option>
                                            <option value="Сантехнические работы">Сантехнические работы</option>
                                         </select>
                                    </div>

                                </div>
                                <textarea rows="5" cols="27" placeholder="Комментарий" class="textarea textarea_full textarea_grey discount-form__textarea" name="discountwebarch[text]"></textarea>                              



                                 <div class="discount-form__btn-wrap">
                                   <button type="button" class="btn btn_red discount-form__btn">отправить заявку</button>                        
                                </div>
                    </form>
                  <div class="successfull_container_text_accepted" style="display: none;"></div>
                </div>

                <div class="modal__close flex">
                    <svg class="modal__close-icon">
                        <use xlink:href="#close__icon"></use>
                    </svg>
                </div>

            </div>
        </div>


        <div class="modal-wrap modal-call-wrap" id="modal-call-back" style="">
                          
            <div class="modal modal-call">
                            
                <div class="modal-call__header">
                    <p class="modal-call__header-title">Вызвать мастера</p>
                </div>
                            
                <div class="modal-call__content" id="_prompt2" data-sender="1">
                    <form method="post" class="form discount__form form-send-mosakspa" action="#">
                                <input type="hidden" name="discountwebarch[_csrf_token]" value=""> 
                                <div class="inputs discount-form__inputs flex">
                                   <div class="input_half">
                                      <input type="tel" class="input" placeholder="Ваш телефон" name="">         
                                   </div>

                                </div>


                                 <div class="discount-form__btn-wrap">
                                   <button type="button" class="btn btn_red discount-form__btn">отправить заявку</button>                        
                                </div>
                    </form>
                  <div class="successfull_container_text_accepted" style="display: none;"></div>
                </div>

                <div class="modal__close flex">
                    <svg class="modal__close-icon">
                        <use xlink:href="#close__icon"></use>
                    </svg>
                </div>

            </div>
        </div>
    </div>
