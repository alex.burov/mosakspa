 <nav class="nav header-nav">
                <div class="container">
                   <ul class="nav__list header-nav__list flex">
                     
                      <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">замена</a>
                      </li>

                       <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">установка</a>
                      </li>

                       <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">поверка</a>
                      </li>
                      <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">цены</a>
                      </li>
                      <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">наши работы</a>
                      </li>

                      <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">отзывы и гарантии</a>
                         <span class="list-unfold-wrap">
                            <svg class="list-unfold__icon">
                               <use xlink:href="#arrow__icon"></use>
                            </svg>
                         </span>
                         <div class="nav__sublist-block">
                            <div class="nav__sublist-wrap">
                               <div class="nav-sublist__triangle"></div>
                               <ul class="nav__sublist">
                                  <li class="nav-sublist__item">
                                     <a href="#" class="nav-sublist__link">отзывы</a>
                                  </li>
                                  <li class="nav-sublist__item">
                                     <a href="#" class="nav-sublist__link">лицензии и сертификаты</a>
                                  </li>

                               </ul>
                            </div>
                         </div>
                      </li>
                     
                       <li class="nav__item header-nav__item">
                         <a href="#" class="nav-list__link">контакты</a>
                         <span class="list-unfold-wrap">
                            <svg class="list-unfold__icon">
                               <use xlink:href="#arrow__icon"></use>
                            </svg>
                         </span>
                         <div class="nav__sublist-block">
                            <div class="nav__sublist-wrap">
                               <div class="nav-sublist__triangle"></div>
                               <ul class="nav__sublist">
                                  <li class="nav-sublist__item">
                                     <a href="#" class="nav-sublist__link">о компании</a>
                                  </li>
                                  <li class="nav-sublist__item">
                                     <a href="#" class="nav-sublist__link">новости</a>
                                  </li>
                                   <li class="nav-sublist__item">
                                     <a href="#" class="nav-list__link">контакты</a>
                                  </li>
                               </ul>
                            </div>
                         </div>
                      </li>
                   </ul>
                </div>
            </nav>