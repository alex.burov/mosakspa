 <div class="header-contact__phone-wrap flex">                          
    <div class="knob-numb knob-none">
      <svg class="phone-icon header-contact__phone-icon">
          <use xlink:href="#phone__icon"></use>
       </svg>
       <p class="header-contact__phone-text"><a href="tel:+7 (800)555-50-20" class="roistat-phone-tel mgo-number">+7 (800)555-50-20</a></p>
    </div>

    <div class="header-contact__day-night">
      <button class="btn btn_white header-contact__button" data-link="modal-call">Оставить заявку</button>
    </div>
</div>