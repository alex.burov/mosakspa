<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8">
	<title><?$APPLICATION->ShowTitle(false);?></title>
	<meta name="description" content="о чем страница">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" type="image/png" href="/local/img/favicon/favicon-16x16.png">
    <?
      use Bitrix\Main\Page\Asset;
      use Bitrix\Main\Context;
      use Bitrix\Main\Web\Cookie;
      use Bitrix\Main\Application;
      // выносим стили и скрипты в default
      $template_path = "/local/templates/.default"; 
      Asset::getInstance()->addCss($template_path . "/css/bootstrap.css");
      Asset::getInstance()->addCss($template_path . "/css/style.min.css");
      Asset::getInstance()->addCss($template_path . "/css/styleNew.css");


      // Asset::getInstance()->addJs($template_path . "/js/jQuery.js");
      // Asset::getInstance()->addJs($template_path . "/js/jquery.inputmask.min.js");
      // Asset::getInstance()->addJs($template_path . "/js/jquery.mCustomScrollbar.concat.min.js");
      // Asset::getInstance()->addJs($template_path . "/js/jquery.lettering.min.js");
      // Asset::getInstance()->addJs($template_path . "/js/swiper.min.js");
      // Asset::getInstance()->addJs($template_path . "/js/main.js");
      // Asset::getInstance()->addJs($template_path . "/js/slider.js");

      // $page = $APPLICATION->GetProperty('pageType', 'simple');
      // if($page == 'services'){
      //   $url = getUrl ();
      //   if($url !== '/services/'){
      //     $page = 'simple';
      //   }
      // }    
    ?> 
    <?$APPLICATION->ShowHead();?>
    <style type="text/css">
      .loaded {
        display: none;
      }
      .tdu {
          text-decoration: underline;
      }
    </style>    
	</head>
	<body class="page-body">
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
    <div class="main-wrapper"> 
      <!-- start scroll-up-->
      <div class="scroll-up"><a class="scroll-up__link animate-scroll" href="top">
        <div class="decor">&lt;&lt;</div><?=GetMessage("DEF_TEMPLATE_TOP_BUTTON")?></a>
      </div> 
      