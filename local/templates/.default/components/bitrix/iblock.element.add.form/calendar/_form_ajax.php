<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;

global $APPLICATION; 

Bitrix\Main\Loader::IncludeModule('iblock');

$request = Context::getCurrent()->getRequest();
$iblockId = intval($request->getPost('iblock_id'));
$type = htmlspecialcharsbx($request->getPost('type'));

$name = htmlspecialcharsbx($request->getPost('name'));
$phone = htmlspecialcharsbx($request->getPost('phone'));
$message = htmlspecialcharsbx($request->getPost('message'));

$time = htmlspecialcharsbx($request->getPost('time'));
$date = htmlspecialcharsbx($request->getPost('date'));
$insertdate = htmlspecialcharsbx($request->getPost('insertdate'));


switch ($type) {
    case 1:
        $type_value = 8;
        break;
    case 2:
        $type_value = 9;
        break;
    case 3:
        $type_value = 10;
        break;
    case 4:
        $type_value = 11;
        break;
    default:
        $type_value = 8;
        break;
}

switch ($time) {
    case 1:
        $time_value = 12;
        break;
    case 2:
        $time_value = 12;
        break;
    case 3:
        $time_value = 14;
        break;
    case 4:
        $time_value = 15;
        break;
    case 5:
        $time_value = 16;
        break;
    default:
        $time_value = 12;
        break;
}

$arResult = array(
    'hasError' => true,
    'msg' => "ERROR",
    'type' => $type,
    'phone' => $phone,
    'name' => $name,
    'time' => $time
);

if (!$request->isPost()
   || !($type && $phone && $name && $iblockId)
   || !Loader::includeModule('iblock')
) {

    if(!($type && $phone && $name && $iblockId)) {
        // ОШИБКА: Заполните все обязательные поля.;
        $arResult['msg'] = "FIELD_ERROR";
    }
    echo json_encode($arResult);
    die();
}

$arPROPS = array(
    'PHONE' => $phone,
    'SERVICE_TYPE' => array('VALUE' => $type_value), // вид услуги
    'TIME' => array('VALUE' => $time_value), // время прихода мастера
    'NAME' => $name,
    'HTTP_REFERER' => $_SERVER['HTTP_REFERER'],
    'MESSAGE' => $message,
    'FORM' => array('VALUE' => 17),
    'CAMPAIGN' => 24
);

// дата когда мастер придет выполнять заявку
$format = date_create_from_format('d.m.Y', $date);
if($format){
    $active_from = new \Bitrix\Main\Type\DateTime($format->format($date.' 00:00:00'));
    $arPROPS['DATE'] = $active_from;
}

$arFields = array(
    'IBLOCK_ID' => $iblockId,
    'ACTIVE' => 'Y',
    'NAME' => 'сообщение от посетителя '.$name,
    'PROPERTY_VALUES' => $arPROPS,
    'IBLOCK_SECTION_ID' => SITE_IBLOCK_SECTION
);

$el = new \CIBlockElement();
$itemId = $el->Add($arFields);

// создаем почтовое событие
// отправка почты
// Event::send(array(
//     "EVENT_NAME" => "FEEDBACK_RESPONSE",
//     "LID" => "s1",
//     "C_FIELDS" => array(
//         'EMAIL' => $arPROPS['EMAIL'],
//         'PHONE' => $arPROPS['PHONE'],
//         'NANE' => $arPROPS['NAME']
//     )
// )); 

if(!$itemId) {
    echo json_encode($arResult);
    die();
}
$arResult = array(
    'hasError' => false,
    'msg' => "SUCCESS",//  "Спасибо! Ваше обращение отправлено.",
);
echo json_encode($arResult);
die();
?>