<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$this->addExternalJs($templateFolder.'/datepicker/calendar.js');
?>
<div class="container"  id="section_application_form"> 
    <div class="col-12 col-lg-12 order-lg-1">
        <div class="research__wrapper" style="padding: 1em 0 1em 0;">
            
                <h3 class="title-color"><?=$arParams['TITLE']?></h3>

                
               
                <div class="container">
                    <div class="order-form">                                  
                        <div class="order-form-block">
                            <form class="mosakspa-request-form" id="application_form">

                                <div class="form-group dp-block calendar">
                                    <div class="dp"> 
                                        <input type="hidden" name="date" class="form-control"> 
                                    </div>
                                </div>

                                <div class="error_date hide_field">вы не выбрали дату</div>

                                <div class="form-group text-center date-time-block">
                                    <div class="date-time"></div>
                                </div>

                                <?if(empty($arParams['TIME'])):?>
                                    <div class="form-group time-group">
                                        <label>Время в которое придет мастер*</label> 
                                        <div class="btn-group btn-group-justified" role="group" aria-label="...">        
                                            
                                            <?foreach ($arResult["PROPERTY_LIST_FULL"][$arResult["KEYS"]['TIME']]["ENUM"]  as $item):?>   
                                                <div class="btn-group" role="group"> 
                                                    <button type="button" class="btn btn-default btn-time" data-time="<?=$item['ID']?>"><?=$item["VALUE"]?></button> 
                                                </div>
                                            <?endforeach?>
                                        </div>                      
                                    </div>
                                    <div class="error_time hide_field">вы не выбрали время</div>
                                <?endif?>

                                <?if(empty($arParams['SERVICE_TYPE'])):?>
                                    <div class="form-group mb32">
                                            <label>Вид услуги*</label> 
                                            <select class="form-control" required name="type" id="type">
                                                <option value="">Выберите услугу</option>
                                                <?foreach ($arResult["PROPERTY_LIST_FULL"][$arResult["KEYS"]['SERVICE_TYPE']]["ENUM"]  as $item):?>
                                                <option value="<?=$item['ID']?>"><?=$item["VALUE"]?></option>
                                            <?endforeach?>
                                           </select>
                                    </div>
                                <?else:?>                                    
                                    <?foreach ($arResult["PROPERTY_LIST_FULL"][$arResult["KEYS"]['SERVICE_TYPE']]["ENUM"]  as $item):?>
                                        <?if($arParams['SERVICE_TYPE'] == $item['XML_ID']):?>
                                        <input type="hidden" name="type" class="form-control" value="<?=$item['ID']?>" id="type">
                                        <?endif?>
                                    <?endforeach?>
                                <?endif?>

                                <div class="form-group mb32"> 
                                    <label>Ваше имя *</label> 
                                    <input type="text" name="name" class="form-control" placeholder="Ваше имя" required id="name"> 
                                </div>

                                <div class="form-group mb32"> 
                                    <label>Ваш телефон *</label> 
                                    <input type="tel" name="phone" class="form-control" placeholder="Ваш телефон" required id="phone"> 
                                </div>

                                <div class="form-group mb32">
                                    <label>Комментарий</label>
                                    <textarea class="form-control" name="message" maxlength="650" id="message"></textarea>
                                </div>

                                <div class="form-group mb32">
                                    <p>* поля, обязательные для заполнения</p>
                                    <div class="form__message field_error hide_field"><?=GetMessage("FIELD_ERROR")?></div>
                                </div>

                              
                                <input type="hidden" name="time" class="form-control" value="" id="time">
                                <input type="hidden" name="date" class="form-control" value="" id="date">
                                <input type="hidden" name="insertdate" class="form-control" value="" id="insertdate">
                                 <?
                                     if($arResult["KEYS"]['CAMPAIGN']){
                                        $campaignValue = $arResult["PROPERTY_LIST_FULL"][$arResult["KEYS"]['CAMPAIGN']]['CAMPAIGN']['ID'];
                                     }else{
                                        $campaignValue = 0;
                                     }
                                 ?>
                                <input type="hidden" name="campaign" class="form-control" value="<?=$campaignValue?>" id="campaign">

                                <div class="form-group text-center btn-box">
                                      <button type="submit" class="btn" id="calendar_send">Отправить заявку</button> 
                                      <div class="btn-for-modal">
                                        <button type="reset" class="btn btn-default cancel mt16">Отмена</button> 
                                      </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
                   
            </div>

    </div>
</div>

<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<script type="text/javascript">
    var arParamsCalendarForm = <?=CUtil::PhpToJSObject($arParams)?>;  
    BX.message({
        // DOCUMENT_DELETE: '<?//=GetMessage("DOCUMENT_DELETE")?>',
    });
</script>
<?//require(realpath(dirname(__FILE__)) . '/modal.php');?>

<style> 
          .regionInsideSelect{overflow:auto;position:absolute;width:100%;top:100%;background:#fff;opacity:0;pointer-events:none;transition:0.2s;height:auto;max-height:150px;border:1px solid #848484;} .regionInsideContainer.focus .regionInsideSelect{opacity:1;pointer-events:all;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4);} .regionInsideContainer{position:relative;z-index:10;} .regionInsideSelect div{cursor:pointer;padding:10px;transition:.2s;} .regionInsideSelect div:hover{background:#bebebe;} .regionInsideSelect div.active{background: #fabe8a;border-top: 1px solid #bebebe;border-bottom: 1px solid #bebebe;} .regionInsideSelect div:last-of-type{background:#e1edff;} .regionInsideContainer:before{position:absolute;content:'';width:10px;height:7px;right:4px;top:50%;-webkit-transform:translate(0,-50%);-ms-transform:translate(0,-50%);transform:translate(0,-50%);pointer-events:none;background:url('/template/img/sarrmini.jpg') no-repeat center;} 
</style>