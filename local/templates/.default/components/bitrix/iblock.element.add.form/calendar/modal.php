<div class="modal-wrap modal-call-wrap" id="modal-application-send" style="">       
        <div class="modal modal-call">                            
            <div class="modal-call__content"  data-sender="1" style="text-align: center; padding: 10px;">
                <p>Ваша заявка отправлена, мы перезвоним Вам в ближайшее время!</p>    
            </div>
            <div class="modal__close flex">
                <svg class="modal__close-icon">
                    <use xlink:href="#close__icon"></use>
                </svg>
            </div>
        </div>
</div>
<button data-link="modal-application-send" id="init-application-send" class="btn btn_white header-contact__button" style="display: none;"></button>
