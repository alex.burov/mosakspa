function checkDate(insert, application, currentTime) {
    // console.log('checkDate');
    // console.log(insert);
    // console.log(application);

    let date_insert = new Date(insert);
    let date_application = new Date(application);

    if(date_insert<date_application){
            const buttonList = document.querySelectorAll('.btn-time');
                buttonList.forEach(function(item){
                if(currentTime >= item.dataset.timeActive){
                    item.removeAttribute("disabled", "disabled");
                }
            });
    }
}

$(document).ready(function(){


    // текущее время
    var today = new Date();
    var currentTime = today.getHours();
    var applicationTime = false;
    
    // дата создания заявки
    var insertMonth = today.getMonth()+1;
    var insertDate = today.getDate()+"."+insertMonth+"."+today.getFullYear();
    var insertDateParam = insertMonth+"-"+today.getDate()+"-"+today.getFullYear();

    // деактивация выбора времени если время исеткло
    // const buttonList = document.querySelectorAll('.btn-time');
    //     buttonList.forEach(function(item){
    //     if(currentTime >= item.dataset.timeActive){
    //         item.setAttribute("disabled", "disabled");
    //     }
    // });
    
    // выбор даты в календаре
    var donatFlag=false;
    $('.dp').datepicker({
        language: "ru",
        todayHighlight: true,
        startDate: "today",
        templates: {
            leftArrow: '<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>',
            rightArrow: '<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>'
        },
        todayHighlight: true,
    });
    
    $('.dp').datepicker().on('changeDate', function (e) {
        var month = [
            "Января","Февраля","Марта", 
            "Апреля", "Мая","Июня", 
            "Июля", "Августа", "Сентября", 
            "Октября", "Ноября", "Декабря"
        ];

        $(this).parents('form')
        .find('.date-time')
        .html('Вы выбрали ' + e.date.getDate() + ' ' + month[e.date.getMonth()])
        .css('display', 'inline-block');

        // дата заявки
        var month = e.date.getMonth()+1;
        var applicationDate = e.date.getDate()+"."+month+"."+e.date.getFullYear();
        var applicationDateParam = month+"-"+e.date.getDate()+"-"+e.date.getFullYear();

        if(applicationDate){
            $(this).parents("form").find("input[name=date]").val(applicationDate).trigger('change');
        }

        if(insertDate){
            $(this).parents("form").find("input[name=insertdate]").val(insertDate);
        }
    });  

    // выбор времени
   $(".time-group button").on("click", function () {
        $(this).parents(".time-group").find(".active").removeClass("active");
        $(this).addClass("active");

        applicationTime = $(this).attr('data-time');
        console.log(applicationTime);
        if(applicationTime){
            $(this).parents("form").find("input[name=time]").val(applicationTime).trigger('change');
        }
    });  

    
    var objAppForm = {
        init: function(){
            this.checkForm();    
        },
        checkPhone: function($field){
            const phonePattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/; 
            let  err = 0;
            if ($field.val() == ''){
                err++;
            } 
            else {
                if(!phonePattern.test($field.val())){
                   err++;
                }               
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },
        checkDate: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }

            if(err == 0){
                $("#section_application_form .error_date").addClass('hide_field');
            }else{
                $("#section_application_form .error_date").removeClass('hide_field');
            }
            return err;
        },
        checkType: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },
        checkTime: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }

            if(err == 0){
                $("#section_application_form .error_time").addClass('hide_field');
            }else{
                $("#section_application_form .error_time").removeClass('hide_field');
            }

            return err;
        },
        checkName: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }

            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }

            return err;
        },
        toggelFiledError: function(err){
            if(err == 0){
                $("#section_application_form .field_error").addClass('hide_field');
              }else{
                $("#section_application_form .field_error").removeClass('hide_field');
            }
        },
        formAjax: function(fields) {

            let formData = new FormData();
            
            let phone = fields.phone.val(),
                type = fields.type.val(),
                name = fields.name.val(),
                time = fields.time.val(),
                date = fields.date.val(),
                insertdate = fields.insertdate.val(),
                message = fields.message.val();
                campaign = fields.campaign.val();

            let iblock_id = arParamsCalendarForm['IBLOCK_ID'];
                formData.append('phone', phone);
                formData.append('type', type);
                formData.append('name', name);
                formData.append('iblock_id', iblock_id);
                formData.append('time', time);
                formData.append('date', date);
                formData.append('insertdate', insertdate);
                formData.append('message', message);
                formData.append('campaign', campaign);

            let obj = this;

            // отправляем через ajax 
            $.ajax({
                url: arParamsCalendarForm['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    // console.log(data);
                    $('#init-form-send').click();
                    $('#application_form')[0].reset();
                },                
                error: function (jqXHR, exception) {
                    console.log(jqXHR);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });

        },
        checkForm: function() {    
            let obj = this; 
            // проверка формы обратной связи перед отправкой
            $(document).on('click', '#calendar_send', function (event) {
                event.preventDefault();
                let phoneField = $("#phone");
                let typeField = $("#type");
                let nameField = $("#name");
                let timeField = $("#time");
                let dateField = $("#date");
                let messageField = $("#message");
                let insertdateField = $("#insertdate");
                let campaignField = $("#campaign");

                let fields = {
                    phone:phoneField,
                    type:typeField,
                    name:nameField,
                    time:timeField,
                    date:dateField,
                    insertdate:insertdateField,
                    message:messageField,
                    campaign:campaignField
                }
                
                let err = 0;    
                    err = err + obj.checkPhone(phoneField); // phone
                    err = err + obj.checkType(typeField); // type
                    err = err + obj.checkName(nameField); // name
                    err = err + obj.checkTime(timeField); // name
                    err = err + obj.checkDate(dateField);
                if(err == 0){
                    obj.formAjax(fields);
                    obj.toggelFiledError(err);
                }else{
                    // console.log(err);
                    obj.toggelFiledError(err);
                }
            });

            // проверка полей формы
            // проверка имени при вводе
            $(document).on('change', '#name', function (event) {
                 let err = obj.checkName($(this));
                 obj.toggelFiledError(err);
            });
            $(document).on('change', '#phone', function (event) {
                let err = obj.checkPhone($(this));
                obj.toggelFiledError(err);
            });
            $(document).on('change', '#type', function (event) {
                let err = obj.checkType($(this));
                obj.toggelFiledError(err);
            });
            $(document).on('change', '#time', function (event) {
                let err = obj.checkTime($(this));
                obj.toggelFiledError(err);
            });
            $(document).on('change', '#date', function (event) {
                let err = obj.checkDate($(this));
                obj.toggelFiledError(err);
            });
        }       
    }
    objAppForm.init();

});