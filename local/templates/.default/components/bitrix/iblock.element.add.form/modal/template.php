<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<form method="post" class="form discount__form form-send-mosakspa" action="#" id="modal_application_form">
    <input type="hidden" name="discountwebarch[_csrf_token]" value=""> 
     <div class="inputs discount-form__inputs flex"> 
                                   
        <div class="input_half">
           <input class="input" placeholder="Ваше имя *" type="text" type="text" name="name" required id="name_modal">         
        </div>
        <div class="input_half">
          <input type="tel" class="input" placeholder="Ваш телефон *" name="phone" required id="phone_modal">         
        </div>

        <?if(empty($arParams['SERVICE_TYPE'])):?>
        <div class="input_half">           
          <label>Вид услуги*</label> 
          <select class="select-css select-form-control" name="type" id="type_modal">
              <option value="">Выберите услугу</option>
              <?foreach ($arResult["PROPERTY_LIST_FULL"][$arResult["KEYS"]['SERVICE_TYPE']]["ENUM"]  as $item):?>
                  <option value="<?=$item['ID']?>"><?=$item["VALUE"]?></option>
              <?endforeach?>
          </select>            
        </div> 
        <?endif?>   
    </div>
    <textarea rows="5" cols="27" placeholder="Комментарий" class="textarea textarea_full textarea_grey discount-form__textarea" id="message_modal" name="message_modal" ></textarea>

    <div class="form-group mb32">
        <p>* поля, обязательные для заполнения</p>
        <div class="form__message field_error hide_field"><?=GetMessage("FIELD_ERROR")?></div>
    </div>

      <div class="discount-form__btn-wrap">
        <button type="button" class="btn btn_red discount-form__btn" id="modal_form_send">отправить заявку</button>
      </div>
</form>
<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<script type="text/javascript">
    var arParamsModalForm = <?=CUtil::PhpToJSObject($arParams)?>; 
    BX.ready(function(){            
            BX.message({
                // DOCUMENT_DELETE: '<?//=GetMessage("DOCUMENT_DELETE")?>',
            });
     });
</script>
<?//require(realpath(dirname(__FILE__)) . '/modal.php');?>
