<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function getModalCampaign() {
    $arSelect = array("ID", "NAME", "CODE");
    $arFilter = array("IBLOCK_ID"=>CAMPAIGNS_IBLOCK,"CODE"=>'site');
    $arItems = [];
    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize"=>500), $arSelect);
    while($arItem = $res->fetch()){
        $arItems[] = $arItem; 
    }
    return $arItems[0];
}

// КЛЮЧИ СВОЙСТВ 
$arResult["KEYS"] = [];
foreach ($arResult["PROPERTY_LIST_FULL"] as $key => $value) {
    if(is_string($value['CODE'])){		
		$arResult["KEYS"][$value['CODE']] = $key; // записываем ключи свойств в массив
	}

	if($value['CODE']=='CAMPAIGN'){
	    $arResult["PROPERTY_LIST_FULL"][$key]["CAMPAIGN"] = getModalCampaign();
	}
}
?>