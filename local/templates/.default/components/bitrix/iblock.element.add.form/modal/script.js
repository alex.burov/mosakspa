function checkDate(insert, application, currentTime) {
    // console.log('checkDate');
    // console.log(insert);
    // console.log(application);

    let date_insert = new Date(insert);
    let date_application = new Date(application);

    if(date_insert<date_application){
            const buttonList = document.querySelectorAll('.btn-time');
                buttonList.forEach(function(item){
                if(currentTime >= item.dataset.timeActive){
                    item.removeAttribute("disabled", "disabled");
                }
            });
    }
}

$(document).ready(function(){

   
    var objModalForm = {
        init: function(){
            this.checkForm();    
        },
        checkPhone: function($field){

            const phonePattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/; 
            let  err = 0;
            if ($field.val() == ''){
                err++;
            } 
            else {
                if(!phonePattern.test($field.val())){
                   err++;
                }               
            }

            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }

            return err;
        },
        checkType: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }

            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }

            return err;
        },
        checkName: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }

            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }

            return err;
        },
        toggelFiledError: function(err){
            if(err == 0){
                $("#modal_application_form .field_error").addClass('hide_field');
              }else{
                $("#modal_application_form .field_error").removeClass('hide_field');
            }
        },
        formAjax: function(fields) {

            let formData = new FormData();
            
            let phone = fields.phone.val(),
                type = fields.type.val(),
                name = fields.name.val(),
                message = fields.message.val();

            let iblock_id = arParamsModalForm['IBLOCK_ID'];
                formData.append('phone', phone);
                formData.append('type', type);
                formData.append('name', name);
                formData.append('iblock_id', iblock_id);
                formData.append('message', message);
                
            let obj = this;

            // отправляем через ajax 
            $.ajax({
                url: arParamsModalForm['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    // console.log(data);
                    $('.modal__close').click(); 
                    $('#init-form-send').click();
                                       
                },                
                error: function (jqXHR, exception) {
                    console.log(jqXHR);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });
        },

        checkForm: function() {    
            let obj = this; 
            // проверка формы обратной связи перед отправкой
            $(document).on('click', '#modal_form_send', function (event) {
                event.preventDefault();
                let phoneField = $("#phone_modal");
                let typeField = $("#type_modal");
                let nameField = $("#name_modal");
                let messageField = $("#message_modal");

                let fields = {
                    phone:phoneField,
                    type:typeField,
                    name:nameField,
                    message:messageField
                }
                let err = 0;    
                    err = err + obj.checkPhone(phoneField); // phone
                    err = err + obj.checkType(typeField); // type
                    err = err + obj.checkName(nameField); // name
           
                if(err == 0){
                    obj.formAjax(fields);
                }else{
                    // console.log(err);
                }

            });

            // проверка полей формы
            // проверка имени при вводе
            $(document).on('change', '#name_modal', function (event) {
                 let err = obj.checkName($(this));
            });
            // проверка телефона при вводе
            $(document).on('change', '#phone_modal', function (event) {
                let err = obj.checkPhone($(this));
            });
            // проверка телефона при вводе
            $(document).on('change', '#type_modal', function (event) {
                let err = obj.checkType($(this));
            });
        }       
    }
    objModalForm.init();

});