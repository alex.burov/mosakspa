<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Сохранить";
$MESS["IBLOCK_FORM_APPLY"] = "Применить";
$MESS["IBLOCK_FORM_RESET"] = "Сбросить";
$MESS["IBLOCK_FORM_CANCEL"] = "Отмена";
$MESS["IBLOCK_FORM_BACK"] = "Назад к списку";
$MESS["IBLOCK_FORM_DATE_FORMAT"] = "Формат: ";
$MESS["IBLOCK_FORM_FILE_NAME"] = "Файл";
$MESS["IBLOCK_FORM_FILE_SIZE"] = "Размер";
$MESS["IBLOCK_FORM_FILE_DOWNLOAD"] = "скачать";
$MESS["IBLOCK_FORM_FILE_DELETE"] = "удалить файл";
$MESS["IBLOCK_FORM_CAPTCHA_TITLE"] = "Защита от автоматического заполнения";
$MESS["IBLOCK_FORM_CAPTCHA_PROMPT"] = "Введите слово с картинки";
$MESS["CT_BIEAF_PROPERTY_VALUE_NA"] = "(не установлено)";

$MESS["FEEDBACK_TITLE"] = "Перейдём к делу";
$MESS["EMAIL"] = "Email";
$MESS["PHONE"] = "Телефон";
$MESS["MESSAGE"] = "Сообщение";

$MESS["SEND"] = "Отправить";
$MESS["PRIVACY_POLICY"] = "Нажимая кнопку «Отправить», я подтверждаю свою дееспособность, даю согласие на обработку своих персональных данных в соответствии с";
$MESS["TERMS"] = "Условиями";

$MESS["DOCUMENT_ATTACHED"] = "Прикреплён документ";
$MESS["DOCUMENT_DELETE"] = "Удалить";

// ERRORS
// $MESS["FILE_LIMIT_NUMS_ERROR"] = "Превышен лимит количества файлов. К сообщению можно прикрепить не более 5 файлов.;";
// $MESS["FILE_LIMIT_SIZE_ERROR"] = "Превышен допустимый размер файла. Разрешены файлы не более 5Mb.";
// $MESS["FILE_TYPE_ERROR"] = "Прикрепить можно только файлы изображений (jpg, jpeg, png, gif) и документов (pdf, xls, xlsx, doc, docx)";
$MESS["FILE_ERROR"] = "Форма заполнена некорректно. Файл не правильного формата.";
$MESS["FIELD_ERROR"] = "Форма заполнена некорректно";

// SUCCESS
$MESS["SUCCESS"] = "Форма отправлена, спасибо!";
?>