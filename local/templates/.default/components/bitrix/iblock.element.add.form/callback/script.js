function checkDate(insert, application, currentTime) {
    // console.log('checkDate');
    // console.log(insert);
    // console.log(application);

    let date_insert = new Date(insert);
    let date_application = new Date(application);

    if(date_insert<date_application){
            const buttonList = document.querySelectorAll('.btn-time');
                buttonList.forEach(function(item){
                if(currentTime >= item.dataset.timeActive){
                    item.removeAttribute("disabled", "disabled");
                }
            });
    }
}

$(document).ready(function(){

    
    var objModalForm = {
        init: function(){
            this.checkForm();    
        },
        checkPhone: function($field){

            const phonePattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/; 
            let  err = 0;
            if ($field.val() == ''){
                err++;
            } 
            else {
                if(!phonePattern.test($field.val())){
                   err++;
                }               
            }

            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }

            return err;
        },
        checkType: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            return err;
        },
        checkName: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }
            return err;
        },
        toggelFiledError: function(err){

        },
        formAjax: function(fields) {

            let formData = new FormData();            
            let phone = fields.phone.val();
            let iblock_id = arParamsCallbackForm['IBLOCK_ID'];
            
                formData.append('phone', phone);
                formData.append('iblock_id', iblock_id);
                formData.append('message', message);
                
            let obj = this;

            // отправляем через ajax 
            $.ajax({
                url: arParamsCallbackForm['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    // console.log(data);
                    $('.modal__close').click();
                    $('#init-form-send').click();                   
                    
                },                
                error: function (jqXHR, exception) {
                    console.log(jqXHR);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });
        },

        checkForm: function() {    
            let obj = this; 
            // проверка формы обратной связи перед отправкой
            $(document).on('click', '#callback_form_send', function (event) {
                event.preventDefault();
                let phoneField = $("#phone_callback");
                let fields = {
                    phone:phoneField
                }
                let err = 0;    
                    err = err + obj.checkPhone(phoneField); // phone
                if(err == 0){
                    obj.formAjax(fields);
                }else{
                    console.log(err);
                }

            });

            // проверка полей формы
            // проверка телефона при вводе
            $(document).on('change', '#phone_callback', function (event) {
                let err = obj.checkPhone($(this));
            });
        }       
    }
    objModalForm.init();

});