<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;

global $APPLICATION; 

Bitrix\Main\Loader::IncludeModule('iblock');
$request = Context::getCurrent()->getRequest();
$iblockId = intval($request->getPost('iblock_id'));
$phone = htmlspecialcharsbx($request->getPost('phone'));

$arResult = array(
    'hasError' => true,
    'msg' => "ERROR",
    'phone' => $phone,
);

if (!$request->isPost()
   || !($phone && $iblockId)
   || !Loader::includeModule('iblock')
) {

    if(!($phone && $iblockId)) {
        // ОШИБКА: Заполните все обязательные поля.;
        $arResult['msg'] = "FIELD_ERROR";
    }
    echo json_encode($arResult);
    die();
}

$arPROPS = array(
    'PHONE' => $phone,
    'HTTP_REFERER' => $_SERVER['HTTP_REFERER'],
    'FORM'=>array('VALUE' => 19)
);
$arrSET = getArrivalDateSet($iblockId);
$arPROPS['DATE'] = new \Bitrix\Main\Type\DateTime();
$arPROPS['ARRIVAL_DATE_SET'] = array('VALUE' => $arrSET['NO']);
$arFields = array(
    'IBLOCK_ID' => $iblockId,
    'ACTIVE' => 'Y',
    'NAME' => 'сообщение от посетителя '.$phone,
    'DATE_ACTIVE_FROM' => new \Bitrix\Main\Type\DateTime(),
    'PROPERTY_VALUES' => $arPROPS,
    'IBLOCK_SECTION_ID' => SITE_IBLOCK_SECTION
);

$el = new \CIBlockElement();
$itemId = $el->Add($arFields);

// создаем почтовое событие
// отправка почты
// Event::send(array(
//     "EVENT_NAME" => "FEEDBACK_RESPONSE",
//     "LID" => "s1",
//     "C_FIELDS" => array(
//         'EMAIL' => $arPROPS['EMAIL'],
//         'PHONE' => $arPROPS['PHONE'],
//         'NANE' => $arPROPS['NAME']
//     )
// )); 

if(!$itemId) {
    echo json_encode($arResult);
    die();
}
$arResult = array(
    'hasError' => false,
    'msg' => "SUCCESS",//  "Спасибо! Ваше обращение отправлено.",
);
echo json_encode($arResult);
die();
?>