<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$this->addExternalJs($templateFolder.'/datepicker/calendar.js');
?>
 <form method="post" class="form discount__form form-send-mosakspa" action="#">        
        <input type="hidden" name="discountwebarch[_csrf_token]" value=""> 
        <div class="inputs discount-form__inputs flex">
             <div class="input_half">
                <input type="tel" class="input" placeholder="Ваш телефон" name="phone_callback" id="phone_callback">         
            </div>
        </div>
        <div class="discount-form__btn-wrap">
            <button type="button" class="btn btn_red discount-form__btn" id="callback_form_send">отправить заявку</button>                        
        </div>
</form>
<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<script type="text/javascript">
     var arParamsCallbackForm = <?=CUtil::PhpToJSObject($arParams)?>;
     BX.ready(function(){         
        BX.message({
            // DOCUMENT_DELETE: '<?//=GetMessage("DOCUMENT_DELETE")?>',
        });
    });
</script>
