<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
// $this->addExternalJs($templateFolder.'/datepicker/calendar.js');
?>

<div class="container"  id="section_review"> 

                <!--                     
                    <div class="reviews-button">
                        <button type="button" id="show_review_form">Оставить отзыв</button>
                    </div> -->
               
        
                    <div class="order-form">                                  
                        <div class="order-form-block">
                            <form class="mosakspa-request-form" id="review_form" style="/*display: none*/">

                                <h3 class="title-color">Оставить отзыв</h3>
                                
                                <div class="form-group mb32"> 
                                    <label>Ваше имя *</label> 
                                    <input type="text" name="name" class="form-control" placeholder="Ваше имя" required id="name"> 
                                </div>

                                <div class="form-group mb32"> 
                                    <label>Номер заявки *</label> 
                                    <input type="text" name="number" class="form-control" placeholder="Номер заявки" required id="number"> 
                                </div>

                                <div class="form-group mb32">
                                    <label>Текс отзыва *</label>
                                    <textarea name="message" maxlength="650" id="message" class="form-control"></textarea>
                                </div>


                                <div class="form-group mb32">
                                        <label>Вид услуги *</label> 
                                        <select class="form-control" required name="type" id="type">
                                           <option value="">Выберите услугу</option>
                                           <option value="1">Поверка счетчиков</option>
                                           <option value="2">Замена счетчиков</option>
                                           <option value="3">Установка счетчиков</option>
                                           <option value="4">Сантехнические работы</option>
                                           <option value="5">Другие услуги</option>
                                       </select>
                                </div>
                                
                                
                                <div class="form-group rating-group">
                                        <label>Оцените нашу работу (по 5 бальной шкале) *:</label> 

                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">        
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default -btn-time" data-rating="1">Плохо</button> 
                                        </div>
                                         <div class="btn-group" role="group"> 
                                             <button type="button" class="btn btn-default -btn-time" data-rating="2">Устраивает</button> 
                                        </div>
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default -btn-time" data-rating="3">Средне</button> 
                                        </div>
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default -btn-time" data-rating="4">Хорошо</button> 
                                        </div>
                                                                                
                                        <div class="btn-group" role="group"> 
                                            <button type="button" class="btn btn-default -btn-time" data-rating="5">Отлично</button> 
                                        </div>
                                    </div>                      
    
                                </div>

                                <div class="error_rating hide_field">вы не дали оценку нашей работе</div>

                                <div class="form-group mb32">
                                    <p>* поля, обязательные для заполнения</p>
                                </div>

                                <input type="hidden" name="rating" class="form-control" value="" id="rating">

                                <div class="form-group text-center btn-box">
                                      <button type="submit" class="btn" id="review_send">Отправить</button> 
                                      <div class="btn-for-modal">
                                        <button type="reset" class="btn btn-default cancel mt16">Отмена</button> 
                                      </div>
                                </div>

                            </form>

                        </div>
                    </div>
      
                   
</div>
<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<script type="text/javascript">
    var arParamsReviewsForm = <?=CUtil::PhpToJSObject($arParams)?>;  
    BX.message({
        // DOCUMENT_DELETE: '<?//=GetMessage("DOCUMENT_DELETE")?>',
    });

    $(document).on('click', '#show_review_form', function (event) {
        $("#review_form").show(1000);
        $(this).hide(1000);
    });
</script>
<?require(realpath(dirname(__FILE__)) . '/modal.php');?>
<style> 
          .regionInsideSelect{overflow:auto;position:absolute;width:100%;top:100%;background:#fff;opacity:0;pointer-events:none;transition:0.2s;height:auto;max-height:150px;border:1px solid #848484;} .regionInsideContainer.focus .regionInsideSelect{opacity:1;pointer-events:all;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4);} .regionInsideContainer{position:relative;z-index:10;} .regionInsideSelect div{cursor:pointer;padding:10px;transition:.2s;} .regionInsideSelect div:hover{background:#bebebe;} .regionInsideSelect div.active{background: #fabe8a;border-top: 1px solid #bebebe;border-bottom: 1px solid #bebebe;} .regionInsideSelect div:last-of-type{background:#e1edff;} .regionInsideContainer:before{position:absolute;content:'';width:10px;height:7px;right:4px;top:50%;-webkit-transform:translate(0,-50%);-ms-transform:translate(0,-50%);transform:translate(0,-50%);pointer-events:none;background:url('/template/img/sarrmini.jpg') no-repeat center;} 
</style>