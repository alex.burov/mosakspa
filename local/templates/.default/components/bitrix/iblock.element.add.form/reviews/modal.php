<div class="modal-wrap modal-call-wrap" id="modal-review-send" style="">       
    <div class="modal modal-call">                            
        <div class="modal-call__content" style="text-align: center; padding: 10px;">
            <p>Ваш отзыв принят, он будет опубликован после модерации!</p>    
        </div>
        <div class="modal__close flex">
            <svg class="modal__close-icon">
               <use xlink:href="#close__icon"></use>
            </svg>
        </div>
    </div>
</div>
<button data-link="modal-review-send" id="init-review-send" class="btn btn_white header-contact__button" style="display: none;"></button>