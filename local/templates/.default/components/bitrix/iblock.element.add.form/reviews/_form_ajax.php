<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;

global $APPLICATION; 

Bitrix\Main\Loader::IncludeModule('iblock');

$request = Context::getCurrent()->getRequest();

$iblockId = intval($request->getPost('iblock_id'));
$name = htmlspecialcharsbx($request->getPost('name'));
$number = intval($request->getPost('number'));
$type = intval($request->getPost('type'));
$message = htmlspecialcharsbx($request->getPost('message'));
$rating = intval($request->getPost('rating'));

$arResult = array(
    'hasError' => true,
    'msg' => "ERROR",
    'name' => $name,
    'number' => $number,
    'message' => $message,
    'type' => $type,
    'rating' => $rating
);

// echo json_encode($arResult);
// die();


// if(!$request->isPost() || !($name && $number && $message && $type && $rating && $iblockId) || !Loader::includeModule('iblock')) {
// if(!$request->isPost() || !($name && $number && $message && $type && $rating && $iblockId)) {
//     if(!($name && $number && $message && $type && $rating && $iblockId)) {
//         // ОШИБКА: Заполните все обязательные поля.;
//         $arResult['msg'] = "FIELD_ERROR";
//     }
//     echo json_encode($arResult);
//     die();
// }

// if(!$request->isPost()) {
    
//     echo json_encode($arResult);
//     die();
// }

$arPROPS = array(
    'CLIENT_NAME' => $name,
    'NUMBER' => $number,
    'TYPE' => array('VALUE' => $type), // вид услуги
    'RATING' => array('VALUE' => $rating), // время прихода мастера   
    'MESSAGE' => $message,  
    'HTTP_REFERER' => $_SERVER['HTTP_REFERER']
);

$arFields = array(
    'IBLOCK_ID' => $iblockId,
    'ACTIVE' => 'N',
    'NAME' => $name.'_'.$number,
    'PROPERTY_VALUES' => $arPROPS
);

$el = new \CIBlockElement();
$itemId = $el->Add($arFields);

// создаем почтовое событие
// отправка почты
// Event::send(array(
//     "EVENT_NAME" => "FEEDBACK_RESPONSE",
//     "LID" => "s1",
//     "C_FIELDS" => array(
//         'EMAIL' => $arPROPS['EMAIL'],
//         'PHONE' => $arPROPS['PHONE'],
//         'NANE' => $arPROPS['NAME']
//     )
// )); 

if(!$itemId) {
    echo json_encode($arResult);
    die();
}
$arResult = array(
    'hasError' => false,
    'msg' => "SUCCESS",//  "Спасибо! Ваше обращение отправлено.",
    'type' => $type,
    'rating' => $rating,
    'number' => $number,
    'name' => $name
);
echo json_encode($arResult);
// echo json_encode($arFields);
die();
?>