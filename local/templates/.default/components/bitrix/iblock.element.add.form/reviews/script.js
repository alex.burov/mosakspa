$(document).ready(function(){

    // выбор времени
   $(".rating-group button").on("click", function () {
        $(this).parents(".rating-group").find(".active").removeClass("active");
        $(this).addClass("active");

        let dataRatinge = $(this).attr('data-rating');
        console.log(dataRatinge);
        if(dataRatinge){
            $(this).parents("form").find("input[name=rating]").val(dataRatinge).trigger('change');
        }
    });  

    
    var objAppForm = {
        init: function(){
            this.checkForm();    
        },

        checkName: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },

        checkMessage: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },

        checkNumber: function($field){
            let  err = 0;
            if($field.val() == ''){
                err = 1;
                // добавляем класс для вывода ошибки
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err; 
        }, 

        checkType: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            if(err){
                $field.addClass('error');
            }else{
                $field.removeClass('error');
            }
            return err;
        },

        checkRating: function($field){
            let  err = 0;
            if($field.val() == ''){
                err++;
            }
            if(err == 0){
                $("#section_review .error_rating").addClass('hide_field');
            }else{
                $("#section_review .error_rating").removeClass('hide_field');
            }

            return err;
        },

        toggelFiledError: function(err){
            if(err == 0){
                $("#section_review .field_error").addClass('hide_field');
              }else{
                $("#section_review .field_error").removeClass('hide_field');
            }
        },

        formAjax: function(fields) {

            // console.log('formAjax')

            let formData = new FormData();
            
            let number = fields.number.val(),
                type = fields.type.val(),
                name = fields.name.val(),
                rating = fields.rating.val(),
                message = fields.message.val();

            let iblock_id = arParamsReviewsForm['IBLOCK_ID'];

                formData.append('name', name);
                formData.append('number', number);
                formData.append('type', type);
                formData.append('rating', rating);
                formData.append('message', message);
                formData.append('iblock_id', iblock_id);              

            let obj = this;
            
            // console.log(iblock_id);
            console.log(name);
            console.log(number);
            console.log(message);
            console.log(type);            
            console.log(rating);
            

            // отправляем через ajax 
            $.ajax({
                url: arParamsReviewsForm['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    console.log(data);
                    $('#init-review-send').click();
                },                
                error: function (jqXHR, exception) {
                    console.log(jqXHR.responseText);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });
            // $('#init-review-send').click();
        },

        checkForm: function() {    
            let obj = this; 
            // проверка формы обратной связи перед отправкой
            $(document).on('click', '#review_send', function (event) {
                event.preventDefault();

                // console.log('review_send');

                let nameField = $("#name");
                let numberField = $("#number");
                let typeField = $("#type");               
                let timeField = $("#rating");              
                let messageField = $("#message");
                let ratingField = $("#rating");

                let fields = {
                    number:numberField,
                    type:typeField,
                    name:nameField,
                    rating:ratingField,
                    message:messageField
                }

                let err = 0;    
                    err = err + obj.checkNumber(numberField); 
                    err = err + obj.checkRating(ratingField); // rating
                    err = err + obj.checkType(typeField); // type
                    err = err + obj.checkName(nameField); // name
                    err = err + obj.checkMessage(messageField); // message


                if(err == 0){
                    obj.formAjax(fields);
                }else{
                    console.log(err);
                }
            });

            // проверка полей формы
            // проверка имени при вводе
            $(document).on('change', '#name', function (event) {
                 let err = obj.checkName($(this));
                 obj.toggelFiledError(err);
            });            
            $(document).on('change', '#number', function (event) {
                let err = obj.checkNumber($(this));
                obj.toggelFiledError(err);
            });
            $(document).on('change', '#message', function (event) {
                let err = obj.checkMessage($(this));
            });
            $(document).on('change', '#rating', function (event) {
                let err = obj.checkRating($(this));
                obj.toggelFiledError(err);
            });
            $(document).on('change', '#type', function (event) {
                let err = obj.checkType($(this));
                obj.toggelFiledError(err);
            });
          
        }       
    }
    objAppForm.init();

});