<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(mb_strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
{
	$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
	$obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
	$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
}


function getSection($ElementID){
	//echo $ElementID;
	$res = CIBlockElement::GetByID($ElementID);
	$section = false;
	//pr($res);
	if($arRes = $res->Fetch())
	{
		$res = CIBlockSection::GetByID($arRes["IBLOCK_SECTION_ID"]);
		if($arRes = $res->Fetch())
		{
			$section = $arRes["CODE"];
		}
	}

	return $section;
}

foreach($arResult["SEARCH"] as $key =>$arItem){
	$section = getSection($arItem['ITEM_ID']);
	$arResult["SEARCH"][$key]["URL_WO_PARAMS"] = $arParams['RESULT_PATH'].$section.$arItem["URL_WO_PARAMS"];
}
?>