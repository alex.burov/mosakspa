<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function getUsers($groupId) {
    $result = \Bitrix\Main\UserGroupTable::getList(array(
        'filter' => array('GROUP_ID'=>$groupId, 'USER.ACTIVE'=>'Y'),
        'select' => array(
            'ID'=>'USER.ID',
            'NAME'=>'USER.NAME',
            'LOGIN'=>'USER.LOGIN',            
            'LAST_NAME'=>'USER.LAST_NAME'            
        ), // выбираем идентификатор п-ля, имя и фамилию
        'order' => array('USER.ID'=>'DESC'), // сортируем по идентификатору пользователя
    ));
    $arUsers = array();
    while ($arItem = $result->fetch()){
        $arUsers[] = $arItem;
    }
    return $arUsers;
}


foreach ($arResult["ITEMS"] as $key => $arItem){

	// pr($arItem['NAME']);
	// pr($arItem['NAME']);
	
	foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){
		 // pr($arProperty);

		if($arProperty['CODE']=='NAME'){
			$arResult["ITEMS"][$key]['NAME']  = $arProperty["DISPLAY_VALUE"];
		}
		
		if($arProperty['CODE']=='PHONE'){
			$arResult["ITEMS"][$key]['PHONE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='DATE'){
			$arResult["ITEMS"][$key]['DATE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='MASTER'){
			if(!empty($arProperty["VALUE"])){
				$arResult["ITEMS"][$key]['MASTER']  =  getUserData($arProperty["VALUE"]);
			}
		}

		if($arProperty['CODE']=='OPERATOR'){
			if(!empty($arProperty["VALUE"])){
				$arResult["ITEMS"][$key]['OPERATOR']  =  getUserData($arProperty["VALUE"]);
			}
		}

		if($arProperty['CODE']=='METRO'){
			$arResult["ITEMS"][$key]['METRO']  = $arProperty;			
		}

		if($arProperty['CODE']=='DISTRICTS'){
			$arResult["ITEMS"][$key]['DISTRICTS']  = $arProperty;
		}

		if($arProperty['CODE']=='TIME'){
			$arResult["ITEMS"][$key]['TIME']  = $arProperty;
		}

		if($arProperty['CODE']=='ADDRESS'){
			$arResult["ITEMS"][$key]['ADDRESS']  = $arProperty;
		}

		if($arProperty['CODE']=='REMARK'){
			$arResult["ITEMS"][$key]['REMARK']  = $arProperty;
		}

		// отказ, претензия, выполнено  
		if($arProperty['CODE']=='DONE'){
			$arResult["ITEMS"][$key]['DONE'] = $arProperty['VALUE_XML_ID']; 
		}

		if($arProperty['CODE']=='REJECTION_REASON'){
			$arResult["ITEMS"][$key]['REJECTION_REASON'] = $arProperty['VALUE'];
		}

		if($arProperty['CODE']=='SERVICE_TYPE'){
			$arResult["ITEMS"][$key]['SERVICE_TYPE'] = $arProperty;
		}

		if($arProperty['CODE']=='CLAIM'){
			$arResult["ITEMS"][$key]['CLAIM'] =  $arProperty['VALUE'];
		}
		
		if($arProperty['CODE']=='ARRIVAL_DATE_SET'){
			$arResult["ITEMS"][$key]['ARRIVAL_DATE_SET'] = $arProperty;
		}

	}
}


//  список мастеров
if($arParams['LINKED_IBLOCKS']){
	if(array_key_exists('MASTER', $arParams['LINKED_IBLOCKS'])){
		$arResult['MASTERS'] = getUsers($arParams['LINKED_IBLOCKS']['MASTER']);
	}
}

// if(isset($arResult['MASTER'])){
// 	pr($arResult['MASTER']);
// }

?>