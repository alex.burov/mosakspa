<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;

global $APPLICATION; 

Bitrix\Main\Loader::IncludeModule('iblock');
$request = Context::getCurrent()->getRequest();

$iblockId = intval($request->getPost('iblock_id'));
$recordID = intval($request->getPost('record_id'));
$masterID = intval($request->getPost('master_id'));

$arResult = array(
    'hasError' => true,
    'msg' => "ERROR",
    'record_id' => $recordID,
    'master_id' => $masterID,
);

if (!$request->isPost()
   || !($recordID && $masterID)
   || !Loader::includeModule('iblock')
) {

    if(!($recordID && $masterID)) {
        // ОШИБКА: Заполните все обязательные поля.;
        $arResult['msg'] = "FIELD_ERROR";
    }
    echo json_encode($arResult);
    die();
}

$res = CIBlockElement::SetPropertyValueCode($recordID, 'MASTER', $masterID);
if(!$res) {
    echo json_encode($arResult);
    die();
}
$arResult = array(
    'hasError' => false,
    'msg' => "SUCCESS",
    'ID'=>$recordID
);
echo json_encode($arResult);
die();
?>