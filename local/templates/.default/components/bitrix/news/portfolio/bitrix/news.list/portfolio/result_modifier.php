<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

$arElementGroups = [];
$getiblock = CIBlockSection::GetList(
   Array("SORT"=>"ASC"),
   Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'])
);
 
while($sectionwhile = $getiblock->GetNext()){
	$arS[] = $sectionwhile;
}
 
foreach($arS as $arSec){  
	$activeElements = CIBlockSection::GetSectionElementsCount($arSec['ID'], Array("CNT_ACTIVE"=>"Y"));
	if($activeElements){
		foreach($arResult["ITEMS"] as $key=>$arItem){		
			 if($arItem['IBLOCK_SECTION_ID'] == $arSec['ID']){
				$arSec['ELEMENTS'][] =  $arItem;
			 }
		}	
		$arElementGroups[] = $arSec;
	}
} 
$arResult["ITEMS"] = $arElementGroups;
// pr($arElementGroups);