<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// $this->addExternalCss("/local/templates/".SITE_TEMPLATE_ID."/css/portfolio.css");
// $this->addExternalCss("/local/templates/".SITE_TEMPLATE_ID."/css/owl.css");
// $this->addExternalJS("/local/templates/".SITE_TEMPLATE_ID."/js/owlCarousel.js");
?>


<section class="section services">
    <div class="container">  
    <h2 class="title-color">Наши работы</h2>
    <?if($arResult["ITEMS"]):?> 
    	<!-- desctop версия -->            
        <div class="section-content-wrap serv-title">
			<div class="cards service-cards flex service-cards_hidden">
				<?foreach($arResult["ITEMS"] as $arItem):?>
						<?//pr(CFile::GetPath($arItem["PICTURE"]))?>
						<div class="flipcard h">                                   
				            <div class="card__image-wrap service-card__image-wrap">
				               <img src="<?=CFile::GetPath($arItem["PICTURE"])?>" alt="" class="card__image service-card__image"> 
				            </div>

				            <div class="service-card__content-wrap">
				               <p class="service-card__title card__title stc">
				                  <a href="<?=$arItem["SECTION_PAGE_URL"]?>">замена счетчика</a>
				               </p>
				               <ul class="service-card__subservice-list">
				                  <li class="service-card__subservice">
				                     <a href="<?=$arItem["SECTION_PAGE_URL"]?>">смотреть подробнее</a>
				                  </li>                           
				               </ul>
				            </div>                                    
				        </div>
				<?endforeach;?>
			</div>
		</div>
		<!-- /desctop версия -->

		<!-- mobile версия -->
		<div class="cards service-cards owl-carousel" id="service-cards_slider">  
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<div class="flipcard h">                    
                        <div class="card__image-wrap service-card__image-wrap">
                           <img src="<?=CFile::GetPath($arItem["PICTURE"])?>" alt="" class="card__image service-card__image">
                        </div>
                        <div class="service-card__content-wrap">
                           <p class="service-card__title card__title stc">
                              <a href="<?=$arItem["SECTION_PAGE_URL"]?>">замена счетчика</a>
                           </p>
                           <ul class="service-card__subservice-list">
                              <li class="service-card__subservice">
                                 <a href="<?=$arItem["SECTION_PAGE_URL"]?>">смотреть подробнее</a>
                              </li>                           
                           </ul>
                        </div>                  
                  </div>


			<?endforeach;?>
		</div>
		<!-- /mobile версия -->
	 <?endif;?>
	</div>
</section>
<script type="text/javascript">
    var owl = $('#service-cards_slider');
    owl.owlCarousel({
         items:1,
         center: false,
         loop:true,
         dots:false,
         nav: true,
         autoplay: false,
         autoplayTimeout: 5000,
         autoplayHoverPause: true,
         navText: ['<span aria-label="Previous">‹</span>','<span aria-label="Next">›</span>']
      });
</script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

