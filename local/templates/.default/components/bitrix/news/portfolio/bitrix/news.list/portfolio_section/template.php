<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// $this->addExternalCss("/bitrix/css/main/bootstrap.css");
// $this->addExternalCss("/bitrix/css/main/font-awesome.css");
// $this->addExternalCss($this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');
// $this->addExternalCss("/bitrix/css/main/portfolio.css");

?>

<div class="content-wrapper">
    <div class="container">

    	<h2 class="title-color"><?=$arResult['SECTION']['PATH'][0]['NAME']?> (галерея работ)</h2>
    	<?if($arResult["ITEMS"]):?>
	        <div style="/*display: flex; flex-direction: column;*/ text-align: center;"> 
	        	<?foreach($arResult["ITEMS"] as $arItem):?>

	        	<div class="box">
                    <a class="gallery-item gallery-item_bordered" href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" data-fancybox="gallery" data-caption="<?=$arItem["NAME"]?>">
                    <span class="gallery-item--image" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></span>
                    <span class="gallery-item--caption-pos">
                        <span class="gallery-item--caption">
                            <span class="caption--title"><?=$arItem["NAME"]?></span>
                        </span>
                    </span>
                    </a>
                   <p><?=$arItem["NAME"]?></p>
                </div>
	        	<?endforeach;?>
	        </div>
        <?endif;?>
        <div class="bottom"></div>
    </div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

