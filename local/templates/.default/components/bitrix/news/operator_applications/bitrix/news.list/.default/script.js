$(document).ready(function(){
    // BX.ready(function(){
    function filter(selected_date){ 
        // console.log(selected_date);
        BX.ajax({
                method: 'POST',   
                dataType: 'json',
                url: arParams['AJAX_FILTER_FILE_PATH'],
                data: {
                  'date':selected_date,
                  'arParams':arParams,
                  // 'operator_id':user_id,
                  // 'master_id':master_id,
                  // 'page':page,
                  // 'path_to_detail':path_to_detail,
                  // 'base_link':base_link
                },
                start: true,
                cache: false,
                processData: true,
                scriptsRunFirst: true,
                emulateOnload: true,
                async:true,
                onsuccess: async function(data){
                   const appBlock  = $('.applications');
                   appBlock.empty();
                   appBlock.prepend(data["ITEMS"]);
                    // console.log(data);
                    // console.log(data["PARAMS"]);
                    // console.log(data["PROPERTY_CODE"]);
                },
                onfailure: function(data){
                    console.log(data);
                }
        });  
    }

    function getDate(param){
        
        let date = new Date(param);
        var dd = date.getDate();
        var mm = date.getMonth()+1; 
        var yyyy = date.getFullYear();
        if(dd<10) {
            dd = '0' + dd;
        } 
        if(mm<10) {
            mm = '0' + mm;
        } 

        let dateFormat = dd+'.'+mm+'.'+yyyy;
        return dateFormat;
    }


    // редактирование мастера заявки
    $(document).on('change', '.masters', function (event) {
        
        let master_id = $(this).find(":selected").val();
        let record_id = $(this).attr('data-id');

        if(master_id  && record_id){
            let formData = new FormData();   
                formData.append('iblock_id', arParams['IBLOCK_ID']);
                formData.append('record_id', record_id);
                formData.append('master_id', master_id);

            // console.log(arParams['IBLOCK_ID']);
            // console.log(record_id);
            // console.log(master_id);

            $.ajax({
                url: arParams['AJAX_FILE_PATH'],
                type: "POST",
                dataType : "json", 
                cache: false,
                contentType: false,
                processData: false,         
                data: formData, 
                success: function(data){
                    console.log(data);
                },                
                error: function (jqXHR, exception) { 
                    console.log(jqXHR);
                    // obj.getErrorMessage(jqXHR, exception);
                }
            });
        }     
    });

    // календарь 
    $('#date').datetimepicker({
            locale: 'ru',
            stepping:10,
            format: 'DD.MM.YYYY',
            defaultDate: BX.message('defaultDate'),
    }).on("dp.change", function (e) {        
        let selected_date = $(this).find("input").val(); //
        // console.log(selected_date);
        filter(selected_date);
    });  

    // фильтруем календарь при создании новой записи
    $(document).on('filter.application.list', function (event, filter_param) {
        // console.log('filter.application.list');
        // console.log(filter_param);
        
        let formatted_date = getDate(filter_param.date);   
        // console.log(formatted_date);    
        $('#date input').val(formatted_date); 
        filter(formatted_date);

        // $('#date input').val(filter_param.date); 
        // filter(filter_param.date);
    });

});