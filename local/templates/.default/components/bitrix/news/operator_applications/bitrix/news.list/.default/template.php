<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
session_start();	
if(isset($_SESSION["DATE"])){
    $date = $_SESSION["DATE"];
}else{
	$date = date('d.m.Y'); 
}

$this->setFrameMode(true);
$this->addExternalCss($templateFolder.'/css/datetimepickercss');
$this->addExternalJs($templateFolder.'/moment-with-locales.min.js');
$this->addExternalJs($templateFolder.'/bootstrap-datetimepicker.min.js');?>

<div class="news-list">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?> 

	<p class="history-back"><a href="javascript:history.back()">Вернуться назад</a></p>

	<div class="applications">
		<?if(count($arResult["ITEMS"])>0):?>   
		<h2><?=$arParams["TITLE"]?></h2>

		<div class="mobile">		
			<div class="news-item-header">
				<div class="parameter-id">ID</div>		
				<div class="parameter-phone">Телефон</div>
				<div class="parameter-date">Дата</div>
				<div>Подробно</div>
			</div>
			<?foreach($arResult["ITEMS"] as $arItem):?>		
					<div class="news-item">					
						<div class="parameter parameter-id">
							<span class="parameter-name">ID</span>
							<span class="parameter-value"><?echo $arItem["ID"]?></span>
						</div>
						<div class="parameter parameter-phone">
							<span class="parameter-name">Телефон</span>
							<span class="parameter-value"><?=$arItem["PHONE"]?></span>
						</div>
						<div class="parameter parameter-date">
							<span class="parameter-name">Дата</span>
							<span class="parameter-value"><?=$arItem["DATE"]?></span>
						</div>
						<div class="parameter edit-news-item">
							<span class="parameter-value"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">подробно</a></span>			
						</div>
					</div>
			<?endforeach;?>
		</div>

		<div class="desktop">
			<div class="news-list">
				<table class="application-list">
					<tbody>
						<tr>
							<th>Номер</th>
							<th>ID</th>
							<th>Дата</th>
							<th>Округ</th>
							<th>Метро</th>
							<th>Время</th>
							<th>Адрес</th>
							<th>Тел</th>
						    <th>ФИО</th>	
						    <th>Вид работ</th>
						    <th>Комментарий</th>
						    <th>Мастер и оператор</th>
						    <th></th>					    
						</tr>
						<?foreach($arResult["ITEMS"] as $key => $arItem):?>
							<?	

								$class = '';

								if($arItem['DONE'] == 'no'){
									if(!empty($arItem['REJECTION_REASON'])){
										$class = 'refusal';
									}	
								}

								if($arItem['DONE'] == 'claim'){
									if(!empty($arItem['CLAIM'])){
										$class = 'claim';
									}	
								}

								if($arItem['DONE'] == 'yes'){
									if(!empty($arItem['SERVICE_TYPE']['VALUE_XML_ID'])){
										$class = 'done';
									}	
								}
								
							?>		
						    <tr class="<?=$class?>">
						    	<td><?=$key+1?></td>
						    	<td><span class="parameter-value"><?echo $arItem["ID"]?></span></td>
						        <td>
						        	<span class="parameter-value"><?=$arItem["DATE"]?></span>
						        	<?if($arItem['ARRIVAL_DATE_SET']['VALUE_XML_ID'] == 'no'):?>
						        		<span class="check-date">проверьте дату</span>
						        	<?endif?>
						        </td>
						        <td>
						        	<span class="parameter-value">
						        		<?=$arItem["DISTRICTS"]["DISPLAY_VALUE"]?>				
						        	</span>
						        </td>
						        <td>
						        	<span class="parameter-value">
						        		<?=$arItem["METRO"]["DISPLAY_VALUE"]?>
									</span>
						        </td>
						        <td>
						        	<span class="parameter-value">
						        		<?=$arItem["TIME"]["DISPLAY_VALUE"]?>
						        	</span>
						        </td>
						        <td>
						        	<span class="parameter-value"><?=$arItem["ADDRESS"]["DISPLAY_VALUE"]?></span>
						        </td>

						        <td><span class="parameter-value"><?=$arItem["PHONE"]?></span></td>

						        <td><span class="parameter-value"><?=$arItem["NAME"]?></span></td>	
						        <td><span class="parameter-value"><?=$arItem["SERVICE_TYPE"]["VALUE"]?></span></td>
						        <td><span class="parameter-value"><?=$arItem["REMARK"]["~VALUE"]["TEXT"]?></span></td> 
						        <td>
						        	мастер:						        	
									<select name="MASTERS" class="masters" data-id="<?=$arItem["ID"]?>">


										<option value="">выберите мастера</option>

										<?foreach ($arResult["MASTERS"] as $key => $master):?>
											<?if($master['ID'] == $arItem["MASTER"]['ID']):?>
												<option value="<?=$master['ID']?>" selected><?=$master["LOGIN"]?></option>
											<?else:?>
												<option value="<?=$master['ID']?>"><?=$master["LOGIN"]?></option>
											<?endif?>
										<?endforeach?>
									</select> 
									<?if($arItem["OPERATOR"]):?>
										<p class="operator">оператор: <?=$arItem["OPERATOR"]['LOGIN']?></p>
									<?endif;?>
						        </td>	

						        <td><span class="parameter-value"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">подробно</a></span></td>
						    </tr>
					    <?endforeach;?>
					   
					</tbody>
				</table>
	 
			</div>
		</div>
		

			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<br /><?=$arResult["NAV_STRING"]?>
			<?endif;?>
			<?else:?>
				 <h2>Нет записей</h2>
			<?endif;?>
		</div>	
	
</div>
<?$arParams['AJAX_FILE_PATH'] = $templateFolder.'/form_ajax.php';?>
<?$arParams['AJAX_FILTER_FILE_PATH'] = $templateFolder.'/filter_ajax.php';?>
<?//pr($arParams['LIST_PROPERTY_CODE']);?>
<script type="text/javascript">
    var arParams = <?=CUtil::PhpToJSObject($arParams)?>;  
    BX.message({
        defaultDate:moment('<?=$date;?>','DD.MM.YYYY')
    });
</script>
