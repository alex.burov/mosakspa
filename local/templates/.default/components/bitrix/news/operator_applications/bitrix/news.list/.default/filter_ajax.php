<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Context;
use Bitrix\Main\Data;
global $APPLICATION; 

Bitrix\Main\Loader::IncludeModule('iblock');
$request = Context::getCurrent()->getRequest();
$data = [];
$data['DATE'] =  htmlspecialcharsEx($request->getPost('date'));
$arParams = htmlspecialcharsEx($request->getPost('arParams'));
global $arrFilter;
$date = DateTime::createFromFormat('d.m.Y', $data['DATE']);
// $arrFilter["PROPERTY_DATE"] = date($date->format('Y-m-d'), time()); 
// $arrFilter["SECTION_ID"] = $arParams["SECTION_ID"];

if(!empty($arParams["USER_ID"])){
    $user_id = (int)$arParams["USER_ID"];       
    $arManager = getUserData($user_id);
    $title = "Заявки оператора ".$arManager['NAME']." ".$arManager["LAST_NAME"];

    $arrFilter = array(array(
        "LOGIC" => "AND", 
        "SECTION_ID" => $arParams["SECTION_ID"],
        "PROPERTY_DATE" => date($date->format('Y-m-d'), time())),
        "PROPERTY_OPERATOR" => $user_id
    );
}else{
    $user_id = false;
    $title = $arParams["TITLE"];    

    $arrFilter = array(array(
        "LOGIC" => "AND", 
        "SECTION_ID" => $arParams["SECTION_ID"],
        "PROPERTY_DATE" => date($date->format('Y-m-d'), time()))
    );
}?>

<?ob_start(); // тут будет вызов компонета?> 
<?
$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "operator_applications_ajax", 
    array(
        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "NEWS_COUNT" => $arParams["NEWS_COUNT"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_DESC_NUMBERING" =>  $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
        "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SHOW_404" => $arParams["SHOW_404"],
        "SORT_BY1" => $arParams["SORT_BY1"],
        "SORT_BY2" => $arParams["SORT_BY2"],
        "SORT_ORDER1" => $arParams["SORT_ORDER1"],
        "SORT_ORDER2" => $arParams["SORT_ORDER2"],
        "STRICT_SECTION_CHECK" => "N",
        "COMPONENT_TEMPLATE" => ".default",
        "USE_FILTER" => "Y", // !!!
        "FILTER_NAME" => "arrFilter",
        "LINKED_IBLOCKS"=>$arParams['LINKED_IBLOCKS'],
        "TITLE" => $arParams["TITLE"]
    ), 
    false
);?>
<?
// $data['PARAMS']  = $arParams;
$data['USER_ID'] = $user_id;
$data['PROPERTY_CODE']  = $arParams['PROPERTY_CODE'];
$data['ITEMS'] = ob_get_clean();
echo json_encode($data);
die();
?>