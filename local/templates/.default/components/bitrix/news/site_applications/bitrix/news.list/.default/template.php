<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?> 

	<p class="history-back"><a href="javascript:history.back()">Вернуться назад</a></p>

	<?if($arParams["APP_FORM_PATH"]):?>
	<p class="make-application"><a href="<?=$arParams["APP_FORM_PATH"]?>">Создать заявку для мастера</a></p>
	<?endif;?> 
	
	<?if(count($arResult["ITEMS"])>0):?>   
		<h2><?=$arParams["TITLE"]?></h2>

	<div class="mobile">		
		<div class="news-item-header">
			<div class="parameter-id">ID</div>		
			<div class="parameter-phone">Телефон</div>
			<div class="parameter-date">Дата</div>
			<div>Подробно</div>
		</div>
		<?foreach($arResult["ITEMS"] as $arItem):?>		

				<div class="news-item">					
					<div class="parameter parameter-id">
						<span class="parameter-name">ID</span>
						<span class="parameter-value"><?echo $arItem["ID"]?></span>
					</div>


					<div class="parameter parameter-phone">
						<span class="parameter-name">Телефон</span>
						<span class="parameter-value"><?=$arItem["PHONE"]?></span>
					</div>


					<div class="parameter parameter-date">
						<span class="parameter-name">Дата</span>
						<span class="parameter-value"><?=$arItem["DATE"]?></span>
					</div>

					<div class="parameter edit-news-item">
						<span class="parameter-value"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">подробно</a></span>				
					</div>
				</div>
		<?endforeach;?>

	</div>


	<div class="desktop">
		<div class="news-item-header">
				<div class="parameter-id">ID</div>		
				<div class="parameter-phone">Телефон</div>
				<?if(in_array('MASTER',$arParams["PROPERTY_CODE"])):?>   
					<div class="parameter-master">Мастер</div>
				<?endif;?>
				<?if(in_array('OPERATOR',$arParams["PROPERTY_CODE"])):?>   
					<div class="parameter-operator">Оператор</div>
				<?endif;?>
				<div class="parameter-service-type">Тип услуги</div>
				<div class="parameter-date">Дата</div>
				<div>Подробно</div>
		</div>

		<?foreach($arResult["ITEMS"] as $arItem):?>		
					<div class="news-item">				
						<div class="parameter parameter-id">
							<span class="parameter-value"><?echo $arItem["ID"]?></span>
						</div>
						<div class="parameter parameter-phone">
							<span class="parameter-value"><?=$arItem["PHONE"]?></span>
						</div>
						<?if(in_array('MASTER',$arParams["PROPERTY_CODE"])):?>   
							<div class="parameter parameter-master">
								<span class="parameter-value">логин: <?=$arItem["MASTER"]['LOGIN']?></span>
								<span class="parameter-value">ФИО: <?=$arItem["MASTER"]['NAME']?>&nbsp;<?=$arItem["MASTER"]['LAST_NAME']?></span>
								<?if(!isset($_REQUEST['USER_ID'])):?>  
									<span class="parameter-value"><a href="<?=$arParams["SEF_FOLDER"].'/?USER_ID='.$arItem["MASTER"]['ID']?>">заявки мастера</a></span>
								<?endif;?>
							</div>
						<?endif;?>
						<?if(in_array('OPERATOR',$arParams["PROPERTY_CODE"])):?>   
							<div class="parameter parameter-operator">
									<span class="parameter-value">логин: <?=$arItem["OPERATOR"]['LOGIN']?></span>
								<span class="parameter-value">ФИО: <?=$arItem["OPERATOR"]['NAME']?>&nbsp;<?=$arItem["OPERATOR"]['LAST_NAME']?></span>
								<?if(!isset($_REQUEST['USER_ID'])):?>  
									<span class="parameter-value"><a href="<?=$arParams["SEF_FOLDER"].'/?USER_ID='.$arItem["OPERATOR"]['ID']?>">заявки мастера</a></span>
								<?endif;?>
							</div>
						<?endif;?>
						<div class="parameter parameter-service-type">
							<span class="parameter-value"><?=$arItem["SERVICE_TYPE"]?></span>
						</div>
						<div class="parameter parameter-date">
							<span class="parameter-value"><?=$arItem["DATE"]?></span>
						</div>
						<div class="parameter edit-news-item">
							<span class="parameter-value"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">подробно</a></span>				
						</div>				
					</div>					
		<?endforeach;?>
	</div>
		

		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br /><?=$arResult["NAV_STRING"]?>
		<?endif;?>
	<?else:?>

		    <h2>Нет записей</h2>

	<?endif;?>
</div>

