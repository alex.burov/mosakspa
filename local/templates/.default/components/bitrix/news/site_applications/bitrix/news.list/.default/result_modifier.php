<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach ($arResult["ITEMS"] as $key => $arItem){
	
	foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){
		if($arProperty['CODE']=='PHONE'){
			$arResult["ITEMS"][$key]['PHONE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='SERVICE_TYPE'){
			$arResult["ITEMS"][$key]['SERVICE_TYPE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='DATE'){
			$arResult["ITEMS"][$key]['DATE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='MASTER'){
			if(!empty($arProperty["VALUE"])){
				$arResult["ITEMS"][$key]['MASTER']  =  getUserData($arProperty["VALUE"]);
			}
		}

		if($arProperty['CODE']=='OPERATOR'){
			if(!empty($arProperty["VALUE"])){
				$arResult["ITEMS"][$key]['OPERATOR']  =  getUserData($arProperty["VALUE"]);
			}
		}

	}

}
?>