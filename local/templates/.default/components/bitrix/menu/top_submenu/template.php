<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 offset-md-4">
                <ul class="bottom-menu">
                <?foreach($arResult as $arItem):?>
                  <li>
                    <button class="blob-btn">
                      <a href="<?=$arItem["LINK"]?>" target="blank"><?=$arItem["TEXT"]?></a>
                      <span class="blob-btn__inner">
                        <span class="blob-btn__blobs">
                          <span class="blob-btn__blob"></span>
                          <span class="blob-btn__blob"></span>
                          <span class="blob-btn__blob"></span>
                          <span class="blob-btn__blob"></span>
                        </span>
                      </span>
                    </button>
                  </li>                
            <?endforeach?>  
          </ul>

            <!-- это служебный компонент для стиля кнопки -->
            <svg style='display: none;'>
            <defs>
              <filter id="goo">
                <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"></feGaussianBlur>
                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 21 -7" result="goo"></feColorMatrix>
                <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
              </filter>
            </defs>
          </svg>  
        </div>
        </div>
    </div>  
<?endif?> 


 