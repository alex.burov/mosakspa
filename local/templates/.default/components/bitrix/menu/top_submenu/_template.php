<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
    <div class="main-nav">
        <button class="main-nav__toggle"><span></span></button>
        
        <ul class="main-nav__list">                    
            <li class="main-nav__item"><div class="main-tel">8(800)555-50-20<br/><span>c 8:00 до 21:00</span></div></li>
            <?foreach($arResult as $arItem):
                    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
                        continue;?>
                    <?if($arItem["SELECTED"]):?>
                        <li class="main-nav__item"><button class="blob-btn">
                                <?=$arItem["TEXT"]?>
                                <span class="blob-btn__inner">
                                  <span class="blob-btn__blobs">
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                  </span>
                                </span>
                        </button></li>
                    <?else:?>
                        <li class="main-nav__item"><a href="<?=$arItem["LINK"]?>" target="blank"><button class="blob-btn"><?=$arItem["TEXT"]?>
                                <span class="blob-btn__inner">
                                  <span class="blob-btn__blobs">
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                    <span class="blob-btn__blob"></span>
                                  </span>
                                </span>
                        </button></a></li>
                    <?endif?>
            <?endforeach?>
            <li class="main-nav__item">
                <a href="#form_">
                    <button class="main-btn btn">Оставить заявку
                    <div class="decor">>></div>
                </button></a>
            </li> 
        </ul>
        <svg style='display: none;'>
            <defs>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10"></feGaussianBlur>
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 21 -7" result="goo"></feColorMatrix>
                    <feBlend in2="goo" in="SourceGraphic" result="mix"></feBlend>
                </filter>
            </defs>
        </svg>  
    </div> 
<?endif?> 


 