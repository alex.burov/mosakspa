<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// текущая страница
use \Bitrix\Main\Application;
// $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
// $uri = new \Bitrix\Main\Web\Uri($request->getRequestUri());
// $cur_page = $uri->getPath(false);
// $requestUri = $request->getRequestUri(); // текущий адрес страницы

if($USER->IsAuthorized()){
	$arUser = getUserData($USER->GetID());

	foreach($arResult as $i => $arMenu){

		if($arUser['TYPE'] == 'master'){
				switch ($arMenu['LINK']) {

					case '/manager/operators/':
						unset($arResult[$i]);
					break;					

					case '/manager/applications/site/':
						unset($arResult[$i]);
					break;					

					case '/manager/applications/main/':
						unset($arResult[$i]);
					break;					

					case '/manager/masters/':
						unset($arResult[$i]);
					break;		

					case '/manager/applications/history/':
						unset($arResult[$i]);
					break;	

					case '/manager/search/':
						unset($arResult[$i]);
					break;

					case '/manager/applications/':
						unset($arResult[$i]);
					break;		

					default:
						# code...
						break;
				}
		}

	}

}else{
	foreach($arResult as $i => $arMenu){
		if($arMenu['LINK'] !== '/'){
			unset($arResult[$i]);
		}
	}
}




