<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<?$dir = $APPLICATION->GetCurDir();
$show = false;
if($dir == '/manager/applications/main/' || $dir == '/manager/applications/site/' ||$dir == '/manager/applications/masters/'){
	$show = true;
}?>
<nav class="nav header-nav">
    <div class="container">
        <ul class="nav__list header-nav__list flex">	

        	<li class="nav__item header-nav__item ">
				<div class="form-group">
					<div class="input-group date" id="date" <?if (!$show):?>style="display: none;"<?endif?>>	          
						<input type="text" class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon-calendar glyphicon"></span>
						</span>
					</div>
				</div>
			</li>


			<li class="nav__item header-nav__item ">
				<button class="make-application header-contact__button" data-link="modal-call">Создать заявку</button>
			</li>

			<?
			$previousLevel = 0;
			foreach($arResult as $arItem):?> 
				<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
			<?endif?>

			<?if ($arItem["IS_PARENT"]):?>

				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>root-item-selected<?endif?>"><a href="<?=$arItem["LINK"]?>" class="nav-list__link "><?=$arItem["TEXT"]?></a>
						
						<div class="nav__sublist-block">
                          	<div class="nav__sublist-wrap">
                               <div class="nav-sublist__triangle"></div>
								<ul class="nav__sublist">
				<?else:?>
					<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>item-selected<?endif?>">
						<a href="<?=$arItem["LINK"]?>" class="nav-list__link parent"><?=$arItem["TEXT"]?></a>						
                        <ul> 
				<?endif?>
			<?else:?>

				<?if ($arItem["PERMISSION"] > "D"):?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>item-selected<?endif?>">
							<a href="<?=$arItem["LINK"]?>" class="nav-list__link"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>item-selected<?endif?>">
							<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?else:?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>item-selected<?endif?>">
							<a href="" class="nav-list__link" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?else:?>
						<li class="nav__item header-nav__item <?if ($arItem["SELECTED"]):?>item-selected<?endif?>">
							<a href="" class="nav-list__link denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?endif?>

			<?endif?>
			<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

			<?endforeach?>
			<?if ($previousLevel > 1)://close last item tags?>
				<?=str_repeat("</ul></div></div></li>", ($previousLevel-1) );?>
			<?endif?>

        </ul>
    </div>

</nav>
<?endif?>