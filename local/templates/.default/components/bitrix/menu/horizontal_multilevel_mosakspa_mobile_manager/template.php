<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="nav-header-mob" style=""> 
<a href="#"><i class="fa fa-bars"></i></a>
<ul style="display: none;">
        <div class="close"><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>

        <?
		$previousLevel = 0;
		foreach($arResult as $arItem):?>


			<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
			<?endif?>

			<?if ($arItem["IS_PARENT"]):?>
				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li <?if ($arItem["SELECTED"]):?>class="root-item-selected"<?endif?>>
						<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?><i class="fa fa-angle-right" aria-hidden="true"></i></a>
						<ul style="display: none;" id="lt">
							<div class="close cl"><a href="#"><i class="fa fa-chevron-left"></i></a> <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>
				<?else:?> 
					<li >
						<a href="<?=$arItem["LINK"]?>" class="nav-list__link parent"><?=$arItem["TEXT"]?></a>						
                        <ul> 
				<?endif?>
			<?else:?>
				<?if ($arItem["PERMISSION"] > "D"):?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li <?if ($arItem["SELECTED"]):?>class="item-selected"<?endif?>>
							<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li <?if ($arItem["SELECTED"]):?>class="item-selected"<?endif?>>
							<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?else:?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li <?if ($arItem["SELECTED"]):?>class="item-selected"<?endif?>>
							<a href="" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?else:?>
						<li <?if ($arItem["SELECTED"]):?>class="item-selected"<?endif?>>
							<a href="" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?endif?>
			<?endif?>
			<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
		<?endforeach?>

		<?if ($previousLevel > 1)://close last item tags?>
			<?=str_repeat("</ul></div></div></li>", ($previousLevel-1) );?>
		<?endif?>
 </ul>
</nav>
<?endif?>