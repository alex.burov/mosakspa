<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="nav header-nav">
    <div class="container">
        <ul class="nav__list header-nav__list flex">	
			<?
			$previousLevel = 0;
			foreach($arResult as $arItem):?>

				<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
			<?endif?>

			<?if ($arItem["IS_PARENT"]):?>

				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li class="nav__item header-nav__item"><a href="<?=$arItem["LINK"]?>" class="nav-list__link <?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
						<span class="list-unfold-wrap">
                            <svg class="list-unfold__icon">
                               <use xlink:href="#arrow__icon"></use>
                            </svg>
                         </span>
						<div class="nav__sublist-block">
                          	<div class="nav__sublist-wrap">
                               <div class="nav-sublist__triangle"></div>
								<ul class="nav__sublist">
				<?else:?>
					<li class="nav__item header-nav__item item-selected">
						<a href="<?=$arItem["LINK"]?>" class="nav-list__link parent"><?=$arItem["TEXT"]?></a>						
                        <ul> 
				<?endif?>

			<?else:?>

				<?if ($arItem["PERMISSION"] > "D"):?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li class="nav__item header-nav__item">
							<a href="<?=$arItem["LINK"]?>" class="nav-list__link"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li class="nav__item header-nav__item item-selected">
							<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?else:?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li class="nav__item header-nav__item">
							<a href="" class="nav-list__link" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?else:?>
						<li class="nav__item header-nav__item">
							<a href="" class="nav-list__link denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a>
						</li>
					<?endif?>
				<?endif?>

			<?endif?>
			<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

			<?endforeach?>
			<?if ($previousLevel > 1)://close last item tags?>
				<?=str_repeat("</ul></div></div></li>", ($previousLevel-1) );?>
			<?endif?>

        </ul>
    </div>

</nav>
<?endif?>