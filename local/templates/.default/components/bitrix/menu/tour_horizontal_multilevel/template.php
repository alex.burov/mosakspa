<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>

<?if (!empty($arResult)):?>

<nav class="nav">
	<div class="container">
	
	<div class="box-btn">
		<a href="#" class="toggle-menu">
			<span></span>
			<span></span>
			<span></span>
		</a>
	</div>

	<ul class="menu">
	
		<?$previousLevel = 0;
		foreach($arResult as $arItem):?>
			<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
			<?endif?>

			<?if ($arItem["IS_PARENT"]):?>
				<?if ($arItem["DEPTH_LEVEL"] == 1):?>
					<li class="dd"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						<ul>
				<?else:?>
					<li><a <?=$arItem["PARAMS"]["target"]?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						<ul>
				<?endif?>
			<?else:?>		
				<?if ($arItem["PERMISSION"] > "D"):?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li><a <?if ($arItem["SELECTED"]):?>class="act"<?endif?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
					<?endif?>
				<?else:?>
					<?// ���� � ������������ ��� ����� �� ������ � ����� �������?>
					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li><a <?=$arItem["PARAMS"]["target"]?> href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
					<?else:?>
						<li><a <?=$arItem["PARAMS"]["target"]?> href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
					<?endif?>
				<?endif?>		
			<?endif?>
			
			<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
		<?endforeach?>
		
		<?if ($previousLevel > 1)://close last item tags?>
			<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
		<?endif?>
	
		</ul>

	</div>
</nav>
<?endif?>