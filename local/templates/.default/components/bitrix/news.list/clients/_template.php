<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<div class="ec-message-list">
	<?if(count($arResult["ITEMS"])>0):?>   
		<h3 class="title-color"><?=$arParams["TITLE"]?></h3>
		<?foreach($arResult["ITEMS"] as $arItem):?>	
		<?
			// $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			// $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>	
				<div class="ec-message">
                    <div class="number-order"> Номер заявки: #<?echo $arItem["APP_NUMBER"]?> </div>
                    <div class="name"><?echo $arItem["CLIENT_NAME"]?></div>
                    <div class="desc">
                        <p><?echo $arItem["MESSAGE"]?></p>
                    </div>
                    <div class="services">
					    <div class="ser">
					      <div class="OtNaUs">Отзыв на услугу:</div>
					      <div class="NameUs"> 
					         <span itemprop="itemReviewed"><?echo $arItem["TYPE"]?></span> 
					      </div>
					    </div>
					    <?if($arItem["RATING"]):?>
					   		<?if($arItem["RATING"]==1):?>
					   			<div class="ec-stars"><span style="width: calc(4%*5)"></span> </div>
					   		<?endif;?>

					   		<?if($arItem["RATING"]==2):?>
					   			<div class="ec-stars"><span style="width: calc(8%*5)"></span> </div>
					   		<?endif;?>

					   		<?if($arItem["RATING"]==3):?>
					   			<div class="ec-stars"><span style="width: calc(12%*5)"></span> </div>
					   		<?endif;?>

					   		<?if($arItem["RATING"]==4):?>
					   			<div class="ec-stars"><span style="width: calc(16%*5)"></span> </div>
					   		<?endif;?>

					   		<?if($arItem["RATING"]==5):?>
					   			<div class="ec-stars"><span style="width: calc(20%*5)"></span> </div>
					   		<?endif;?>
					    <?endif;?>
					</div>
                </div>              
		<?endforeach;?>
<?else:?>
	<h3 class="title-color"><?=$arParams["TITLE"]?></h3>
<?endif;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

