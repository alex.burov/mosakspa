<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <h3 class="title-color">Наши клиенты</h3>                
    <div class="slider" style=" margin: 50px 100px; position: relative;"> 
        <div class="sliders__arrow sliders__arrow--prev"><</div>
        <div class="sliders__arrow sliders__arrow--next">></div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
            	<?foreach($arResult["ITEMS"] as $arItem):?>	
	                <div class="swiper-slide">
	                    <div class="slides__slide-wrapper">
	                    <div class="slides__logo-wrapper">
	                        <a href="#">
	                            <img class="slides__slide-logo" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" role="presentation"/>
	                        </a>                                      
	                    </div>

	                     </div>
	                </div>
	            <?endforeach;?>                               
            </div>
        </div>                  
    </div>         
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

