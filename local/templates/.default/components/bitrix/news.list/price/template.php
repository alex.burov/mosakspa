<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="title-color">Цены на услуги</h2>
<div class="container"> 	
	<div class="accordion"> 
		<?foreach($arResult["ITEMS"] as $key => $arSectItem): //Цикл для вывода категорий?>

			<?if($key == 0):?>
				<?$opened = 'accordion-item_opened';?>
				<?$style = 'display: block;';?>
			<?else:?>
				<?$opened = false;?>
				<?$style = 'display: none;';?>
			<?endif?>

			<section class="accordion-item <?=$opened?>">
		        <div class="accordion-item--title"><?echo $arSectItem['NAME']?></div>
		        <div class="accordion-item--content" style="<?=$style?>">
		          <div class="table-responsiver"> 
		              <table>
		                <thead>
		                    <tr>
		                      <th>Вид работ</th>
		                      <th>Стоимость</th>
		                    </tr>
		                </thead>
		                <tbody>
			                <?if(!empty($arSectItem['ELEMENTS'])):?>
			                	<? foreach($arSectItem['ELEMENTS'] as $arItem): //цикли для элементов?>
				                    <tr>
				                      <td><span><?echo $arItem["NAME"]?></span></td>
				                   		<?
				                   		foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){ 
						                    if($arProperty['CODE']=='PRICE'){
					 							$price = $arProperty["VALUE"];
					 						}
					 					}
					 					?>
				                      <td><span><?echo $price?></span></td>
				                    </tr>
			                    <? endforeach ?>
			                <?endif?>
		                   
		                </tbody>
		              </table>
		          </div>
		        </div>
		    </section>

		<?endforeach;?>
	</div>
</div>
<script type="text/javascript">    
    /* Аккордеон */
	$(document).ready(function () {
		    var o, n;
		    $(".accordion-item--title").on("click", function () {
		        (o = $(this).parents(".accordion-item")),
		            (n = o.find(".accordion-item--content")),
		            o.hasClass("accordion-item_opened")
		                ? (o.removeClass("accordion-item_opened"), n.slideUp())
		                : (o.addClass("accordion-item_opened"), n.stop(!0, !0).slideDown(), o.siblings(".accordion-item_opened").removeClass("accordion-item_opened").children(".accordion-item--content").stop(!0, !0).slideUp());
		    });
	});
  </script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
