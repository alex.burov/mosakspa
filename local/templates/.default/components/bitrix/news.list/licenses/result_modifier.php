<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach ($arResult["ITEMS"] as $key => $arItem){
	
	foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){

		if($arProperty['CODE']=='FILES'){

			if($arProperty['VALUE']){
				$files = [];
				foreach ($arProperty['VALUE'] as $value) {
					$files[] = CFile::GetPath($value);
				}
				$arResult["ITEMS"][$key]['FILES'] = $files;
			}
		}
	}
}
?>