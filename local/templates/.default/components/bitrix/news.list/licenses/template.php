<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
// $this->addExternalCss("/local/templates/".SITE_TEMPLATE_ID."/css/licences.css");
// $this->addExternalJS("https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js");
?>


<section class="section licensies">
         <div class="container">
            <div class="section-content-wrap lic-title">
                
                <h2 class="title-color">Лицензии</h2>               
                	<?if($arResult["ITEMS"]):?>

		                <div class="licensies__content flex">
			               	<?foreach($arResult["ITEMS"] as $arItem):?>		               		
			                	
				                <div class="license">		
				    				<?if($arItem['FILES']):?>	
									    <div class="license__image-wrap flex">
										    <?foreach($arItem['FILES'] as $file):?>
						                        <a class="owl-fancy" href="<?=$file?>" data-fancybox="gallery" data-caption="<?=$arItem["PREVIEW_TEXT"]?>">
						                        	<img class="owl-fancy-min" alt="feedback1_s" src="<?=$file?>" alt="" class="license__image">
						                        </a>
						                    <?endforeach;?>
					                    </div>
	            					<?endif;?>	
				                    <p class="license__text"><?=$arItem["PREVIEW_TEXT"]?></p>
				                </div>

			                <?endforeach;?>
		                </div>

		                <div class="licensies__slider owl-carousel" id="licensies-slider">
		               	    <?foreach($arResult["ITEMS"] as $arItem):?>	                    
		                    	<div class="license">
			                    	<?if($arItem['FILES']):?>	
									    <div class="license__image-wrap flex">
										    <?foreach($arItem['FILES'] as $file):?>
						                        <a class="owl-fancy" href="<?=$file?>" data-fancybox="gallery" data-caption="<?=$arItem["PREVIEW_TEXT"]?>">
						                        	<img class="owl-fancy-min" alt="feedback1_s" src="<?=$file?>" alt="" class="license__image">
						                        </a>
						                    <?endforeach;?>
					                    </div>
	            					<?endif;?>	
			                       <p class="license__text"><?=$arItem["PREVIEW_TEXT"]?></p>
			                    </div>
		                    <?endforeach;?>
		                </div>

	            	<?endif;?>

            </div>
         </div>
    </section>

<script type="text/javascript">
    $(document).ready(function(){
        $("#licensies-slider").owlCarousel({
           items: 2
        });
    });

    var owl = $('#licensies-slider');
        owl.owlCarousel({
            items:1,
            center: true,
            loop:true,
            dots:false,
            nav: true,
            autoplay: false,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            navText: ['<span aria-label="Previous">‹</span>','<span aria-label="Next">›</span>']
    });
</script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

