<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0):?>
<div class="slideshow-container">
    <div class="slider-mini">
      <?foreach($arResult["ITEMS"] as $arItem):?>
          <?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
          $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
          <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <img style="padding-left: 0; transform: translateX(0px);" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
          </div>
      <?endforeach;?>
    </div>
</div>
<?endif;?>
