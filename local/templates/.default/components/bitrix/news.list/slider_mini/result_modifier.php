<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach ($arResult["ITEMS"] as $key => $arItem)
{
	$arResult["ITEMS"][$key]["LINK"] = $arItem["PROPERTIES"]["LINK"]["VALUE"];
	
	$arResult["ITEMS"][$key]["TARGET"] = NULL;
	
	if ($arItem["PROPERTIES"]["TARGET"]["VALUE_XML_ID"] == "id_yes")
		$arResult["ITEMS"][$key]["TARGET"] = "_blank";
	
	if ($arItem["PROPERTIES"]["TARGET"]["VALUE_XML_ID"] == "id_no")
		$arResult["ITEMS"][$key]["TARGET"] = "_self";
	}
?>