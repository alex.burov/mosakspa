<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0):?>
<div class="slider-main">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
      $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
      <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
        <div class="slider-container">
          <div class="row">
              <div class="col-11 col-md-5 col-lg-7">
                <div class="slider-text">
                     <?=$arItem['PREVIEW_TEXT']?>
                    </div>
              </div>
          </div>
        </div>
      </div>
    <?endforeach;?>  
</div>
<?endif;?>
