<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="reviews services-page--web">
   <div class="container">
        <h3 class="title-color">Отзывы</h3>
        <div class="reviews-block">
            <div class="reviews-slide  owl-carousel ec-message-list">
               <?foreach($arResult["ITEMS"] as $arItem):?>	
                <div class="item">                                 
                    <div class="number-order"> Номер заявки: #<?echo $arItem["APP_NUMBER"]?> </div>
                    <div class="name"><?echo $arItem["CLIENT_NAME"]?></div> 
                    <div class="desc">
                        <p><?echo $arItem["MESSAGE"]?></p>
                    </div>
                    <div class="review-block">
                        <div class="service-block">
                           <div class="review"><img class="icon" src="<?=$templateFolder?>/img/icon-install.png" alt="Установка счетчиков воды">Услуга</div>
                           <div class="service-name"><span><?echo $arItem["TYPE"]?></span></div>
                        </div>                                            
                    </div>                                 
                </div>
               <?endforeach;?>           
            </div>
        </div>               
   </div>
</div>        

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
<script type="text/javascript">    
    $('.reviews-slide').owlCarousel({ 
        margin: 50,
        loop: true,
        nav: true,
        autoHeight: true,
        navText: '',
        responsive: {
            991: {
                items: 2,
            },
            600: {
                 items: 2,
            },
            320: {
                 items: 1,
            }
         }
    }); 
</script>