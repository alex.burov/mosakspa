<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach ($arResult["ITEMS"] as $key => $arItem){
	
	foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty){

		if($arProperty['CODE']=='CLIENT_NAME'){
			$arResult["ITEMS"][$key]['CLIENT_NAME']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='APP_NUMBER'){
			$arResult["ITEMS"][$key]['APP_NUMBER']  = $arProperty["DISPLAY_VALUE"];
		}


		if($arProperty['CODE']=='MESSAGE'){
			$arResult["ITEMS"][$key]['MESSAGE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='TYPE'){
			$arResult["ITEMS"][$key]['TYPE']  = $arProperty["DISPLAY_VALUE"];
		}

		if($arProperty['CODE']=='RATING'){
			$arResult["ITEMS"][$key]['RATING']  = $arProperty["VALUE_XML_ID"];
		}
	}
}
?>