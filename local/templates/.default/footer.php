<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!-- start form-->
<?
$APPLICATION->IncludeComponent(
  "bitrix:iblock.element.add.form", 
  "feebdack_up_recaptcha", 
  array(
    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
    "CUSTOM_TITLE_DETAIL_TEXT" => "",
    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
    "CUSTOM_TITLE_NAME" => "",
    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
    "CUSTOM_TITLE_TAGS" => "",
    "DEFAULT_INPUT_SIZE" => "30",
    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
    "ELEMENT_ASSOC" => "CREATED_BY",
    "GROUPS" => array(
      0 => "2",
    ),
    "IBLOCK_ID" => "8",
    "IBLOCK_TYPE" => "forms",
    "LEVEL_LAST" => "Y",
    "LIST_URL" => "",
    "MAX_FILE_SIZE" => "0",
    "MAX_LEVELS" => "100000",
    "MAX_USER_ENTRIES" => "100000",
    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
    "PROPERTY_CODES" => array(
    ),
    "PROPERTY_CODES_REQUIRED" => array(
    ),
    "RESIZE_IMAGES" => "N",
    "SEF_MODE" => "N",
    "STATUS" => "ANY",
    "STATUS_NEW" => "N",
    "USER_MESSAGE_ADD" => "",
    "USER_MESSAGE_EDIT" => "",
    "USE_CAPTCHA" => "N",
    "COMPONENT_TEMPLATE" => "feebdack_up"
  ),
  false
);
?>

<!-- <section class="form" id="form"><a name="form"></a>
        <div class="container">
          <form class="form__wrapper">
            <div class="form__head-wrapper">
              <h2 class="form__title">Отправить заявку
              </h2>
              <div class="form__message-wrapper">
                <p class="form__message">Форма заполнена некорректно
                </p>
                <p class="form__message accent-color">Форма отправлена, спасибо!
                </p>
              </div>
            </div>
            <div class="row animate" href="#" data-animation="fadeInRight">
              <div class="col-12 col-lg-6">
                <div class="form__input-wrap input-wrap"><input class="form__input input" type="mail" required="required"/>
                  <label class="form__label label"><span class="form__placeholder placeholder">Email</span>
                  </label>
                </div>
              </div>
              <div class="col-12 col-lg-6">
                <div class="form__input-wrap input-wrap"><input class="form__input input js_phone_mask input input--error" type="phone" required="required"/>
                  <label class="form__label label"><span class="form__placeholder placeholder">Телефон</span>
                  </label>
                </div>
              </div>

              <div class="col-12 col-lg-12" style="display: flex; flex-direction: row; text-align: center;">
                <div  class="col-4 col-lg-4">
                      <input type="checkbox" name="servece" checked>
                      <label for="scales">Поверка</label>
                </div>

                <div class="col-4 col-lg-4">
                      <input type="checkbox" name="servece">
                      <label for="horns">Установка</label>
                </div>

                <div class="col-4 col-lg-4">
                      <input type="checkbox" name="servece">
                      <label for="horns">Замена</label>
                </div>
            </div>
             <div class="col-12 col-lg-7 _col-xxl-6 offset-lg-1 offset-xxl-0 pl-xxl-4 mt-3">
                <p>Нажимая кнопку «Отправить», я подтверждаю свою дееспособность, даю согласие на обработку своих персональных данных в соответствии с<a href="#"> Условиями</a></p>
              </div>

              <div class="col-12 col-lg-4 _col-xxl-6 mt-3 col-button">
                <button class="form__btn btn">Отправить
                </button>
              </div>
              
            </div>
          </form>
        </div>
      </section>  -->

<!-- end form-->
      <footer class="footer">
        <div class="container">
          <div class="row align-items-lg-stretch">
            <div class="col-12 col-lg-1 pl-lg-0 mb-4 pb-2 order-lg-4 pb-lg-0 mb-lg-0">
              <div class="row align-items-center flex-lg-column justify-content-between h-100">
                <div class="col col-lg-auto">
                  <div class="social social social--hide-lg-tg">
                    <ul class="social__list">
                      <li class="social__item social__item--telegram"><a class="social__item-link" href="https://t.me/addstickers/metasharks">
                        <svg class="icon icon-telegram ">
                          <use xlink:href="/local/img/icons/sprite.svg#telegram"></use>
                        </svg><span><?=GetMessage("STICKERPACK")?></span></a>
                      </li>
                      <li class="social__item"><a class="social__item-link" href="https://www.facebook.com/metasharks" target="_blank">
                        <svg class="icon icon-facebook ">
                          <use xlink:href="/local/img/icons/sprite.svg#facebook"></use>
                        </svg></a>
                      </li>
                      <li class="social__item"><a class="social__item-link" href="https://www.instagram.com/metasharks" target="_blank">
                        <svg class="icon icon-instagram ">
                          <use xlink:href="/local/img/icons/sprite.svg#instagram"></use>
                        </svg></a>
                      </li>
                    </ul>
                  </div>
                </div>
               
              </div>
            </div>

            <div class="col-12 col-lg-4 order-lg-3">
              <div class="row flex-lg-column justify-content-between h-100">
                <div class="col-12 col-lg-auto">
               <?$APPLICATION->IncludeComponent(
                              "bitrix:main.include",
                              "",
                              Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "phone",
                                "EDIT_TEMPLATE" => ""
                              )
               );?>
                <?$APPLICATION->IncludeComponent(
                              "bitrix:main.include",
                              "",
                              Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "email",
                                "EDIT_TEMPLATE" => ""
                              )
               );?>
                </div>
                <div class="d-none d-lg-block col-lg-auto mt-lg-3">
                  <div class="social social social--tg">
                    <ul class="social__list">
                      <li class="social__item social__item--telegram">
                        <a class="social__item-link" href="https://t.me/addstickers/metasharks">
                        <svg class="icon icon-telegram ">
                          <use xlink:href="/local/img/icons/sprite.svg#telegram"></use>
                        </svg><span><?=GetMessage("STICKERPACK")?></span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-12 col-lg-4 order-lg-2">
              <div class="row flex-lg-column justify-content-between h-100">
                <div class="col-12 col-lg-auto">
                  <?$APPLICATION->IncludeComponent(
                              "bitrix:main.include",
                              "",
                              Array(
                                "AREA_FILE_RECURSIVE" => "Y",
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "address",
                                "EDIT_TEMPLATE" => ""
                              )
                            );?>
               
                </div>
                <div class="col-12 col-lg-auto mt-lg-3">
                  <a class="footer__politics" href="<?=SITE_DIR?>privacy-policy/"><?=GetMessage("PRIVACY_POLICY")?></a>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-3 order-lg-1">
              <div class="row flex-lg-column justify-content-between h-100">
                <div class="col-6 col-lg-auto"><img class="footer__logo" src="/local/img/footer-logo.svg" alt="footer-logo" title=""/>
                </div>
                <div class="col-6 col-lg-auto mt-lg-3">
                  <div class="footer__copyright"> © ООО АКСПА <?=date('Y')?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>

  </div>
    <script>
      // window.onload = function () {
      //     const preloader = document.querySelector('.preloader');
      //     if(preloader){
      //       window.setTimeout(function () {
      //         preloader.classList.add('loaded');
          
      //       }, 1500); 
      //     }
      // }
    </script>
    <script src="/local/templates/.default/js/jQuery.js"></script>
    <script src="/local/templates/.default/js/jquery.inputmask.min.js"></script>
    <script src="/local/templates/.default/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/local/templates/.default/js/jquery.lettering.min.js"></script>
    <script src="/local/templates/.default/js/swiper.min.js"></script>
    
    <script src="/local/templates/.default/js/main.js"></script>
<!--     <script src="/local/templates/.default/js/slider.js"></script> -->
	</body>
</html>