<div class="footer-col-header footer-col footer-col_center flex_col">
  <div class="footer-col-content_center">
      <h3 class="h3 footer__title footer-services-wrap__title">Принимаем к оплате</h3>
  </div>
</div>
<div class="footer-col footer-col_center footer-col-content footer-col-content_border flex_col">
  <div class="footer-col-content_center">
      <div class="footer__payment-wrap__list-wrap flex">

      <ul class="footer-services-wrap__list flex">
          <li class="list-item footer__list-item footer-services-wrap__list-item">
              <a href="#"  class="list-link nav-list__link footer-nav-list__link">Банковские карты</a>
           </li>
          <li class="list-item footer__list-item footer-services-wrap__list-item">
               <a href="#"  class="list-link nav-list__link footer-nav-list__link">Электронные деньги</a>
          </li>
          <li class="list-item footer__list-item footer-services-wrap__list-item">
              <a href="#"  class="list-link nav-list__link footer-nav-list__link">Терминалы и платежные системы</a>
          </li>

      <div class="paymet-box">
                                    
          <ul class="payment">
              <li class="list-inline">
                  <img src="<?=$template_path?>/img/payment/mir.png" alt="МИР">
               </li>

              <li class="list-inline">
                  <img src="<?=$template_path?>/img/payment/mastercard.png" alt="Mastercard">
              </li>

               <li class="list-inline">
                   <img src="<?=$template_path?>/img/payment/visa.png" alt="Visa">
              </li>
          </ul>


           <ul class="payment" style="">
             <li class="list-inline">
                <img src="<?=$template_path?>/img/payment/pay_pal.png" alt="Mastercard">
            </li>
             <li class="list-inline">
                <img src="<?=$template_path?>/img/payment/qiwi.png" alt="Visa">
            </li>
              <li class="list-inline">
                <img src="<?=$template_path?>/img/payment/yandex.png" alt="Visa">
            </li>
          </ul>

      </div>                                

      </div>
  </div>
</div>