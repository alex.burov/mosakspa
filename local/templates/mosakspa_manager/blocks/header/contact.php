 <div class="header-contact__phone-wrap flex">                          
    <div class="knob-numb knob-none"></div>
    <div class="header-contact__day-night"></div>
    <div class="account">
    	<ul>
	    <?if($USER->IsAuthorized()):?>
	    		<?$arUser = getUserData($USER->GetID());?>	
					<li class="inline">
						<a href="/manager/account/" title="Личный кабинет">
							<?=$arUser['LOGIN']?> (<?=$arUser['TYPE']?>)</a>
					</li>
					<li class="inline">
						<a href="<?=$_SERVER['SCRIPT_URI'].'?logout=yes'?>" title="Выход">Выход</a>
					</li>				
				<?else:?>
					<li class="inline">
						<a href="/manager/" title="Личный кабинет">Авторизация</a>
					</li>
	    		<?endif?>
    	</ul>
    </div>
</div>