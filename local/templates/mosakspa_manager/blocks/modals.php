    <!----------- MODAL ----------->
    <div class="overlay"></div>
    <div id="accident">
            <div class="modal-wrap modal-call-wrap" id="modal-call" style="top: 10px; left: 469px; display: none;">
                          
            <div class="modal modal-call">
                            
                <div class="modal-call__header">
                    <p class="modal-call__header-title">Оставить заявку</p>
                </div>                            
                <div class="modal-call__content" id="prompt2" data-sender="1">
                <?if($USER->IsAuthorized()):?> 
                <?$arUser = getUserData($USER->GetID());
                        if($arUser['TYPE'] == 'admin'){
                            $PROPERTY_CODES = array(
                                    "NAME",
                                    "25",
                                    "26",
                                    "28",
                                    "31",
                                    "32", // округа
                                    "33",
                                    "34",
                                    "35",
                                    "36",
                                    "37",
                                    "38", 
                                    "40",
                                    "41", //метро
                                    "42", // оператор
                                    "48",
                                    "59",
                                    "IBLOCK_SECTION",
                            );

                            $LINKED_IBLOCKS = array(
                                "MASTER" => MASTERS,
                                "OPERATOR" => OPERATORS,
                                "CAMPAIGN" => CAMPAIGNS_IBLOCK,
                                "METRO" => METRO_IBLOCK,
                                "DISTRICTS" => DISTRICTS_IBLOCK
                            );
                        }

                        if($arUser['TYPE'] == 'operator'){
                            $PROPERTY_CODES = array(
                                    "NAME",
                                    "25",
                                    "26",
                                    "28",
                                    "31",
                                    "32", // округа
                                    "36", // "36" мастер
                                    "38", 
                                    "40",
                                    "41", // метро
                                    "42", // оператор
                                    "48",
                                    "59"
                            );      

                            $LINKED_IBLOCKS = array(
                                "MASTER" => MASTERS,
                                "CAMPAIGN" => CAMPAIGNS_IBLOCK,
                                "METRO" => METRO_IBLOCK,
                                "DISTRICTS" => DISTRICTS_IBLOCK
                            );
                        }   

                        if($arUser['TYPE'] == 'master'){
                            // die();
                        }
                    ?> 
                    <?if($arUser['TYPE'] !== 'master'):?> 
                        <?$APPLICATION->IncludeComponent(
                          "mosakspa:iblock.element.add.form", 
                          "modal", 
                          array(
                            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                            "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                            "CUSTOM_TITLE_DETAIL_TEXT" => "",
                            "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                            "CUSTOM_TITLE_NAME" => "",
                            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                            "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                            "CUSTOM_TITLE_TAGS" => "",
                            "DEFAULT_INPUT_SIZE" => "30",
                            "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "GROUPS" => array(
                              0 => "2",
                            ),
                            "IBLOCK_ID" => FORMS_IBLOCK,
                            "IBLOCK_TYPE" => "forms",
                            "LEVEL_LAST" => "Y",
                            "LIST_URL" => "",
                            "MAX_FILE_SIZE" => "0",
                            "MAX_LEVELS" => "100000",
                            "MAX_USER_ENTRIES" => "100000",
                            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                            "PROPERTY_CODES" => $PROPERTY_CODES,
                            "PROPERTY_CODES_REQUIRED" => array(
                            ),
                            "RESIZE_IMAGES" => "N",
                            "SEF_MODE" => "N",
                            "STATUS" => "ANY",
                            "STATUS_NEW" => "N",
                            "USER_MESSAGE_ADD" => "",
                            "USER_MESSAGE_EDIT" => "",
                            "USE_CAPTCHA" => "N",
                            "COMPONENT_TEMPLATE" => "modal",
                            "LINKED_IBLOCKS" => $LINKED_IBLOCKS,
                            "USER_TYPE"=>$arUser['TYPE']
                          ),
                          false
                        );?>
                    <?endif?>
                <?endif?>
                <div class="successfull_container_text_accepted" style="display: none;"></div>
                </div>

                <div class="modal__close flex">
                    <svg class="modal__close-icon">
                        <use xlink:href="#close__icon"></use>
                    </svg>
                </div>

            </div>
        </div>
    </div>
