<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
</div>
        <footer class="footer">
             <div class="container">
                <div class="footer-content-wrap">
                   <div class="footer-row footer-row-1 flex">
                      <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/logo.php');?>
                      <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/contact.php');?>                   
                   </div>

                   <div class="footer-row footer-row-2 flex">                      
                      <div class="footer__nav-wrap flex_col">                  
                        <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/sections.php');?>
                      </div>
                      <div class="footer__services-wrap flex_col">
                         <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/services.php');?>
                      </div>
                       <div class="footer__payment-wrap flex_col">
                          <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/payment.php');?>
                      </div>
                      <div class="footer__links-wrap flex_col">
                          <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/footer/social.php');?>                       
                      </div>
                   </div>
                   <div class="footer-row footer-row-3 flex">
                       <div class="footer-col-header footer-bottom">
                           Все материалы, представленные на сайте носят исключительно информационный характер и являются ознакомительными и ни при каких условиях не являются публичной офертой.

                       </div>
                   </div>
                </div>
             </div>
        </footer>

        <? 
        use Bitrix\Main\Page\Asset;
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/jquery.bxslider.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/jquery.inputmask.min.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/jquery.mCustomScrollbar.concat.min.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/jquery.lettering.min.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/main.js"></script>');

        Asset::getInstance()->addString('<script src="'.$template_path.'/js/datepicker/calendar.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/swiper.min.js"></script>');
        Asset::getInstance()->addString('<script src="'.$template_path.'/js/swiper-slider.js"></script>');
        ?> 

        <style> 
          /*
          .regionInsideSelect{overflow:auto;position:absolute;width:100%;top:100%;background:#fff;opacity:0;pointer-events:none;transition:0.2s;height:auto;max-height:150px;border:1px solid #848484;} .regionInsideContainer.focus .regionInsideSelect{opacity:1;pointer-events:all;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4); box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.4);} .regionInsideContainer{position:relative;z-index:10;} .regionInsideSelect div{cursor:pointer;padding:10px;transition:.2s;} .regionInsideSelect div:hover{background:#bebebe;} .regionInsideSelect div.active{background: #fabe8a;border-top: 1px solid #bebebe;border-bottom: 1px solid #bebebe;} .regionInsideSelect div:last-of-type{background:#e1edff;} .regionInsideContainer:before{position:absolute;content:'';width:10px;height:7px;right:4px;top:50%;-webkit-transform:translate(0,-50%);-ms-transform:translate(0,-50%);transform:translate(0,-50%);pointer-events:none;background:url('/template/img/sarrmini.jpg') no-repeat center;} */
        </style>

        <script type="text/javascript">
         /* document.addEventListener('DOMContentLoaded', function() {
            if($('#date').length){
                $('#date').css({'display':'block'});
                console.log($('#date'));              
            }    
          });      */  
        </script>

  </body>
</html>