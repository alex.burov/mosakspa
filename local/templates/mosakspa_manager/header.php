<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Application;

use Bitrix\Main\Context;
if($USER->IsAuthorized()){
    $request = Context::getCurrent()->getRequest();
    $requestUri = $request->getRequestUri(); // текущий адрес страницы
    $logout = htmlspecialcharsbx($request->get('logout'));
    if($logout == 'yes'){
        $USER->Logout();
        LocalRedirect('/manager/');
    }
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?$APPLICATION->ShowTitle(false);?></title>
    <meta name="description" content="о чем страница">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="path/to/image.jpg">
    <?      
      // выносим стили и скрипты в default
      $template_path = '/local/templates/mosakspa_manager'; 
      Asset::getInstance()->addCss($template_path . "/css/font-awesome.min.css");
      Asset::getInstance()->addCss($template_path . "/css/main.css");
      Asset::getInstance()->addCss($template_path . "/css/additional.css");
      Asset::getInstance()->addCss($template_path . "/css/base/forms.css");
      Asset::getInstance()->addCss($template_path . "/css/base/banner.css");
      Asset::getInstance()->addCss($template_path . "/css/base/footer.css");

      Asset::getInstance()->addCss($template_path . "/css/swiper-slider.css");
      Asset::getInstance()->addCss($template_path . "/css/jquery.bxslider.css");
      Asset::getInstance()->addCss($template_path . "/css/owl.css");
      Asset::getInstance()->addCss($template_path . "/css/reviews.css");
      Asset::getInstance()->addCss($template_path . "/css/form_old.css");
      Asset::getInstance()->addCss($template_path . "/css/form_old.css");

      Asset::getInstance()->addCss($template_path . "/css/datetimepicker.css");
      Asset::getInstance()->addJs($template_path . "/js/moment-with-locales.min.js");
      Asset::getInstance()->addJs($template_path . "/js/bootstrap-datetimepicker.min.js");

      Asset::getInstance()->addString('<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />');
      Asset::getInstance()->addString('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>');
      Asset::getInstance()->addString('<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>');
      Asset::getInstance()->addString('<script src="'.$template_path.'/js/owlCarousel.js"></script>');
      Asset::getInstance()->addString('<script src="'.$template_path.'/js/base/forms-modals.js"></script>');     

    ?> 
    <?$APPLICATION->ShowHead();?>  
  </head>
  <body class="page-body">
    <?$APPLICATION->ShowPanel();?>
    <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/svg.php');?> 
    <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/modals.php');?> 
  <div class="main-wrapper">      
      <!-- start scroll-up-->
      <div class="scroll-up"><a class="scroll-up__link animate-scroll" href="top">
          <div class="decor"><<</div>Наверх</a>
      </div>      
      <!-- верхнее меню -->
      <div class="header header--white">                
          <div class="container" style="">
              <div class="header__content flex"> 
                    <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/header/logo.php');?>
                    <div class="header__contact-wrap flex">                       
                        <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/header/contact.php');?>
                        <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/header/mobile_navigation.php');?>
                    </div>
              </div>
          </div>

          <!-- NAVIGATION -->
          <?require_once($_SERVER["DOCUMENT_ROOT"].$template_path.'/blocks/header/navigation.php');?>
      </div>
      <!-- / -->
<!-- контент -->
<div class="container manager-content">