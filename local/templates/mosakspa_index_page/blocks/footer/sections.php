<div class="footer-col-header footer-col footer-col_left flex_col">
  <div class="footer-col-content_left">
     <h3 class="h3 footer__title footer-nav-wrap__title">Разделы</h3>
   </div>
</div>

<div class="footer-col footer-col_left footer-col-content footer-col-content_border flex_col">
  <div class="footer-col-content_left">
      <nav class="nav footer-nav flex">
        <ul class="nav__list footer-nav__list flex">
          <li class="nav__item footer-nav__item list-item footer__list-item">
              <a href="#" class="list-link nav-list__link footer-nav-list__link">О компании</a></li>
           <li class="nav__item footer-nav__item list-item footer__list-item">
              <a href="#" class="list-link nav-list__link footer-nav-list__link">Наши цены</a></li>
          <li class="nav__item footer-nav__item list-item footer__list-item">
              <a href="#" class="list-link nav-list__link footer-nav-list__link">Лицензии и сертификаты</a></li>
          <li class="nav__item footer-nav__item list-item footer__list-item">
              <a href="#" class="list-link nav-list__link footer-nav-list__link">Отзывы</a></li>
          <li class="nav__item footer-nav__item list-item footer__list-item">
              <a href="#" class="list-link nav-list__link footer-nav-list__link">Контакты</a>
          </li>
        </ul>
      </nav>

      <div class="footer__documents-wrap">
         <p class="footer__rights">© 2012-2021 ООО «АКСПА»</p>
        <div class="form-group agreement"> 
                  
          <label class="text-grey">
           <a rel="nofollow" data-fancybox="" data-type="iframe" data-src="/politika-konfidenczialnosti-gcur" data-options="{&quot;type&quot; : &quot;iframe&quot;, &quot;iframe&quot; : {&quot;preload&quot; : false, &quot;css&quot; : {&quot;width&quot; : &quot;90%&quot;, &quot;padding-left&quot; : &quot;15px&quot;}}}" href="javascript:;">Политика конфиденциальности</a> 
                                    </label> 
      </div>
      </div>
  </div>
</div>