<div class="footer-col-header footer-col footer-col_center flex_col">
    <div class="footer-col-content_center">
        <h3 class="h3 footer__title footer-services-wrap__title">Наши услуги</h3>
    </div>
</div>
<div class="footer-col footer-col_center footer-col-content footer-col-content_border flex_col">
    <div class="footer-col-content_center">
        <div class="footer-services-wrap__list-wrap flex">

            <ul class="footer-services-wrap__list flex">
            <li class="list-item footer__list-item footer-services-wrap__list-item">
                <a href="#"  class="list-link nav-list__link footer-nav-list__link">Поверка</a>
            </li>
             <li class="list-item footer__list-item footer-services-wrap__list-item">
                <a href="#"  class="list-link nav-list__link footer-nav-list__link">Установка</a>
             </li>
            <li class="list-item footer__list-item footer-services-wrap__list-item">
                <a href="#"  class="list-link nav-list__link footer-nav-list__link">Замена</a>
            </li>

             <li class="list-item footer__list-item footer-services-wrap__list-item">
                <a href="#"  class="list-link nav-list__link footer-nav-list__link">Сантехнические услуги</a>
            </li>
        </ul>
        </div>
    </div>
</div>