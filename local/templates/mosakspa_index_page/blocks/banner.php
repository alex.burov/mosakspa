<div class="banner" style="background-image: url(<?=$template_path?>/img/banner/banner-blue-green.png);">
    <div class="container">
        <div class="banner__content-wrap flex_col" style="box-sizing: content-box;">
            <div class="banner__content">
                  <h2 class="h2 h2_bold banner-content__title">Установка и поверка счетчиков</h2>
                  <div class="ball-btn">
                     <div class="urgently_call">
                        <div class="block_btn">
                            <div class="block_blue">
                              <div class="block-lin">
                                  <button class="header-contact__button block_button" data-link="modal-call-back">
                                    СРОЧНО вызвать мастера
                                  </button>
                              </div>
                           </div>
                        </div>
                      </div>
                  </div>
            </div>
           <div class="urgently_call mob-hidden">
                <div class="block_btn">
                      <div class="block_blue">
                        <div class="block-lin">
                            <button class="header-contact__button block_button" data-link="modal-call-back">
                               СРОЧНО вызвать мастера 
                            </button>
                         </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>