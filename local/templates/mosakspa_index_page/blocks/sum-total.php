      <div class="container sum-total-wrapper after-intro">

            <div class="container-fluid">
                <div class="after-intro-items">
                    
                    <div class="after-intro-item after-intro-title">
                        <div class="after-intro-item-num">9 лет</div>
                        <div class="after-intro-item-text">опыт работы</div>
                    </div>
                    
                    <div class="after-intro-item">
                        <div class="after-intro-item-num">43</div>
                        <div class="after-intro-item-text">мастеров в штате</div>
                    </div>

                    <div class="after-intro-item">
                        <div class="after-intro-item-num">100746</div>
                        <div class="after-intro-item-text">заказов выполнено</div>
                    </div>

                    <div class="after-intro-item">
                        <div class="after-intro-item-num">98561</div>
                        <div class="after-intro-item-text">довольных клиентов</div>
                    </div>

                </div>
            </div>
      </div>  