<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Установка");?>
    
 
<div class="container">
      <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <h2 class="title-color">Поверить или заменить?</h2>
           </div>
      </div>

      <div class="row">          
          <div class="col-12 col-md-12 col-lg-12">              
              <div class="img-text-box">     
                    <div class="text-box" style="margin-top: 60px;">
                      <p>Монтаж приборов учета воды производится в соответствии со строительными нормами. Установка пломб на счетчики выполняется обязательно, поскольку без них управляющие организации не вправе произвести регистрацию оборудования. Заводской гарантийный срок на все счетчики – 6 лет.</p>
                    </div>           
                    <img src="/local/img/mont-2.png" alt="1" title="" class="img-max-width-md" style="">
              </div>
              <div class="img-text-box">  
              		 <img src="/local/img/mont-4.jpg" alt="1" title="" class="img-max-width-md" style="">   
                    <div class="text-box">
                      <p> Мастер-метролог сам приезжает к вам на дом в удобное время с переносным портативным стендом Эталон и в течение 10-15 минут делает поверку на работоспособность водосчетчика без демонтажа. Ниже указаны цены на популярные счетчики воды для Москвы</p>

                      <p>Согласно ФЗ № 102 «Об обеспечении единства измерений» мы передаем данные о результатах поверки в электронном виде в Росстандарт через ФГИС "Аршин". Также для удобства потребителей мы продолжаем дополнительно выдавать документы о прохождении поверки счетчиков в бумажном виде.</p>
                    </div>          
              </div>
          </div>  
      </div>
 </div>
<h2 class="title-color">Какие документы мы выдаем для регистрации в УК</h2>
<div class="container">

      <div class="row">
          <div class="col-12 col-md-12 col-lg-12">

               <section class="section licensies">
                 <div class="container">
                    <div class="section-content-wrap lic-title">
                        
                       

                       <div class="licensies__content flex">

                           <div class="license">
                             <div class="license__image-wrap flex">
                                <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" alt="" class="license__image"></a>
                                    </div>
                             <p class="license__text">Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)</p>
                          </div>

                          <div class="license">
                             <div class="license__image-wrap flex">
                                <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" alt="" class="license__image"></a>
                             </div>
                             <p class="license__text">Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев</p>
                          </div>

                          <div class="license">
                             <div class="license__image-wrap flex">
                                <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img class="owl-fancy-min" alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" alt="" class="license__image"></a>
                             </div>
                             <p class="license__text">Лицензии МЧС<br />№ 66-Б/01116</p>
                          </div>

                       </div>

                       <div class="licensies__slider owl-carousel" id="licensies-slider">
                              <div class="license">
                                <div class="license__image-wrap flex">
                                   <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro.jpg" alt="" class="license__image"></a>
                                   <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/sro-2.7.jpg" data-fancybox="gallery" data-caption="СРО"><img class="owl-fancy-min" alt="feedback1_s" src="/local/static/truba-delo/images/webarch/sro-2.jpg" alt="" class="license__image"></a>
                                </div>
                                <p class="license__text">Наша компания является членом СРО (свидетельство №&nbsp;0124.3-2017-6670144841)</p>
                             </div>

                             <div class="license">
                                <div class="license__image-wrap flex">
                                   <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-1.1.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback2_s" src="/local/static/truba-delo/images/webarch/license-2-1.jpg" alt="" class="license__image"></a>
                                   <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-2-2.2.jpg" data-fancybox="gallery" data-caption="ВСК"><img class="owl-fancy-min" alt="feedback3_s" src="/local/static/truba-delo/images/webarch/license-2-2.jpg" alt="" class="license__image"></a>
                                </div>
                                <p class="license__text">Услуги компании застрахованы и срок гарантии на них — 12&nbsp;месяцев</p>
                             </div>

                             <div class="license">
                                <div class="license__image-wrap flex">
                                   <a class="owl-fancy" href="/local/static/truba-delo/images/webarch/license-3-1.7.jpg" data-fancybox="gallery" data-caption="МЧС"><img class="owl-fancy-min" alt="feedback4_s" src="/local/static/truba-delo/images/webarch/license-3-1.jpg" alt="" class="license__image"></a>
                                </div>
                                <p class="license__text">Лицензии МЧС<br />№ 66-Б/01116</p>
                             </div>
                       </div>

                    </div>
                 </div>
            </section>

          </div>
      </div> 
</div>
<div class="bottom"></div>
<script type="text/javascript">
            $(document).ready(function(){
            $("#licensies-slider").owlCarousel({
               items: 2
            });
         });

         var owl = $('#licensies-slider');
         owl.owlCarousel({
            items:1,
            center: true,
            loop:true,
            dots:false,
            nav: true,
            autoplay: false,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            navText: ['<span aria-label="Previous">‹</span>','<span aria-label="Next">›</span>']
         });
      </script>
<!-- форма -->
<?
// $APPLICATION->IncludeComponent(
//   "bitrix:iblock.element.add.form", 
//   "calendar", 
//   array(
//     "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
//     "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
//     "CUSTOM_TITLE_DETAIL_PICTURE" => "",
//     "CUSTOM_TITLE_DETAIL_TEXT" => "",
//     "CUSTOM_TITLE_IBLOCK_SECTION" => "",
//     "CUSTOM_TITLE_NAME" => "",
//     "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
//     "CUSTOM_TITLE_PREVIEW_TEXT" => "",
//     "CUSTOM_TITLE_TAGS" => "",
//     "DEFAULT_INPUT_SIZE" => "30",
//     "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
//     "ELEMENT_ASSOC" => "CREATED_BY",
//     "GROUPS" => array(
//       0 => "2",
//     ),
//     "IBLOCK_ID" => FORMS_IBLOCK,
//     "IBLOCK_TYPE" => "forms",
//     "LEVEL_LAST" => "Y",
//     "LIST_URL" => "",
//     "MAX_FILE_SIZE" => "0",
//     "MAX_LEVELS" => "100000",
//     "MAX_USER_ENTRIES" => "100000",
//     "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
//     "PROPERTY_CODES" => array(
//     ),
//     "PROPERTY_CODES_REQUIRED" => array(
//     ),
//     "RESIZE_IMAGES" => "N",
//     "SEF_MODE" => "N",
//     "STATUS" => "ANY",
//     "STATUS_NEW" => "N",
//     "USER_MESSAGE_ADD" => "",
//     "USER_MESSAGE_EDIT" => "",
//     "USE_CAPTCHA" => "N",
//     "COMPONENT_TEMPLATE" => "feebdack_up",
//     "SERVICE_TYPE" =>3,
//     "TITLE"=>"Отправить заявку на установку счетчика"
//   ),
//   false
// );
?>   
<!-- форма -->  
<div class="bottom"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>